{script src="js/tygh/exceptions.js"}
<div class="ty-product-block ty-product-detail">
    <div class="ty-product-block__wrapper clearfix">
        {hook name="products:view_main_info"}
        {if $product}
            {assign var="obj_id" value=$product.product_id}
            {include file="common/product_data.tpl" product=$product but_role="big" but_text=__("add_to_cart") product_labels_position="left-top"}

            {hook name="abt__youpitheme:product_page_header"}
                {if !$hide_title}
                    <h1 class="ty-product-block-title" {live_edit name="product:product:{$product.product_id}"}><bdi>{$product.product nofilter}</bdi></h1>
                {/if}

                {include file="common/breadcrumbs.tpl"}
            {/hook}
            <div>
                <div class="sticky-block">

                    <div class="product-block-title-rt clearfix">
                        {hook name="products:ypi__product_rating"}
                            <div class="ty-product-block__rating">
                                {assign var="rating" value="rating_`$obj_id`"}
                                {if $product.discussion.posts && $product.discussion.search.total_items > 0}
                                    <div class="ty-discussion__rating-wrapper" id="average_rating_product">
                                        {$smarty.capture.$rating nofilter}<a class="ty-discussion__review-a cm-external-click" data-ca-scroll="discussion" data-ca-external-click-id="discussion">{$product.discussion.search.total_items} {__("reviews", [$product.discussion.search.total_items])}</a>{if $product.discussion_type && $product.discussion_type != 'D'}{include file="addons/discussion/views/discussion/components/new_post_button.tpl" name=__("write_review") obj_id=$obj_id obj_prefix="main_info_title_" style="text" object_type="Addons\\Discussion\\DiscussionObjectTypes::PRODUCT"|enum locate_to_review_tab=true}{/if}
                                    </div>
                                {else}
                                    <div class="ty-discussion__rating-wrapper">
                                        <span class="ty-nowrap no-rating"><i class="ty-icon-star-empty"></i><i class="ty-icon-star-empty"></i><i class="ty-icon-star-empty"></i><i class="ty-icon-star-empty"></i><i class="ty-icon-star-empty"></i></span>
                                        {if $product.discussion_type && $product.discussion_type != 'D'}{include file="addons/discussion/views/discussion/components/new_post_button.tpl" name=__("write_review") obj_id=$obj_id obj_prefix="main_info_title_" style="text" object_type="Addons\\Discussion\\DiscussionObjectTypes::PRODUCT"|enum locate_to_review_tab=true}{/if}
                                    </div>
                                {/if}
                            </div>
                        {/hook}
                        
                        <div class="ty-product-block__sku">
                            {assign var="sku" value="sku_`$obj_id`"}
                            {$smarty.capture.$sku nofilter}
                        </div>
                    </div>

                    <div class="ypi-product-block__left">
                        {$ab__vg_videos = 0} {if function_exists('fn_ab__vg_get_videos')} {$ab__vg_videos='fn_ab__vg_get_videos'|call_user_func:$product.product_id} {/if}
                        <div class="span8 ty-product-block__img-wrapper {if $settings.Appearance.thumbnails_gallery == "N" and $addons.ab__video_gallery.vertical == 'Y' and ($product.image_pairs|count + $ab__vg_videos|count) > 6}two-thumb-col{elseif $addons.ab__video_gallery.vertical == 'Y' and ($product.image_pairs|count + $ab__vg_videos|count) >= 1}one-thumb-col{else}not-thumb-col{/if}">
                            {hook name="products:image_wrap"}
                            {if !$no_images}
                                <div class="ypi-product-block__img cm-reload-{$product.product_id}" data-ca-previewer="true" id="product_images_{$product.product_id}_update">
									{assign var="product_labels" value="product_labels_`$obj_prefix``$obj_id`"}
									{$smarty.capture.$product_labels nofilter}
                                    {include file="views/products/components/product_images.tpl" product=$product show_detailed_link="Y" image_width=$settings.Thumbnails.product_details_thumbnail_width image_height=$settings.Thumbnails.product_details_thumbnail_height}
                                    <!--product_images_{$product.product_id}_update--></div>
                            {/if}
                            {/hook}
                            <div class="yt-mini-pr-image hidden">{include file="common/image.tpl" images=$product.main_pair no_ids=true image_width=120 image_height=120 lazy_load=true}</div>
                        </div>

                        <div class="span8">
                            {assign var="old_price" value="old_price_`$obj_id`"}
                            {assign var="price" value="price_`$obj_id`"}
                            {assign var="clean_price" value="clean_price_`$obj_id`"}
                            {assign var="list_discount" value="list_discount_`$obj_id`"}
                            {assign var="discount_label" value="discount_label_`$obj_id`"}

                            {hook name="products:ab__deal_of_the_day_product_view"}{/hook}                           

                            {assign var="form_open" value="form_open_`$obj_id`"}
                            {$smarty.capture.$form_open nofilter}

                            <div class="fixed-form-pr-title hidden">{$product.product nofilter}</div>

                            {hook name="products:ypi__left_col"}
                                <div class="{if $settings.abt__yt.products.show_variants_of_variations_options == "without_indent"} ypi-g-du-indent{elseif $settings.abt__yt.products.show_variants_of_variations_options == "below_title"} ypi-g-nl{/if}">
                                    {assign var="product_amount" value="product_amount_`$obj_id`"}
                                    {$smarty.capture.$product_amount nofilter}

                                    <div class="{if $smarty.capture.$old_price|trim || $smarty.capture.$clean_price|trim || $smarty.capture.$list_discount|trim}prices-container {/if}price-wrap {if $product.taxed_price}taxed-price{/if}">
                                        {if $smarty.capture.$old_price|trim || $smarty.capture.$clean_price|trim || $smarty.capture.$list_discount|trim}
                                            <div class="ty-product-prices">
                                                {if $smarty.capture.$old_price|trim}{$smarty.capture.$old_price nofilter}{/if}
                                        {/if}

                                        {if $smarty.capture.$price|trim}
                                            <div class="ty-product-block__price-actual">
                                                {$smarty.capture.$price nofilter}
                                            </div>
                                        {/if}

                                        {if $smarty.capture.$old_price|trim || $smarty.capture.$clean_price|trim || $smarty.capture.$list_discount|trim}
                                                {$smarty.capture.$clean_price nofilter}
                                                {$smarty.capture.$list_discount nofilter}
                                            </div>
                                        {/if}
                                    </div>

                                    {if $capture_options_vs_qty}{capture name="product_options"}{$smarty.capture.product_options nofilter}{/if}
                                    <div class="ty-product-block__option">
                                        {assign var="product_options" value="product_options_`$obj_id`"}
                                        {$smarty.capture.$product_options nofilter}
                                    </div>
                                    {if $capture_options_vs_qty}{/capture}{/if}

                                    <div class="ty-product-block__advanced-option clearfix">
                                        {if $capture_options_vs_qty}{capture name="product_options"}{$smarty.capture.product_options nofilter}{/if}
                                        {assign var="advanced_options" value="advanced_options_`$obj_id`"}
                                        {$smarty.capture.$advanced_options nofilter}
                                        {if $capture_options_vs_qty}{/capture}{/if}
                                    </div>

                                    {if $capture_options_vs_qty}{capture name="product_options"}{$smarty.capture.product_options nofilter}{/if}
                                    <div class="ty-product-block__field-group">
                                        {assign var="qty" value="qty_`$obj_id`"}
                                        {$smarty.capture.$qty nofilter}

                                        {assign var="min_qty" value="min_qty_`$obj_id`"}
                                        {$smarty.capture.$min_qty nofilter}
                                    </div>
                                    {if $capture_options_vs_qty}{/capture}{/if}

                                    {assign var="product_edp" value="product_edp_`$obj_id`"}
                                    {$smarty.capture.$product_edp nofilter}

                                    {if $capture_buttons}{capture name="buttons"}{/if}
                                    <div class="ty-product-block__button">
                                        {if $show_details_button}
                                            {include file="buttons/button.tpl" but_href="products.view?product_id=`$product.product_id`" but_text=__("view_details") but_role="submit"}
                                        {/if}

                                        {assign var="add_to_cart" value="add_to_cart_`$obj_id`"}
                                        {$smarty.capture.$add_to_cart nofilter}

                                        {assign var="list_buttons" value="list_buttons_`$obj_id`"}
                                        {$smarty.capture.$list_buttons nofilter}
                                    </div>
                                    {if $capture_buttons}{/capture}{/if}
                                </div>

                                {hook name="products:promo_text"}
                                {if $product.promo_text}
                                    <div class="ypi-product-block__note">
                                        {$product.promo_text nofilter}
                                    </div>
                                {/if}
                                {/hook}

                                {hook name="products:brand"}
                                    {hook name="products:brand_default"}
                                    {if $product.header_features}
                                        <div class="brand">
                                            {include file="views/products/components/product_features_short_list.tpl" features=$product.header_features}
                                        </div>
                                    {/if}
                                    {/hook}
                                {/hook}

                                {if $product.short_description}
                                    <div class="ty-product-block__description" {live_edit name="product:short_description:{$product.product_id}"}>{$product.short_description nofilter}</div>
                                {/if}

                                {assign var="form_close" value="form_close_`$obj_id`"}
                                {$smarty.capture.$form_close nofilter}

                                {if $show_product_tabs}
                                    {include file="views/tabs/components/product_popup_tabs.tpl"}
                                    {$smarty.capture.popupsbox_content nofilter}
                                {/if}

                            
                            {/hook}
                        </div>
                    </div>

                    <div class="ypi-product-block__right">
					
						{if "MULTIVENDOR"|fn_allowed_for && ($company_name || $company_id) && $settings.Vendors.display_vendor == "Y"}
						    <div class="ypi-vendor-block">
						        <label class="ty-control-group__label">{__("vendor")}:</label>
						        <div class="ypi-vendors-logo">{include file="common/image.tpl" images=$company_data.logos.theme.image image_width="100" image_height="70" lazy_load=true}</div>
								<div class="ypi-vendor-link"><a href="{"companies.products?company_id=`$company_id`"|fn_url}">{if $company_name}{$company_name}{else}{$company_id|fn_get_company_name}{/if}</a></div>
						        <div class="ypi-vendors-info">{$company_data.company_description|strip_tags|truncate:90:"...":true nofilter}</div>
						        <div class="ypi-vendors-contacts">
									{if $company_data.phone}<div class="ypi-vendors-phone">{$company_data.phone nofilter}</div>{/if}
									{hook name="companies:product_company_data"}{/hook}
						        </div>
						    </div>
						{/if}
						
						{* Motivation block *}
						{hook name="products:ab__motivation_block"}{/hook}
						
						{if $settings.abt__yt.products.id_block_con_right_col !== ''}
                            {render_block block_id=$settings.abt__yt.products.id_block_con_right_col dispatch="products.view"  use_cache=false parse_js=false}
                        {/if}

                        {hook name="products:product_detail_bottom"}{/hook}
                    </div>
                </div>
            </div>
        {/if}
        {/hook}

        {if $smarty.capture.hide_form_changed == "Y"}
            {assign var="hide_form" value=$smarty.capture.orig_val_hide_form}
        {/if}

        {hook name="products:product_tabs_pre"}{/hook}

        <div class="ypi-product-block__left">
            <div class="ypi-dt-product-tabs">
                {if $show_product_tabs}
                    {hook name="products:product_tabs"}
                        {include file="views/tabs/components/product_tabs.tpl"}

                    {if $blocks.$tabs_block_id.properties.wrapper}
                        {include file=$blocks.$tabs_block_id.properties.wrapper content=$smarty.capture.tabsbox_content title=$blocks.$tabs_block_id.description}
                    {else}
                        {$smarty.capture.tabsbox_content nofilter}
                    {/if}
                    {/hook}
                {/if}
            </div>
        </div>

    </div>
</div>

<div class="product-details">
</div>

{capture name="mainbox_title"}{assign var="details_page" value=true}{/capture}