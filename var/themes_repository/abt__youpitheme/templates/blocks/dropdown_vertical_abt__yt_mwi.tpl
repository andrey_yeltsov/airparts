{hook name="blocks:topmenu_dropdown"}
{strip}

{if $items}
<div class="ypi-menu__inbox">
    <ul class="ty-menu__items cm-responsive-menu" style="min-height: {$settings.abt__yt.general.menu_min_height}px">
        {hook name="blocks:topmenu_dropdown_top_menu"}

        {math assign="settings_cols" equation="min(6, x)" x=$block.properties.abt__yt_columns_count|default:4}
        {foreach from=$items item="item1" name="item1"}
            {assign var="cat_image" value=$item1.category_id|fn_get_image_pairs:'category':'M':true:true}
            {assign var="item1_url" value=$item1|fn_form_dropdown_object_link:$block.type}
            {assign var="unique_elm_id" value="topmenu_`$block.block_id`_`$item1.param_id`"}

            {assign var="subitems_count" value=$item1.$childs|count}
            <li class="ty-menu__item{if !$item1.$childs} ty-menu__item-nodrop{else} cm-menu-item-responsive{/if}{if $item1.active || $item1|fn_check_is_active_menu_item:$block.type} ty-menu__item-active{/if} first-lvl{if $smarty.foreach.item1.last} last{/if}{if $item1.class} {$item1.class}{/if}"{if $item1.abt__yt_mwi__dropdown == "N"} data-subitems-count="{$item1.$childs|count}" data-settings-cols="{$settings_cols}"{/if}>
                {if $item1.$childs}
                    <a class="ty-menu__item-toggle ty-menu__menu-btn visible-phone cm-responsive-menu-toggle">
	                    <i class="ty-menu__icon-open material-icons">&#xE145;</i>
	                    <i class="ty-menu__icon-hide material-icons">&#xE15B;</i>
	                </a>
                {/if}

                <a {if $item1_url} href="{$item1_url}"{/if} class="ty-menu__item-link a-first-lvl">
                    <div class="menu-lvl-ctn{if $item1.abt__yt_mwi__status == 'Y' && $item1.abt__yt_mwi__desc|strip_tags|trim} exp-wrap{/if}">
                        {if $item1.abt__yt_mwi__status == 'Y' && $item1.abt__yt_mwi__icon}
                            {include file="common/image.tpl" images=$item1.abt__yt_mwi__icon class="mwi-img" no_ids=true}
                        {/if}
                        <bdi>{$item1.$name}</bdi>
                        {if $item1.abt__yt_mwi__status == 'Y' && $item1.abt__yt_mwi__label}
                            <span class="m-label" style="color: {$item1.abt__yt_mwi__label_color}; background-color: {$item1.abt__yt_mwi__label_background}">{$item1.abt__yt_mwi__label}</span>
                        {/if}

                        {if $item1.abt__yt_mwi__desc|strip_tags|trim  && $settings.abt__device != 'mobile'}
                            <div class="exp-mwi-text">{$item1.abt__yt_mwi__desc|strip_tags|trim}</div>
                        {/if}

                        {if $item1.$childs}<i class="icon-right-dir material-icons">&#xE315;</i>{/if}
                    </div>
                </a>
                {if $item1.$childs}
                    {capture name="children"}
                        {if $block.properties.abt_menu_ajax_load != 'Y'}<div class="ty-menu__submenu" id="{$unique_elm_id}">{/if}
                            {$col_width = 100 / $settings_cols}
                            {include file="blocks/menu/components/vertical/`$block.properties.abt__yt_filling_type|default:'column_filling'`.tpl"}
                        {if $block.properties.abt_menu_ajax_load != 'Y'}</div>{/if}
                    {/capture}

                    {if $block.properties.abt_menu_ajax_load != 'Y'}
                        {$smarty.capture.children nofilter}
                    {else}
                        <div class="abtam ty-menu__submenu" id="{$unique_elm_id}"></div>
                        {$smarty.capture.children|fn_abt__yt_ajax_menu_save:$unique_elm_id}
                    {/if}
                {/if}
            </li>
        {/foreach}
        {/hook}
    </ul>
    </div>
{/if}
{/strip}
{/hook}