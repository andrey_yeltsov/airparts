{if !$item1.$childs|fn_check_second_level_child_array:$childs}
    {* Only two levels. Vertical output *}
    <ul class="ty-menu__submenu-items ty-menu__submenu-items-simple{if $item1.abt__yt_mwi__text} with-pic{/if}{if $block.properties.abt_menu_icon_items == 'Y'} with-icon-items{/if} cm-responsive-menu-submenu" style="min-height: {$settings.abt__yt.general.menu_min_height}px">
        {hook name="blocks:topmenu_dropdown_2levels_cols_elements"}
            {include file="blocks/menu/components/horizontal/two_level_columns.tpl"}
        {/hook}
    </ul>
{else}
    <ul class="ty-menu__submenu-items{if $item1.abt__yt_mwi__text} with-pic{/if}{if $item1.$childs|count > 3} lot-children{/if} cm-responsive-menu-submenu" style="min-height: {$settings.abt__yt.general.menu_min_height}px">
        {hook name="blocks:topmenu_dropdown_3levels_cols_elements"}
            {include file="blocks/menu/components/horizontal/three_level_columns.tpl"}
        {/hook}
    </ul>

    {script src="js/addons/abt__youpitheme/abt__yt_column_calculator.js"}
{/if}