{strip}
{foreach from=$item1.$childs item="item2" name="item2"}
    {assign var="item_url2" value=$item2|fn_form_dropdown_object_link:$block.type}
    <li class="ty-menu__submenu-item{if $item2.active || $item2|fn_check_is_active_menu_item:$block.type} ty-menu__submenu-item-active{/if}{if $item2.class} {$item2.class}{/if}" style="width: {$col_width}%">
        <a class="ty-menu__submenu-link" {if $item_url2} href="{$item_url2}"{/if}>
            {if $block.properties.abt_menu_icon_items == 'Y' && $item2.abt__yt_mwi__icon && $settings.abt__device != 'mobile'}
                <span class="mwi-icon">{include file="common/image.tpl" images=$item2.abt__yt_mwi__icon class="mwi-img" no_ids=true}</span>
            {/if}
            <bdi>{$item2.$name}</bdi>
            {if $item2.abt__yt_mwi__status == 'Y' && $item2.abt__yt_mwi__label}
                <span class="m-label" style="color: {$item2.abt__yt_mwi__label_color};background-color: {$item2.abt__yt_mwi__label_background}">{$item2.abt__yt_mwi__label}</span>
            {/if}
        </a>
    </li>
{/foreach}
{if $item1.show_more && $item1_url}
    <li class="ty-menu__submenu-item ty-menu__submenu-alt-link">
        <a href="{$item1_url}"
           class="ty-menu__submenu-alt-link">{__("text_topmenu_view_more")}</a>
    </li>
{/if}
{if $item1.abt__yt_mwi__status == 'Y' && $item1.abt__yt_mwi__text && $settings.abt__device != 'mobile'}
    <li class="mwi-html{if $item1.abt__yt_mwi__dropdown == "Y"} bottom{else} {$item1.abt__yt_mwi__text_position}{/if}">{$item1.abt__yt_mwi__text nofilter}</li>
{/if}
{/strip}