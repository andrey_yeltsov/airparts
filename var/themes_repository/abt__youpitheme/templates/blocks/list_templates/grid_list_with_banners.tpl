{strip}
{if $products}
    {if $settings.abt__yt.product_list.grid_list_descr == 'features'}
        {$products=$products|fn_abt__yt_add_products_features_list:0:true}

    {elseif $settings.abt__yt.product_list.show_brand != 'none' and $settings.abt__yt.general.brand_feature_id > 0}
        {$products=$products|fn_abt__yt_add_products_features_list:$settings.abt__yt.general.brand_feature_id:true}
    {/if}
 
	    
	    {if $settings.abt__yt.product_list.show_sku == 'N' && $settings.abt__yt.product_list.show_amount == 'N'}
	    	{assign var="t1" value=19}
	    {/if}
	    
	    {if !$show_add_to_cart || $settings.abt__yt.product_list.show_buttons == 'N'}
	    	{assign var="t2" value=42}
	    {/if}
	
		{$th = $t1|default:0 + $t2|default:0}
		
		{$abt__yt_gl_item_height=$settings.abt__yt.product_list.height_list_prblock - $th}
	

    {script src="js/tygh/exceptions.js"}

    {if !$no_pagination}
        {include file="common/pagination.tpl"}
    {/if}

    {if !$no_sorting}
        {include file="views/products/components/sorting.tpl"}
    {/if}

    {if !$show_empty}
        {split data=$products size=$columns|default:"2" assign="splitted_products"}
    {else}
        {split data=$products size=$columns|default:"2" assign="splitted_products" skip_complete=true}
    {/if}

    {math equation="100 / x" x=$columns|default:"2" assign="cell_width"}
    {if $item_number == "Y"}
        {assign var="cur_number" value=1}
    {/if}

    {* FIXME: Don't move this file *}
    {script src="js/tygh/product_image_gallery.js"}

    {if $settings.Appearance.enable_quick_view == 'Y'}
        {$quick_nav_ids = $products|fn_fields_from_multi_level:"product_id":"product_id"}
    {/if}
    {$product_list_page=$search.page|default:$smarty.request.page}
    <div class="grid-list" {if $is_category}id="product_list_page{$product_list_page}"{/if}>

        {strip}
            {foreach from=$splitted_products item="sproducts" name="sprod"}
                {foreach from=$sproducts item="product" name="sproducts"}
                    {if !empty($product.product_id)}
                        {$product.product_id|fn_abt__yt_insert_banners_into_list:$abt__yt_banners:$products:$search nofilter}
                    {/if}
                    <div class="col-tile">
                        {if $product}
                            {assign var="obj_id" value=$product.product_id}
                            {assign var="obj_id_prefix" value="`$obj_prefix``$product.product_id`"}
                            {include file="common/product_data.tpl" product=$product product_labels_position="left-top"}
                            <div class="ty-grid-list__item ty-quick-view-button__wrapper{if !$show_add_to_cart || $settings.abt__yt.product_list.show_buttons == 'N'} no-buttons{/if}{if $settings.abt__yt.product_list.show_sku == 'N' && $settings.abt__yt.product_list.show_amount == 'N'} no-amount{/if}{if $settings.abt__yt.product_list.grid_item_hover_zoom == 'Y'} hover-zoom{/if}" style="height: {$abt__yt_gl_item_height + 2|intval}px">
                                {assign var="form_open" value="form_open_`$obj_id`"}
                                {$smarty.capture.$form_open nofilter}
                                
                                {hook name="products:product_multicolumns_list"}
                                
								<div class="ypi-grid-list__item_body" style="min-height: {$abt__yt_gl_item_height|intval}px">
                                {if $settings.abt__yt.product_list.show_sku == 'Y' or  $settings.abt__yt.product_list.show_amount == 'Y'}
                                    <div class="stock-block">
                                        {assign var="sku" value="sku_$obj_id"}
                                        {$smarty.capture.$sku nofilter}

                                        {if $settings.abt__yt.product_list.show_brand == 'none'}
                                            {assign var="product_amount" value="product_amount_`$obj_id`"}
                                            {$smarty.capture.$product_amount nofilter}
                                        {/if}
                                    </div>
                                {/if}
                                    <div class="ty-grid-list__image" style="height: {$settings.Thumbnails.product_lists_thumbnail_height}px;">
	                                    
	                                    {if $show_list_buttons and $settings.abt__yt.product_list.service_buttons_position != 'in_buttons_block'}
											<div class="ypi-list-bt-block hidden">
									            {$compare_product_id = $product.product_id}
									            
									            {if $settings.abt__yt.product_list.service_buttons_position == 'in_images_block'}
										            {if $settings.Appearance.enable_quick_view == 'Y' && !$details_page}
										                {include file="views/products/components/quick_view_link.tpl" quick_nav_ids=$quick_nav_ids}
										            {/if}
												{/if}

									            {capture name="product_buy_now_`$obj_id`"}
									                {$compare_product_id = $product.product_id}
									                {hook name="products:buy_now"}
									                {if $settings.General.enable_compare_products == "Y"}
									                    {include file="buttons/add_to_compare_list.tpl" product_id=$compare_product_id}
									                {/if}
									                {/hook}
									            {/capture}
												{assign var="capture_buy_now" value="product_buy_now_`$obj_id`"}
									            {if $smarty.capture.$capture_buy_now|trim}
									                {$smarty.capture.$capture_buy_now nofilter}
									            {/if}
										   	</div>
									    {/if}
									    
                                        {include file="views/products/components/product_icon.tpl" product=$product show_gallery=$settings.abt__yt.product_list.show_gallery == 'Y'}

										{assign var="product_labels" value="product_labels_`$obj_prefix``$obj_id`"}
										{$smarty.capture.$product_labels nofilter}
                                    </div>
                                    <div class="block-top">
	                                    {hook name="products:ypi_product_list_block_top"}{/hook}
	                                    
                                        {assign var="rating" value="rating_$obj_id"}
                                        {if $smarty.capture.$rating|strlen > 40}
                                            <div class="grid-list__rating">

                                                {hook name="products:dotd_product_label"}{/hook}
                                                
                                                {$smarty.capture.$rating nofilter}{if $product.discussion_amount_posts > 0}
                                                    <span class="cn-comments">({$product.discussion_amount_posts})</span>
                                                {/if}
                                            </div>
                                        {else}
                                            <div class="grid-list__rating no-rating">
                                                
                                                {hook name="products:dotd_product_label"}{/hook}
                                                
                                                {if $addons.discussion.status == "A"}
                                                    <span class="ty-nowrap ty-stars"><i class="ty-icon-star-empty"></i><i class="ty-icon-star-empty"></i><i class="ty-icon-star-empty"></i><i class="ty-icon-star-empty"></i><i class="ty-icon-star-empty"></i></span>
                                                {elseif $product.discussion_amount_posts > 0}
                                                    <span class="cn-comments">({$product.discussion_amount_posts})</span>
                                                {/if}
                                            </div>
                                        {/if}
                                    </div>
                                    {if $settings.abt__yt.product_list.show_brand == 'as_text' and $settings.abt__yt.general.brand_feature_id > 0}
                                        {$b_feature=$product.abt__yt_features[$settings.abt__yt.general.brand_feature_id]}
                                        <div class="ypi-brand">{$b_feature.variant}</div>
                                    {elseif $settings.abt__yt.product_list.show_brand == 'as_logo' and $settings.abt__yt.general.brand_feature_id > 0}
                                        {$b_feature=$product.abt__yt_features[$settings.abt__yt.general.brand_feature_id]}
                                        <div class="ypi-brand-img">
                                            <a href="{"categories.view?category_id=`$product.main_category`&features_hash=`$b_feature.features_hash`"|fn_url}">
                                                {include file="common/image.tpl" image_height=20 images=$b_feature.variants[$b_feature.variant_id].image_pairs no_ids=true lazy_load=true}
                                            </a>
                                        </div>
                                    {/if}
                                    <div class="ty-grid-list__item-name">
                                        {if $item_number == "Y"}
                                            <span class="item-number">{$cur_number}.&nbsp;</span>
                                            {math equation="num + 1" num=$cur_number assign="cur_number"}
                                        {/if}

                                        {assign var="name" value="name_$obj_id"}
                                        <bdi>{$smarty.capture.$name nofilter}</bdi>
                                    </div>

                                    <div class="block-bottom {if $product.taxed_price}taxed-price{/if}">
                                        <div class="v-align-bottom">

                                            {if $settings.abt__yt.product_list.show_qty == 'Y' && !$smarty.capture.capt_options_vs_qty}
                                                <div class="ypi-product__option">
                                                    {assign var="product_options" value="product_options_`$obj_id`"}
                                                    {$smarty.capture.$product_options nofilter}
                                                </div>
                                                <div class="ypi-product__qty">
                                                    {assign var="qty" value="qty_`$obj_id`"}
                                                    {$smarty.capture.$qty nofilter}
                                                </div>
                                            {/if}

                                            <div class="ty-grid-list__price {if $show_qty && $settings.abt__yt.product_list.show_qty == 'Y'}qty-wrap{/if} {if $product.price == 0}ty-grid-list__no-price{/if}">
                                                {assign var="old_price" value="old_price_`$obj_id`"}
                                                {if $smarty.capture.$old_price|trim}{$smarty.capture.$old_price nofilter}{/if}

                                                {assign var="price" value="price_`$obj_id`"}
                                                {$smarty.capture.$price nofilter}
                                                
                                                {if $product.ab__is_qty_discount}
                                                    <div class="qty_discounts_grid">
                                                        <a class="ab__show_qty_discounts ajax-link cm-tooltip" data-ca-product-id="{$product.product_id}" data-ca-target-id="qty_discounts_{$obj_id}" title="{__("qty_discounts")}"><i class="material-icons">&#xE88F;</i></a>
                                                        <div id="qty_discounts_{$obj_id}" class="qty_discounts_popup hidden">
                                                        <!--qty_discounts_{$obj_id}--></div>
                                                    </div>
                                                {/if}

                                                {assign var="clean_price" value="clean_price_`$obj_id`"}
                                                {$smarty.capture.$clean_price nofilter}

                                                {assign var="list_discount" value="list_discount_`$obj_id`"}
                                                {$smarty.capture.$list_discount nofilter}
                                                                                              
                                            </div>

                                            {if $settings.abt__yt.product_list.show_buttons == 'Y'}
                                                {capture name="product_multicolumns_list_control_data_wrapper"}
		                                            <div class="ty-grid-list__control {if $show_list_buttons}show-list-buttons{/if} {if $settings.abt__yt.product_list.service_buttons_position == 'in_buttons_block'}enable-action-button{elseif $settings.abt__yt.product_list.service_buttons_position == 'in_images_block'}in-images-buttons{/if}">
		                                                {capture name="product_multicolumns_list_control_data"}
		                                                    {hook name="products:product_multicolumns_list_control"}		
		                                                        {if $show_add_to_cart}
		                                                            <div class="button-container {if $addons.call_requests.buy_now_with_one_click == "Y"}call-requests__show{/if}">
		                                                                {$add_to_cart = "add_to_cart_`$obj_id`"}
		                                                                {$smarty.capture.$add_to_cart nofilter}
		                                                            </div>
		                                                        {/if}
		                                                    {/hook}
		                                                {/capture}
		                                                {$smarty.capture.product_multicolumns_list_control_data nofilter}
		                                            </div>
		                                        {/capture}
		
		                                        {if $smarty.capture.product_multicolumns_list_control_data|trim}
		                                            {$smarty.capture.product_multicolumns_list_control_data_wrapper nofilter}
		                                        {/if}
                                            {/if}

                                        </div>
                                    </div>

                                    <div class="block-middle">
	                                    {hook name="products:additional_info"}{/hook}
	                                    {if $settings.abt__yt.product_list.grid_list_descr != 'none'}
	                                        {if $settings.abt__yt.product_list.grid_list_descr == 'description'}
	                                            <div class="product-description">
	                                                {assign var="prod_descr" value="prod_descr_`$obj_id`"}
	                                                {$smarty.capture.$prod_descr nofilter}
	                                            </div>
	                                        {elseif $product.abt__yt_features|count > 0}
	                                            <div class="product-feature">
	                                                {assign var="product_features" value="product_features_`$obj_id`"}
	                                                {$smarty.capture.$product_features nofilter}
	                                            </div>
		                                        {$max_features = $settings.abt__yt.product_list.max_features}
		                                        {if $product.abt__yt_features|count > $max_features }
		                                            {$existing_features = $product.abt__yt_features|array_slice:0:$max_features|array_column:"feature_id"}
		                                            <div class="abt-yt-show-more-features"><span></span></div>
		                                            <div class="abt-yt-full-features-list product-feature" data-product-id="{$product.product_id}" data-main-category-id="{$product.main_category}" data-existing-feature-ids="{$existing_features|implode:','}"></div>
		                                        {/if}
	                                        {/if}
                                        {/if}
                                    </div>
                                </div>
                                {/hook}

                                {assign var="form_close" value="form_close_`$obj_id`"}
                                {$smarty.capture.$form_close nofilter}
	                            
                            </div>
                        {/if}
                    </div>
                {/foreach}
                {if $show_empty && $smarty.foreach.sprod.last}
                    {assign var="iteration" value=$smarty.foreach.sproducts.iteration}
                    {capture name="iteration"}{$iteration}{/capture}
                    {hook name="products:products_multicolumns_extra"}
                    {/hook}
                    {assign var="iteration" value=$smarty.capture.iteration}
                    {if $iteration % $columns != 0}
                        {math assign="empty_count" equation="c - it%c" it=$iteration c=$columns}
                        {section loop=$empty_count name="empty_rows"}
                            <div class="ty-column{$columns}">
                                <div class="ty-product-empty">
                                    <span class="ty-product-empty__text">{__("empty")}</span>
                                </div>
                            </div>
                        {/section}
                    {/if}
                {/if}
            {/foreach}

            {include file="addons/abt__youpitheme/views/abt__yt_load_more_products/view.tpl"}
        {/strip}
        <!--product_list_page{$product_list_page}--></div>
    {if !$no_pagination}
        {include file="common/pagination.tpl"}
    {/if}

{/if}

{capture name="mainbox_title"}{$title}{/capture}
{/strip}