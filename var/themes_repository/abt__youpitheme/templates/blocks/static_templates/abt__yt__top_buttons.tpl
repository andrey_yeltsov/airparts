{** block-description:tmpl_abt__yt__top_buttons **}

{if $addons.wishlist.status == "A" && !$hide_wishlist_button}
    {$wishlist_count = "fn_wishlist_get_count"|call_user_func}
    <div id="abt__youpitheme_wishlist_count">
        <a class="cm-tooltip ty-wishlist__a {if $wishlist_count > 0}active{/if}" href="{"wishlist.view"|fn_url}" rel="nofollow" title="{__("view_wishlist")}"><i class="material-icons md-36 md-dark">&#xE87E;</i>{if $wishlist_count > 0}<span class="count">{$wishlist_count}</span>{/if}</a>
        <!--abt__youpitheme_wishlist_count--></div>
{/if}
