{** block-description:required_products **}

{if $product.required_products}

    {if $settings.abt__yt.product_list.show_sku == 'Y'}
        {assign var="show_sku" value=true}
    {/if}

    {if $settings.abt__yt.product_list.show_amount == 'Y'}
        {assign var="show_product_amount" value=true}
    {/if}

    {if $settings.abt__yt.product_list.show_qty == 'Y'}
        {assign var="show_qty" value=true}
    {else}
        {assign var="show_qty" value=false}
    {/if}

    {include file="blocks/list_templates/grid_list_with_banners.tpl"
    products=$product.required_products
    columns=1
    form_prefix="required_products"
    no_sorting="Y"
    no_pagination="Y"
    no_ids="Y"
    obj_prefix="`$block.block_id`000"
    item_number=false
    show_name=true
    hide_qty_label=!$show_qty
    show_sku_label=$show_sku
    show_amount_label=$show_product_amount
    show_old_price=true
    show_price=true
    show_rating=true
    show_features=true
    show_descr=true
    show_clean_price=true
    show_list_discount=true
    show_product_labels=true
    show_discount_label=true
    show_shipping_label=true
    show_add_to_cart=true
    show_list_buttons=true
    but_role="action"
    abt__yt_banners="block"|fn_abt__yt_get_object_banners:$block.block_id
    }
{/if}