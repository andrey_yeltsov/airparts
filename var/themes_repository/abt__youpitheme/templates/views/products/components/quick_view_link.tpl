{ab__hide_content bot_type="ALL"}
{$current_url = $config.current_url|urlencode}
{capture name="quick_view_url"}
{** Sets quick view link *}
{hook name="products:product_quick_view_url"}
{"products.quick_view?product_id=`$product.product_id`&prev_url=`$current_url`"}
{/hook}
{/capture}
{$quick_view_url = $smarty.capture.quick_view_url|trim}
{if $block.type && $block.type != 'main'}
    {$quick_view_url = $quick_view_url|fn_link_attach:"n_plain=Y"}
{/if}
{if $quick_nav_ids}
    {$quick_nav_ids = ","|implode:$quick_nav_ids}
    {$quick_view_url = $quick_view_url|fn_link_attach:"n_items=`$quick_nav_ids`"}
{/if}
<a class="ypi-quick-view-button ty-btn ty-btn__tertiary cm-dialog-opener cm-tooltip cm-dialog-auto-size" data-ca-view-id="{$product.product_id}" data-ca-target-id="product_quick_view" href="{$quick_view_url|fn_url}" data-ca-dialog-title="{__("quick_product_viewer")}" title="{__("quick_product_viewer")}" rel="nofollow"><i class="material-icons md-24">&#xE8F4;</i></a>
{/ab__hide_content}
