<div id="selected_filters_ypi">{$smarty.capture.abt__selected_filters nofilter}<!--selected_filters_ypi--></div>

{hook name="products:search_results_content"}
    {* Horizontal filters *}
    {if $settings.abt__yt.general.block_filters_id !=="" && $products}
        <div class="ypi-filters-container">
            <a class="ypi-white-vfbt"><i class="material-icons">&#xE16D;</i></a>
        </div>
    {/if}
    {* End *}

    <div id="products_search_{$block.block_id}">

    {assign var="products_search" value="Y"}

    {if $products}
        {assign var="title_extra" value="{__("products_found")}: `$search.total_items`"}
        {assign var="layouts" value=""|fn_get_products_views:false:0}

        {if $layouts.$selected_layout.template}
            {include file="`$layouts.$selected_layout.template`" columns=$settings.Appearance.columns_in_products_list show_qty=true}
        {/if}
    {else}
        {hook name="products:search_results_no_matching_found"}
            <p class="ty-no-items">{__("text_no_matching_products_found")}</p>
        {/hook}
    {/if}
{/hook}

<!--products_search_{$block.block_id}--></div>

{hook name="products:search_results_mainbox_title"}
{capture name="mainbox_title"}<span class="ty-mainbox-title__left">{__("search_results")}</span><span class="ty-mainbox-title__right" id="products_search_total_found_{$block.block_id}">{$title_extra nofilter}<!--products_search_total_found_{$block.block_id}--></span>{/capture}
{/hook}