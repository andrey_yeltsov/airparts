{assign var="_title" value=$category_data.category|default:__("vendor_products")}

<div id="selected_filters_ypi">{$smarty.capture.abt__selected_filters nofilter}<!--selected_filters_ypi--></div>

{assign var="products_search" value="Y"}

    {if $subcategories or $category_data.description || $category_data.main_pair}
        {math equation="ceil(n/c)" assign="rows" n=$subcategories|count c=$columns|default:"2"}
        {split data=$subcategories size=$rows assign="splitted_subcategories"}

    {if $category_data.description && $category_data.description != ""}
        <div class="ty-wysiwyg-content ty-mb-s">{$category_data.description nofilter}</div>
    {/if}

        {if $subcategories}
            <ul class="subcategories clearfix">
            {foreach from=$splitted_subcategories item="ssubcateg"}
                {foreach from=$ssubcateg item=category name="ssubcateg"}
                    {if $category}
                        <li class="ty-subcategories__item">
                            <a href="{"companies.products?category_id=`$category.category_id`&company_id=$company_id"|fn_url}">
                            {if $category.main_pair}
                                {include file="common/image.tpl"
                                    show_detailed_link=false
                                    images=$category.main_pair
                                    no_ids=true
                                    image_id="category_image"
                                    image_width=$settings.Thumbnails.category_lists_thumbnail_width
                                    image_height=$settings.Thumbnails.category_lists_thumbnail_height
                                    class="ty-subcategories-img"
                                    lazy_load=true
                                }
                            {/if}
                            {$category.category}
                            </a>
                        </li>
                    {/if}
                {/foreach}
            {/foreach}
            </ul>
        {/if}
    {/if}

    {* Horizontal filters *}
    {if $settings.abt__yt.general.block_filters_id !=="" && $products}
        <div class="ypi-filters-container">
            <a class="ypi-white-vfbt"><i class="material-icons">&#xE16D;</i></a>
        </div>
    {/if}
    {* End *}

<div id="products_search_{$block.block_id}">
    {if $products}
    
    	{assign var="title_extra" value="{__("products_found")}: `$search.total_items`"}
        {assign var="layouts" value=""|fn_get_products_views:false:0}
        {if $category_data.product_columns}
            {assign var="product_columns" value=$category_data.product_columns}
        {else}
            {assign var="product_columns" value=$settings.Appearance.columns_in_products_list}
        {/if}

        {if $layouts.$selected_layout.template}
            {include file="`$layouts.$selected_layout.template`" columns=$product_columns show_qty=true}
        {/if}
    {elseif !$subcategories}
        {hook name="products:search_results_no_matching_found"}
            <p class="ty-no-items">{__("text_no_matching_products_found")}</p>
        {/hook}
    {/if}

<!--products_search_{$block.block_id}--></div>

{hook name="products:search_results_mainbox_title"}
{capture name="mainbox_title"}<span class="ty-mainbox-title__left">{$_title}</span><span class="ty-mainbox-title__right" id="products_search_total_found_{$block.block_id}">{$title_extra nofilter}<!--products_search_total_found_{$block.block_id}--></span>{/capture}
{/hook}