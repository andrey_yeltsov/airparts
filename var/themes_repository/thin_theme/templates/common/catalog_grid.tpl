{* TH *}
<div class="grid-list grid-list--categories-catalog clearfix">
{foreach from=$items item="category"}
    {if $category}
        <div class="ty-column{$columns|default:4} ty-column--categories-catalog">
            <a href="{"categories.view?category_id=`$category.category_id`"|fn_url}" class="ath_subcategories-link">
                {if $category.main_pair}
                    {include file="common/image.tpl"
                        show_detailed_link=false
                        images=$category.main_pair
                        no_ids=true
                        image_id="category_image"
                        image_width=$image_width
                        image_height=$image_height
                        class="ath_subcategories-img"
                    }
                {/if}
                <span class="ath_subcategories-name">
                	{$category.category}
                </span>
            </a>
        </div>
    {/if}
{/foreach}
</div>