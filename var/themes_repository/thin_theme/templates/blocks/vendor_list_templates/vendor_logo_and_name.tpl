{** block-description:ex_vendor_logos_grid **}

{$show_location = $block.properties.show_location|default:"N" == "Y"}
{$show_rating = $block.properties.show_rating|default:"N" == "Y"}
{$show_vendor_rating = $block.properties.show_vendor_rating|default:"N" == "Y"}
{$show_products_count = $block.properties.show_products_count|default:"N" == "Y"}

{$show_name = $block.properties.show_name|default:"N" == "Y"}
{$show_icon_in_title = $block.properties.show_icon_in_title|default:"N" == "Y"}
{$show_all_vendors_btn = $block.properties.show_all_vendors_btn|default:"N" == "Y"}

{$columns=$block.properties.number_of_columns}
{$obj_prefix="`$block.block_id`000"}

{if $items}
    {split data=$items size=$columns|default:"5" assign="splitted_companies"}
    {math equation="100 / x" x=$columns|default:"5" assign="cell_width"}

    <div class="grid-list ty-grid-vendors">
        {strip}
            {foreach from=$splitted_companies item="scompanies" name="scomp"}
                {foreach from=$scompanies item="company" name="scompanies"}
                    <div class="ty-column{$columns}">
                        {if $company}
                            {if $company.logos}
                                {$show_logo = true}
                            {else}
                                {$show_logo = false}
                            {/if}

                            {$obj_id=$company.company_id}
                            {$obj_id_prefix="`$obj_prefix``$company.company_id`"}
                            {include file="common/company_data.tpl" company=$company show_links=true show_logo=$show_logo show_location=$show_location}

                            <div class="ty-grid-list__item">
                                {hook name="companies:featured_vendors"}
                                        <div class="ty-grid-list__company-logo">
                                            {$logo="logo_`$obj_id`"}
                                            {$smarty.capture.$logo nofilter}
                                        </div>
                                        
                                        {if $show_name}
	                                        <div class="ty-grid-list__company-name">
		                                        <a href="{"companies.products?company_id=`$company.company_id`"|fn_url}" class="company-name"><bdi>
			                                        {$company.company}
			                                    </bdi></a>
	                                        </div>
                                        {/if}

                                        {$location="location_`$obj_id`"}
                                        {if $show_location && $smarty.capture.$location|trim}
                                            <div class="ty-grid-list__item-location">
                                                <a href="{"companies.products?company_id=`$company.company_id`"|fn_url}" class="company-location"><bdi>
                                                {$smarty.capture.$location nofilter}
                                                </bdi></a>
                                            </div>
                                        {/if}

                                        {$rating="rating_`$obj_id`"}
                                        {if $smarty.capture.$rating && $show_rating}
                                            <div class="grid-list__rating">
                                                {$smarty.capture.$rating nofilter}
                                            </div>
                                        {/if}
                                       
                                        <div class="ty-grid-list__group">
                                            {$vendor_rating="vendor_rating_`$obj_id`"}
                                            {if $smarty.capture.$vendor_rating && $show_vendor_rating}
                                                <div class="ty-grid-list__vendor_rating">
                                                    {$smarty.capture.$vendor_rating nofilter}
                                                </div>
                                            {/if}

                                            <div class="ty-grid-list__total-products">
                                                {$products_count="products_count_`$obj_id`"}
                                                {if $smarty.capture.$products_count && $show_products_count}
                                                    {$smarty.capture.$products_count nofilter}
                                                {/if}
                                            </div>
                                        </div>                                      
                                {/hook}
                            </div>
                        {/if}
                    </div>
                {/foreach}
            {/foreach}
        {/strip}
    </div>
{/if}

{if $show_all_vendors_btn}
    <div class="ty-homepage-vendors__btn">
        <a class="ty-btn {$block.properties.all_vendors_btn_type}" href="{"companies.catalog"|fn_url}">{__("all_vendors")}</a>
    </div>
{/if}

{if $show_icon_in_title}
{capture name="title"}
<span class="title-svg"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 489.4 489.4"><path d="M347.7 263.75h-66.5c-18.2 0-33 14.8-33 33v51c0 18.2 14.8 33 33 33h66.5c18.2 0 33-14.8 33-33v-51c0-18.2-14.8-33-33-33zm9 84c0 5-4.1 9-9 9h-66.5c-5 0-9-4.1-9-9v-51c0-5 4.1-9 9-9h66.5c5 0 9 4.1 9 9v51z"/><path d="M489.4 171.05c0-2.1-.5-4.1-1.6-5.9l-72.8-128c-2.1-3.7-6.1-6.1-10.4-6.1H84.7c-4.3 0-8.3 2.3-10.4 6.1l-72.7 128c-1 1.8-1.6 3.8-1.6 5.9 0 28.7 17.3 53.3 42 64.2v211.1c0 6.6 5.4 12 12 12h381.3c6.6 0 12-5.4 12-12v-209.6c0-.5 0-.9-.1-1.3 24.8-10.9 42.2-35.6 42.2-64.4zM91.7 55.15h305.9l56.9 100.1H34.9l56.8-100.1zm256.6 124c-3.8 21.6-22.7 38-45.4 38s-41.6-16.4-45.4-38h90.8zm-116.3 0c-3.8 21.6-22.7 38-45.4 38s-41.6-16.4-45.5-38H232zm-207.2 0h90.9c-3.8 21.6-22.8 38-45.5 38-22.7.1-41.6-16.4-45.4-38zm176.8 255.2h-69v-129.5c0-9.4 7.6-17.1 17.1-17.1h34.9c9.4 0 17.1 7.6 17.1 17.1v129.5h-.1zm221.7 0H225.6v-129.5c0-22.6-18.4-41.1-41.1-41.1h-34.9c-22.6 0-41.1 18.4-41.1 41.1v129.6H66v-193.3c1.4.1 2.8.1 4.2.1 24.2 0 45.6-12.3 58.2-31 12.6 18.7 34 31 58.2 31s45.5-12.3 58.2-31c12.6 18.7 34 31 58.1 31 24.2 0 45.5-12.3 58.1-31 12.6 18.7 34 31 58.2 31 1.4 0 2.7-.1 4.1-.1v193.2zm-4.1-217.1c-22.7 0-41.6-16.4-45.4-38h90.9c-3.9 21.5-22.8 38-45.5 38z"/></svg></span> {$block.name}{/capture}
{/if}

