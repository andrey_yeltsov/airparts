{* TH *}
{if $subcategories}
    {math equation="ceil(n/c)" assign="rows" n=$subcategories|count c=$columns|default:"2"}
    {split data=$subcategories size=$rows assign="splitted_subcategories"}
    <ul class="th_subcategories-list subcategories clearfix">
    {foreach from=$splitted_subcategories item="ssubcateg"}
        {foreach from=$ssubcateg item=category name="ssubcateg"}
            {if $category}
                <li class="ty-subcategories__item">
                    <a href="{"categories.view?category_id=`$category.category_id`"|fn_url}" class="ty-subcategories__item__link">
                    {if $category.main_pair}
                        {include file="common/image.tpl"
                            show_detailed_link=false
                            images=$category.main_pair
                            no_ids=true
                            image_id="category_image"
                            image_width=24
                            image_height=24
                            class="ty-subcategories-img"
                        }
                    {else}
                    	<i class="ty-subcategories-img ty-subcategories-img--icon icon-tt-cat-bullet"></i>
                    {/if}
                    <span {live_edit name="category:category:{$category.category_id}"}>{$category.category}</span>
                    </a>
                </li>
            {/if}
        {/foreach}
    {/foreach}
    </ul>
{/if}