{** block-description:th_search_with_categories **}
{* TH *}
<div class="ty-search-block th_search-with-categories" id="th_search_by-cat_{$block.block_id}">
    <form action="{""|fn_url}" name="search_form" method="get">
        <input type="hidden" name="subcats" value="Y" />
        <input type="hidden" name="pcode_from_q" value="Y" />
        <input type="hidden" name="pshort" value="Y" />
        <input type="hidden" name="pfull" value="Y" />
        <input type="hidden" name="pname" value="Y" />
        <input type="hidden" name="pkeywords" value="Y" />
        <input type="hidden" name="search_performed" value="Y" />
        
        {hook name="search:additional_fields"}{/hook}

        {strip}
            {if $settings.General.search_objects}
                {assign var="search_title" value=__("search")}
            {else}
                {assign var="search_title" value=__("search_products")}
            {/if}
            <input type="text" name="q" value="{$search.q}" id="search_input{$smarty.capture.search_input_id}" title="{$search_title}" class="ty-search-block__input cm-hint" />
            {assign var="search_category_id" value=0}
			<select name="cid" class="th_search-with-categories__select">
			    <option value="0">{__("all_categories")}</option>

			    {foreach from=$cid|fn_get_subcategories item="cat"}
				    <option value="{$cat.category_id}" {if $smarty.request.cid == $cat.category_id}selected="selected"{$search_category_id = $cat.category_id}{elseif $smarty.request.category_id == $cat.category_id}selected="selected"{$search_category_id = $cat.category_id}{/if} title="{$cat.category nofilter}">
				        {$cat.category|truncate:20:"...":true nofilter}
				    </option>
				{/foreach}
			</select>
			
	        <input type="hidden" name="category_id" value="{$search_category_id}" id="th_search-with-categories__select-id" />
			
            {if $settings.General.search_objects}
                {include file="buttons/magnifier.tpl" but_name="search.results" alt=__("search")}
            {else}
                {include file="buttons/magnifier.tpl" but_name="products.search" alt=__("search")}
            {/if}
        {/strip}

        {capture name="search_input_id"}{$block.snapping_id}{/capture}

    </form>
</div>

<script>
$(document).ready(function(){
    $('.th_search-with-categories__select').customSelect();
});


{* Check search ===== *}
$('#th_search_by-cat_{$block.block_id} form').submit(function() {
    var errors = 0;
	var search_str = $( '#th_search_by-cat_{$block.block_id} .ty-search-block__input' ).val();
	var search_cat = $( '#th_search_by-cat_{$block.block_id} .th_search-with-categories__select' ).val();

	$( '#th_search_by-cat_{$block.block_id} #th_search-with-categories__select-id' ).val( search_cat );


	if ( search_cat != 0 && (!search_str || search_str == '{$search_title}')) {
		errors++;
	}
	
    if(errors > 0){
	    console.log( 'EMPTY ept!' );
	    console.log( '{""|fn_url}index.php?dispatch=categories.view&category_id='+search_cat );
	    location.href = '{""|fn_url}index.php?dispatch=categories.view&category_id='+search_cat;
        return false;
    } 
});

</script>

