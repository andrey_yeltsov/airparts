{if $items}
{strip}

<div class="ath_mega-menu__mobile-opener ty-menu__item ty-menu__menu-btn visible-phone" id="MegaMenuOpenBtn_{$block.block_id}">
    <a class="ty-menu__item-link">
        <i class="ty-icon-short-list"></i>
        <span>{__("menu")}</span>
    </a>
</div>

<div class="ath_mega-menu-wrapper" id="MegaMenu_{$block.block_id}">

<ul class="ath_mega-menu">	
	{* ~~~~~~~~~~~~~~~ LEVEL1 ~~~~~~~~~~~~~~~ *}
	{foreach from=$items item="item_l1" name="item_l1"}
		{assign var="item_l1_url" value=$item_l1|fn_form_dropdown_object_link:"menu"}

		<li class="mm1 ath_mega-menu__l1 ath_mega-menu__l1--{if $item_l1.content_type_l1}{$item_l1.content_type_l1}{else}default{/if} {if $item_l1.float_right == "Y"}ath_mega-menu__l1--right{/if} ath_mega-menu__l1--drop-down-{$item_l1.have_subitems} {$item_l1.class} {$item_l1.drop_down_type}">
				
				{if $item_l1.content_type_l1 == "full_tree"}
				{* ~~~~~~~~~~~~~~~ FULL TREE ~~~~~~~~~~~~~~~ *}
					<a href="{"categories.catalog"|fn_url}" class="ath_mega-menu__l1__link"><i class="icon-tt-menu"></i> {$item_l1.item}
						<i class="icon-tt-right ath_mega-menu-have-child--icon"></i>
					</a>					
					<div class="ath_mega-menu__full_tree-wrapper">
						
						{* back *}
						<div class="ath_mega-menu__back">
							<i class="icon-tt-left"></i> {__("back")}
						</div>
						
						<ul class="ath_mega-menu__full_tree">
						{foreach from=$item_l1.subitems["0"] item="item_cat_l1" name="item_cat_l1"}
						
							{if $smarty.foreach.item_cat_l1.index < $block.properties.full_tree_l1}								
							<li class="mm2 ath_mega-menu__full_tree__l1{if $item_cat_l1.subcategories} ath_mega-menu__full_tree__l1--subcategories{/if}" data-submenu-id="ath_mega-menu__full_tree__l2-wrapper__{$item_cat_l1.category_id}">
								<a  href="{"categories.view&category_id=`$item_cat_l1.category_id`"|fn_url}" class="ath_mega-menu__full_tree__l1__link">
									{if $item_cat_l1.mega_m_category_svg_icon}
										<span class="ath_mega-menu__full_tree__l1__link__svg-icon ath_mega-menu__full_tree__l1__icon">
											{$item_cat_l1.mega_m_category_svg_icon nofilter}
										</span>
									{elseif $item_cat_l1.mega_m_category_icon}
				                        {include file="common/image.tpl"
				                            show_detailed_link=false
				                            images=$item_cat_l1.mega_m_category_icon
				                            no_ids=true
				                            image_id="mega_m_category_icon"
				                            image_width=48
				                            image_height=48
				                            class="ath_mega-menu__full_tree__l1__icon"
				                        }
				                    {else}
					                    <i class="icon-tt-cat-bullet ath_mega-menu__full_tree__l1__icon  ath_mega-menu__full_tree__l1__icon--no-img"></i>
				                    {/if}
				                    
									<span class="ath_mega-menu__name__link">{$item_cat_l1.category}</span>
									
									{if $item_cat_l1.subcategories}
									<i class="icon-tt-right ath_mega-menu__full_tree__l1__icon--child"></i>
									{/if}
								</a>
								
								{* ~~~~~~~~~~~~~~~ FULL TREE l2 ~~~~~~~~~~~~~~~ *}
								{if $item_cat_l1.subcategories}
								<div class="ath_mega-menu__full_tree__l2-wrapper {if $item_cat_l1.mega_m_category_banner}ath_mega-menu__full_tree__l2-wrapper--with-banner{/if}" id="ath_mega-menu__full_tree__l2-wrapper__{$item_cat_l1.category_id}">
									
									{* back *}
									<div class="ath_mega-menu__back">
										<i class="icon-tt-left"></i> {__("back")}
									</div>
									
									{* cat banner *}
									{if $item_cat_l1.mega_m_category_banner}
										<div class="ath_mega-menu__full_tree__l1__banner-wrapper">
		 								{if $item_cat_l1.mega_m_category_banner_url}<a href="{$item_cat_l1.mega_m_category_banner_url|fn_url}" class="ath_mega-menu__full_tree__l1__banner-link">{/if}
											<img src= "{$item_cat_l1.mega_m_category_banner.icon.image_path}" class="ath_mega-menu__full_tree__l1__banner">
		 								{if $item_cat_l1.mega_m_category_banner_url}</a>{/if}
										</div>
		 							{/if}
									
									{* subcategory *}
									<ul class="ath_mega-menu__full_tree__l2--categories clearfix">
									{foreach from=$item_cat_l1.subcategories item=item_cat_l2  name="item_cat_l2"}
									
										{if $smarty.foreach.item_cat_l2.index < $block.properties.full_tree_l2}	
										
											{if $item_cat_l2.subcategories && $block.properties.full_tree_l3 != 0}
												<li class="mm3 ath_mega-menu__full_tree__l2">
													<a  href="{"categories.view&category_id=`$item_cat_l2.category_id`"|fn_url}" class="ath_mega-menu__full_tree__l2__link ath_mega-menu__full_tree__l2__link--title">
														{if $item_cat_l2.mega_m_category_icon}
								                        {include file="common/image.tpl"
								                            show_detailed_link=false
								                            images=$item_cat_l2.mega_m_category_icon
								                            no_ids=true
								                            image_id="mega_m_category_icon"
								                            image_width=20
								                            image_height=20
								                            class="ath_mega-menu__full_tree__l2__icon"
								                        }
								                        {/if}
								                        {$item_cat_l2.category}
													</a>
													{* ~~~~~~~~~~~~~~~ FULL TREE l3 ~~~~~~~~~~~~~~~ *}
													{*
													<a  href="{"categories.view&category_id=`$item_cat_l2.category_id`"|fn_url}" class="ath_mega-menu__full_tree__l2__img-link ath_mega-menu__full_tree__l2__img-link--left">
														<img src="//placehold.it/100" class="ath_mega-menu__full_tree__l2__img">
													</a>
													*}
													{foreach from=$item_cat_l2.subcategories item=item_cat_l3 name="item_cat_l3"}
														{if $smarty.foreach.item_cat_l3.index < $block.properties.full_tree_l3}	
															<a  href="{"categories.view&category_id=`$item_cat_l3.category_id`"|fn_url}" class="ath_mega-menu__full_tree__l3__link"><i class="icon-tt-right"></i> {$item_cat_l3.category}</a>
														{else}
															<a href="{"categories.view&category_id=`$item_cat_l2.category_id`"|fn_url}" class="ath_mega-menu__full_tree__l3__link ath_mega-menu__full_tree__l3__link--more">
																	{__("text_topmenu_view_more")}
															</a>	
															{break}
														{/if}
													{/foreach}
												</li>
											{else}
												<li class="mm3 ath_mega-menu__full_tree__l2">
													<a  href="{"categories.view&category_id=`$item_cat_l2.category_id`"|fn_url}" class="ath_mega-menu__full_tree__l2__link ath_mega-menu__full_tree__l2__link--alone">
														
														<span class="ath_mega-menu__full_tree__l2__cat-name">
														{if $item_cat_l2.mega_m_category_icon}
								                        {include file="common/image.tpl"
								                            show_detailed_link=false
								                            images=$item_cat_l2.mega_m_category_icon
								                            no_ids=true
								                            image_id="mega_m_category_icon"
								                            image_width=20
								                            image_height=20
								                            class="ath_mega-menu__full_tree__l2__icon"
								                        }
								                        {/if}
								                        {$item_cat_l2.category}
														</span>
														{if $item_cat_l2.main_pair}
														{include file="common/image.tpl"
								                            show_detailed_link=false
								                            images=$item_cat_l2.main_pair
								                            no_ids=true
								                            image_id="category_image"
								                            image_width=120
								                            image_height=120
								                            class="ath_mega-menu__full_tree__l2__img"
								                        }
														{/if}
													</a>
												</li>
											{/if}
									
										{else}
											<li class="mm3 ath_mega-menu__full_tree__l2 ath_mega-menu__full_tree__l2--more">
												<a href="{"categories.view&category_id=`$item_cat_l1.category_id`"|fn_url}" class="ath_mega-menu__full_tree__l2__link ath_mega-menu__full_tree__l2__link--title">
													{__("text_topmenu_more", ["[item]" => $item_cat_l1.category])}
												</a>	
											</li>
											{break}
										{/if}
										
									{/foreach}
									</ul>
								</div>
								{/if}
								
	 								 							
							</li>
							{else}
								<li class="mm2 ath_mega-menu__full_tree__l1 ath_mega-menu__full_tree__l1--more">
									<a href="{"categories.catalog"|fn_url}" class="ath_mega-menu__full_tree__l1__link">
										<span class="ath_mega-menu__full_tree__l1__link__svg-icon ath_mega-menu__full_tree__l1__icon">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60"><path d="M8 22c-4.411 0-8 3.589-8 8s3.589 8 8 8 8-3.589 8-8-3.589-8-8-8zm0 14c-3.309 0-6-2.691-6-6s2.691-6 6-6 6 2.691 6 6-2.691 6-6 6zM52 22c-4.411 0-8 3.589-8 8s3.589 8 8 8 8-3.589 8-8-3.589-8-8-8zm0 14c-3.309 0-6-2.691-6-6s2.691-6 6-6 6 2.691 6 6-2.691 6-6 6zM30 22c-4.411 0-8 3.589-8 8s3.589 8 8 8 8-3.589 8-8-3.589-8-8-8zm0 14c-3.309 0-6-2.691-6-6s2.691-6 6-6 6 2.691 6 6-2.691 6-6 6z"/></svg>
										</span>
										{__("all_categories")}
									</a>	
								</li>
								{break}
							{/if}
						{/foreach}
						</ul>
					</div>
				{else}
				{* ~~~~~~~~~~~~~~~ OTHER ~~~~~~~~~~~~~~~ *}
					<a {if $item_l1_url}href="{$item_l1_url}"{/if} class="ath_mega-menu__l1__link">
						{if $item_l1.svg_icon}
							<span class="ath_mega-menu__l1__link__svg-icon">
								{$item_l1.svg_icon nofilter}
							</span>
						{elseif $item_l1.icon.icon}
							{include file="common/image.tpl"
	                            show_detailed_link=false
	                            images=$item_l1.icon
	                            no_ids=true
	                            image_id="static_data_icon"
	                            image_width=22
	                            image_height=22
	                            class="ath_mega-menu__l1__icon"
	                        }
						{/if}
						<span class="ath_mega-menu__name__link">{$item_l1.item}</span>
						{if $item_l1.have_subitems == "Y"}
							<i class="icon-tt-right ath_mega-menu-have-child--icon"></i>
						{/if}
					</a>
					
					{* ~~~~~~~~~~~~~~~ LEVEL2 ~~~~~~~~~~~~~~~ *}
					{if $item_l1.have_subitems == "Y"}
						<div class="ath_mega-menu__dd-wrapper-l2 ath_mega-menu__dd-wrapper-l2--{if $item_l1.drop_down_type}{$item_l1.drop_down_type}{else}columns grid-list{/if}{if $item_l1.drop_down_type == "columns"} grid-list{/if}">
							
							{* back *}
							<div class="ath_mega-menu__back">
								<i class="icon-tt-left"></i> {__("back")}
							</div>
							
							<ul class="ath_mega-menu__l2-items">
							{foreach from=$item_l1.subitems item="item_l2" name="item_l2"}
	                        	{assign var="item_l2_url" value=$item_l2|fn_form_dropdown_object_link:"menu"}
								<li class="mm2 ath_mega-menu__l2 ath_mega-menu__l2--{$item_l2.content_type}{if $item_l1.drop_down_type == "columns" || $item_l1.drop_down_type == "" } th_mm-column{if $item_l1.subitems_count <= 8}{$item_l1.subitems_count}{else}8{/if}{/if} {$item_l2.class}">
								
								
								{if $item_l2.content_type == "wysiwyg"}
									{* ~~~~~~~~~~~~~~~ wysiwyg ~~~~~~~~~~~~~~~ *}
									{$item_l2.wysiwyg nofilter}
								{else}
									{* ~~~~~~~~~~~~~~~ OTHER_2 ~~~~~~~~~~~~~~~ *}			
	 								<a {if $item_l2_url}href="{$item_l2_url}"{/if} class="ath_mega-menu__l2__link{if $item_l2.subitems} ath_mega-menu__l2__link--title{/if}">
		 								{$item_l2.item}
									</a>
		 								
		 								{* ~~~~~~~~~~~~~~~ LEVEL2 ~~~~~~~~~~~~~~~ *}
 		 								{if $item_l2.subitems}
		 								<ul class="ath_mega-menu__l3-tems">
			 								{foreach from=$item_l2.subitems item="item_l3" name="item_l3"}
			 									{assign var="item_l3_url" value=$item_l3|fn_form_dropdown_object_link:"menu"}
		 									<li class="mm3 ath_mega-menu__l3">
		 										<a {if $item_l3_url}href="{$item_l3_url}"{/if} class="ath_mega-menu__l3__link">
		 								<i class="icon-tt-right"></i> {$item_l3.item}
		 										</a>
		 									</li>
		 									{/foreach}
		 								</ul>
		 								{/if}
								{/if}
								</li>
							{/foreach}
							</ul>
						</div>
					{/if}
				{/if}
		</li>
	{/foreach}
	
	
</ul>

{* Settings *}
<div class="ath_mega-menu__mob-settings">
	{$format = "name"}
	
	{if $languages && $languages|count > 1 && $block.properties.language == "Y"}
	<div id="languages_{$block.block_id}" class="clearfix ath_mega-menu__mob-setting ath_mega-menu__mob-setting--languages">
		<h4>{__("languages")}:</h4>
		
        <div class="ty-select-wrapper ty-languages">
            {foreach from=$languages key=code item=language}
                <a href="{$config.current_url|fn_link_attach:"sl=`$language.lang_code`"}" title="{__("change_language")}" class="ty-languages__item{if $format == "icon"} ty-languages__icon-link{/if}{if $smarty.const.DESCR_SL == $code} ty-languages__active{/if}"><i class="ty-flag ty-flag-{$language.country_code|lower}"></i>{if $format == "name"}{$language.name}{/if}</a>
            {/foreach}
        </div>
	<!--languages_{$block.block_id}--></div>
	{/if}
	
	{if $currencies && $currencies|count > 1 && $block.properties.currency == "Y"}
	<div id="currencies_{$block.block_id}" class="clearfix ath_mega-menu__mob-setting ath_mega-menu__mob-setting--currencies">
		<h4>{__("currencies")}:</h4>

        <div class="ty-currencies">
            {if $text}<div class="ty-currencies__txt">{$text}:</div>{/if}
            {foreach from=$currencies key=code item=currency}
                <a href="{$config.current_url|fn_link_attach:"currency=`$code`"|fn_url}" rel="nofollow" class="ty-currencies__item {if $secondary_currency == $code}ty-currencies__active{/if}">{if $format == "name"}{$currency.description}&nbsp;({$currency.symbol nofilter}){else}{$currency.symbol nofilter}{/if}</a>
            {/foreach}
        </div>
	<!--currencies_{$block.block_id}--></div>
	{/if}
	
</div>
{* //Settings *}

</div>

<div class="ath_mega-menu-mask" id="MegaMenuMask_{$block.block_id}">
	<i class="ty-icon-cancel-circle"></i>
</div>

{/strip}
{/if}

<script>

	// Mega menu
	var MegaMenu = document.getElementById('MegaMenu_{$block.block_id}' ),
		MegaMenuOpenBtn = document.getElementById('MegaMenuOpenBtn_{$block.block_id}'),
		MegaMenuMask = document.getElementById('MegaMenuMask_{$block.block_id}'),
		Body = document.body;
	
	mobileMenuShow = function() {
		classie.toggle( MegaMenu, 'mega_menu--open' );
		classie.toggle( Body, 'mega_menu--active' );
		$('.ath_mega-menu li').removeClass('active');
	};	

	MegaMenuMask.onclick = function() {
		mobileMenuShow();
	};
	
	MegaMenuOpenBtn.onclick = function() {
		mobileMenuShow();
	};
	
	// Child
	$('.ath_mega-menu__back').click(function() {
		$(this).parent().parent('li').removeClass('active');
	});
	$('.ath_mega-menu-have-child--icon').click(function() {
		$(this).parents('.mm1').addClass('active');
		return false;
	});	
	$('.ath_mega-menu__full_tree__l1__icon--child').click(function() {
		$(this).parents('.mm2').addClass('active');
		return false;
	});

{*
	//touche	
	$('.touchevents .ath_mega-menu__l1--drop-down-Y .ath_mega-menu__l1__link').click(function() {
		if ($(window).width() >= 768) {
			return false;
		}
	});
	$('.touchevents .ath_mega-menu__full_tree__l1 .ath_mega-menu__full_tree__l1__link').click(function() {
		if ($(window).width() >= 768) {
			return false;
		}
	});
*}
	
	//touche
	initTouche = function() {
		$('.touchevents .ath_mega-menu__l1--drop-down-Y .ath_mega-menu__l1__link').click(function() {
			if ($(window).width() >= 768) {
				return false;
			}
		});
		$('.touchevents .ath_mega-menu__full_tree__l1--subcategories .ath_mega-menu__full_tree__l1__link').click(function() {
			
			if ($(window).width() >= 768) {
				
				rootCategoryLink = $(this).parent('.maintainHover');
				
				if (rootCategoryLink.hasClass('goToCat')) {
					rootCategoryLink.removeClass('goToCat');
					return true;
				} else {
					rootCategoryLink.addClass('goToCat');
				}
			
				return false;
			}
		});
	};
	
	initTouche();
	
	
	//menu like amazon.com
	var $menuTree = $('.ath_mega-menu__full_tree');

	if ($menuTree.length) {
	    $menuTree.menuAim({
	        activate: activateSubmenu,
	        deactivate: deactivateSubmenu,
	        exitMenu: exitMenu,
	        
	        {* rtl *}
			{if $language_direction == "rtl"}
				submenuDirection: "left",
			{/if}	        
	    });
	    $menuTree.mouseleave(function() {
		    $('.goToCat').removeClass("goToCat");
			$('.maintainHover').removeClass("maintainHover");
	    });
    };

    function activateSubmenu(row) {
        $(row).addClass('maintainHover');
    }
    function deactivateSubmenu(row) {
        $(row).removeClass('maintainHover');
        $('.goToCat').removeClass("goToCat");
    }
    function exitMenu(row) {
	    $(row).removeClass('maintainHover');
	    $('.goToCat').removeClass("goToCat");
	    return true;
    }
    
</script>
