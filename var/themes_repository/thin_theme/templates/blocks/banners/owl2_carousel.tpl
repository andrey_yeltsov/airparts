{** block-description: owl22_carousel **}

{if $items}
    <div id="banner_slider_{$block.snapping_id}" class="banners owl2-carousel">
        {foreach from=$items item="banner" key="key"}
            <div class="ty-banner__image-item">
                {if $banner.type == "G" && $banner.main_pair.image_id}
                    {if $banner.url != ""}<a class="banner__link" href="{$banner.url|fn_url}" {if $banner.target == "B"}target="_blank"{/if}>{/if}
                        {include file="common/image.tpl" images=$banner.main_pair class="ty-banner__image" lazy_load=false}
                    {if $banner.url != ""}</a>{/if}
                {else}
                    <div class="ty-wysiwyg-content">
                        {$banner.description nofilter}
                    </div>
                {/if}
            </div>
        {/foreach}
    </div>
{/if}

<script type="text/javascript">

$(document).ready(function(){
	$('#banner_slider_{$block.snapping_id}').owl2Carousel({
		loop:true,
		
		{if $block.properties.animate_in != 'NoN'}
			animateIn: '{$block.properties.animate_in}',
		{/if}
		{if $block.properties.animate_out != 'NoN'}
			animateOut: '{$block.properties.animate_out}',
		{/if}

		items:1,

		smartSpeed:450,
		
		{if $block.properties.dot == 'Y'}
			dots:true,
		{/if}
		
		{if $block.properties.arrows == 'Y'}
			nav:true,
			navText: ['<i class="icon-tt-left"></i>','<i class="icon-tt-right"></i>'],
		{/if}
		
		{if $block.properties.pause_time}
			autoplay:true,
			autoplayTimeout:{$block.properties.pause_time * 1000},
			autoplayHoverPause:true
		{/if}
	});
});

</script>