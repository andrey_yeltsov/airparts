{** block-description:blog.recent_posts_scroller **}

{if $items}

{assign var="obj_prefix" value="`$block.block_id`000"}

{if $block.properties.outside_navigation == "Y"}
    <div class="owl-theme ty-owl-controls">
        <div class="owl-controls clickable owl-controls-outside" id="owl_outside_nav_{$block.block_id}">
            <div class="owl-buttons">
                <div id="owl_prev_{$obj_prefix}" class="owl-prev"><i class="ty-icon-left-open-thin"></i></div>
                <div id="owl_next_{$obj_prefix}" class="owl-next"><i class="ty-icon-right-open-thin"></i></div>
            </div>
        </div>
    </div>
{/if}

<div class="ty-mb-l">
    <div class="ty-blog-recent-posts-scroller">
        <div id="scroll_list_{$block.block_id}" class="owl-carousel ty-scroller-list">

        {foreach from=$items item="page"}
            <div class="ty-blog-recent-posts-scroller__item">
				
				{if $page.main_pair}
	                <a href="{"pages.view?page_id=`$page.page_id`"|fn_url}" class="ty-blog-recent-posts-scroller__item__link">
	                    <div class="th_blog__img-block">
	                        {include file="common/image.tpl" no_ids=true lazy_load=true obj_id=$page.page_id images=$page.main_pair image_width=$addons.ath_thin_theme_settings.blog_img_width|default:386 image_height=$addons.ath_thin_theme_settings.blog_img_height|default:218 }
	                    </div>
		                <div class="th_blog__date">
			                <span class="th_blog__date__day">{$page.timestamp|date_format:"%d"}</span>
			                <span class="th_blog__date__month">{$page.timestamp|date_format:"%b"}</span>
			            </div>
	                </a>
				{else}
	                <div class="th_blog__date">
		                <span class="th_blog__date__day">{$subpage.timestamp|date_format:"%d"}</span>
		                <span class="th_blog__date__month">{$subpage.timestamp|date_format:"%b"}</span>
		            </div>					
                {/if}

                <a href="{"pages.view?page_id=`$page.page_id`"|fn_url}" class="th_blog__post-title__contener">
                    <span class="th_blog__post-title">
                        {$page.page}
                    </span>
					<span class="th_blog__author">{__("by")} {$page.author}</span>
                </a>

            </div>
        {/foreach}

        </div>
    </div>
</div>

{include file="common/scroller_init.tpl" prev_selector="#owl_prev_`$obj_prefix`" next_selector="#owl_next_`$obj_prefix`"}

{/if}