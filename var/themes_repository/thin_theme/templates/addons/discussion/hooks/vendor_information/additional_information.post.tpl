{assign var="discussion" value=$vendor_info.company_id|fn_get_discussion:'M':true}
{if $discussion.average_rating > 0}
    {include file="addons/discussion/views/discussion/components/stars.tpl"
        stars=$discussion.average_rating|fn_get_discussion_rating
        link="companies.view?company_id=`$vendor_info.company_id`"|fn_url
    }
{/if}