{* variables *}
{style src="sizes.less"}
{style src="colors.less"}


{* base *}
{style src="base/base.less"}
{style src="base/buttons.less"}
{style src="base/forms.less"}
{style src="base/tables.less"}
{style src="base/typography.less"}


{* media *}
{style src="media/icon_font.less"}
{style src="media/icons_changes.less"}


{* components *}
{style src="components/dropdown-box.less"}
{style src="components/stars.less"}
{style src="components/labels.less"}
{style src="components/sorting.less"}
{style src="components/pagination.less"}
{style src="components/quantity.less"}
{style src="components/social_buttons.less"}
{style src="components/tabs.less"}
{style src="components/popup.less"}
{style src="components/sections.less"}
{style src="components/quick-view.less"}
{style src="components/owl2.less"}

{if $addons.ath_animate.status != 'A'}
	{style src="components/animate.min.less"}
{/if}

{* blocks *}

{* - top panel *}
{style src="blocks/top-panel__text.less"}

{* - header *}
{style src="blocks/logo.less"}
{style src="blocks/my_account.less"}
{style src="blocks/cart_content.less"}
{style src="blocks/search.less"}
{style src="blocks/menu.less"}

{if $addons.ath_mega_menu.status != 'A'}
	{style src="blocks/mega_menu.less"}
{/if}

{* - footer *}
{style src="blocks/payment_icons.less"}

{* - content *}
{style src="blocks/original_promo.less"} {* banners *}
{style src="blocks/carousel.less"}
{style src="blocks/benefit.less"}
{style src="blocks/subcategories.less"}
{style src="blocks/filters.less"}
{style src="blocks/breadcrumbs.less"}
{style src="blocks/catalog_carousel.less"}

{* - list_templates *}
{style src="blocks/list_templates/small_items.less"}
{style src="blocks/list_templates/grid_simple.less"}
{style src="blocks/list_templates/scroller.less"}
{style src="blocks/list_templates/grid_list.less"}
{style src="blocks/list_templates/products_list.less"}
{style src="blocks/list_templates/compact_list.less"}

{* - product_page *}
{style src="blocks/product_templates/default_template.less"}
{style src="blocks/product_templates/bigpicture_template.less"}

{* - wrappers *}
{style src="blocks/wrappers/mainbox.less"}
{style src="blocks/wrappers/simple.less"}
{style src="blocks/wrappers/footer_general.less"}
{style src="blocks/wrappers/sidebox.less"}


{* sections *}
{style src="sections/grid.less"}
{style src="sections/top_panel.less"}
{style src="sections/header.less"}
{style src="sections/content.less"}
{style src="sections/footer.less"}
{style src="sections/col_3.less"}
{style src="sections/col_4.less"}


{* pages *}
{style src="pages/account.less"}
{style src="pages/orders.less"}
{style src="pages/404.less"}
{style src="pages/cart.less"}
{style src="pages/checkout.less"}
{style src="pages/compare.less"}
{style src="pages/catalog.less"}
{style src="pages/blog.less"}

{if $addons.ath_thin_theme_settings.free_shipping_icon == "Y"}
	{style src="label_shiping.less"}
{/if}
{if $addons.ath_thin_theme_settings.sticky_header_desktop == "Y" || $addons.ath_thin_theme_settings.sticky_header_mobile == "Y"}
	{style src="stikyHeader.less"}
{/if}
{if $addons.ath_thin_theme_settings.mob_menu_left == "Y"}
	{style src="components/mob_men_left.less"}
{/if}

{* Multi-Vendor *}
{style src="multi-vendor.less"}

{* other *}
{style src="other_styles.less"}


{* rtl *}
{if $language_direction == "rtl"}
	{style src="rtl.less"}
{/if}


