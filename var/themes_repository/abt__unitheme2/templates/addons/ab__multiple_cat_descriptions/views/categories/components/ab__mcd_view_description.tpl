{if !empty($category_data.ab__mcd_descs) and (empty($smarty.request.page) or $smarty.request.page == 1)}
    <div class="ab__mcd_descs row-fluid">
        <div class="row-fluid">
            {** отображаем Основное описание категории **}
            <div class="span8 ab__mcd_descs_left">
                {foreach $category_data.ab__mcd_descs as $d}
                    {if $d@first}
                        <div class="ty-wysiwyg-content ty-mb-s" {$mh}>{$d.description nofilter}</div>
                    {else}{break}{/if}
                {/foreach}
            </div>

            {** отображаем Доп описания категории **}
            <div class="span8 ab__mcd_descs_right">
                <ul>
                {foreach $category_data.ab__mcd_descs as $d}
                    {if !$d@first}
                        <li>
                            <span id="sw_ab__mcd_{$d@key}" class="cm-combination ty-hand">{$d.title nofilter}</span>
                            <i id="on_ab__mcd_{$d@key}" class="ty-icon-down-open cm-combination"></i>
                            <i id="off_ab__mcd_{$d@key}" class="ty-icon-up-open cm-combination hidden"></i>
                            <div id="ab__mcd_{$d@key}" class="hidden">
                                <div class="ty-wysiwyg-content ty-mb-s" {$mh}>{$d.description nofilter}</div>
                            </div>
                        </li>
                    {/if}
                {/foreach}
                </ul>
            </div>
        </div>
    </div>
{/if}
