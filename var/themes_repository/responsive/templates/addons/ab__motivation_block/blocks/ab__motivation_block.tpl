{assign var="id" value=$ab__mb_id|default:"ab__mb_id_`$block.block_id`"}
{if defined('AJAX_REQUEST') || $addons.ab__motivation_block.load_by_ajax == 'N'}
{assign var="params" value=$smarty.request}
{if !$product && $params.product_id}
{$product = $params.product_id|fn_get_product_data:$smarty.session.auth}
{/if}
{$params.category_id = $product.main_category}
{if "MULTIVENDOR"|fn_allowed_for && $product.company_id}
{$params.company_id = $product.company_id}
{else}
{$params.company_id = fn_get_runtime_company_id()}
{/if}
{$ab__mb_items = $params|fn_ab__mb_get_motivation_items|reset}
{/if}
<div class="ab__motivation_block{if $ab__mb_items} loaded{/if}" data-ca-product-id="{$product.product_id}" data-ca-result-id="{$id}">
<div id="{$id}">
{if $ab__mb_items}
<div class="ab__mb_items{if $addons.ab__motivation_block.bg_color !="#ffffff"} colored{/if}" style="background-color: {$addons.ab__motivation_block.bg_color}">
{foreach $ab__mb_items as $ab__mb_item}
<div class="ab__mb_item">
<div id="sw_{$id}_{$ab__mb_item.motivation_item_id}" class="ab__mb_item-title cm-combination{if $ab__mb_item.expanded == 'Y'} open{/if}">
{if $ab__mb_item.icon_type == 'img' && $ab__mb_item.main_pair}
{include file="common/image.tpl" images=$ab__mb_item.main_pair}
{elseif $ab__mb_item.icon_type == 'icon' && $ab__mb_item.icon_class}
<i class="{$ab__mb_item.icon_class} ab__mb_item-icon" style="color:{$ab__mb_item.icon_color}"></i>
{/if}
<div class="ab__mb_item-name">{$ab__mb_item.name}</div>
</div>
<div id="{$id}_{$ab__mb_item.motivation_item_id}" class="ab__mb_item-description ty-wysiwyg-content"{if $ab__mb_item.expanded != 'Y'} style="display: none;"{/if}>
{if $addons.ab__motivation_block.description_type == 'smarty'}
{eval_string var=$ab__mb_item.description}
{else}
{$ab__mb_item.description nofilter}
{/if}
</div>
</div>
{/foreach}
</div>
{/if}
<!--{$id}--></div>
</div>
