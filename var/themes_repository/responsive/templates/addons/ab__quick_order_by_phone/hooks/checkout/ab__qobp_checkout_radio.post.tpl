{if $ab__qobp_show_form == 'Y'}
<li class="ty-checkout__register-methods-item">
<input class="ty-checkout__register-methods-radio" type="radio" id="ab__quick_order_by_phone_ab__qobp" name="checkout_type" value=""/>
<label for="ab__quick_order_by_phone_ab__qobp">
<span class="ty-checkout__register-methods-title">{__("ab__qobp.checkout_method")}</span>
<span class="ty-checkout__register-methods-hint">{__("ab__qobp.checkout_method_info")}</span>
</label>
</li>
{/if}