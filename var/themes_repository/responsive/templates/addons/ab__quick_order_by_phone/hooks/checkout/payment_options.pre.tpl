{** Для отображения на странице корзины **}
{if $ab__qobp_show_form == 'Y'}
<form action="{""|fn_url}" method="post" name="ab__qobp_cart">
<div class="ab__qobp_cart">
{include file="addons/ab__quick_order_by_phone/views/components/product_form.tpl" mode="place_order"}
</div>
</form>
<div class="clearfix"></div>
{/if}
