{capture name="ab__qobp_form"}{include file="addons/ab__quick_order_by_phone/views/components/product_form.tpl" mode="add"}{/capture}
<script type="text/javascript">
(function (_, $) {
_.tr('ab__qobp.error_phone', '{__("ab__qobp.error_phone")|escape:"javascript"}');
_.tr('ab__qobp_policy_description', '{__("ab__qobp_policy_description")|escape:"javascript"}');
$.extend(_, {
ab__qobp: {
form: '{$smarty.capture.ab__qobp_form|escape:"javascript" nofilter}',
settings: {$addons.ab__quick_order_by_phone|json_encode nofilter},
rus_personal_data_processing: '{$addons.rus_personal_data_processing.status|default:"D"}'
}
});
function fn_ab__qobp_check_phone ( phone ){
{literal} if (!/^([\+]+)*[0-9\x20\x28\x29\-]{10,20}$/.test(phone)){ {/literal}
$.ceNotification('show', { type: 'E', title: _.tr('error'), message: _.tr('ab__qobp.error_phone'),message_state: 'I' } );
return false;
}
return true;
}
function fn_ab__qobp_display_form(context) {
var block = $('div.ab__qobp_product', context);
if (block.length && !context.find('.ab__qobp_content')){
block.append(_.ab__qobp.form);
}
}
$(document).on('click', '.ab__qobp_phone', function() {
var ab__qobp_content = $(this).parent();
if (!ab__qobp_content.hasClass('ab__qobp_show_agreement') && _.ab__qobp.rus_personal_data_processing === 'A' && _.ab__qobp.settings.jointly_with_personal_data === 'Y') {
ab__qobp_content.addClass('ab__qobp_show_agreement').append('<div class="ab__qobp_agreement">' + _.tr('ab__qobp_policy_description') + '</div>');
var ab__qobp_agreement = ab__qobp_content.find('.ab__qobp_agreement');
var height = ab__qobp_agreement.outerHeight(true);
ab__qobp_agreement.css( { height: "0px", opacity: 0 } ).animate( { height: height + 'px', opacity: 1 } , 200);
}
});
$.ceEvent('on', 'ce.commoninit', function (context) {
if (typeof _.ab__qobp !== "undefined"
&& context.find('input[name^="ab__qobp[product_amount]"]').length
&& _.ab__qobp.settings.show_on_product_page == "Y"
){
if ((_.ab__qobp.settings.only_in_stock == "N")
||
(_.ab__qobp.settings.only_in_stock == "Y" && context.find('input[name^="ab__qobp[product_amount]"]').val() > 0)
){
fn_ab__qobp_display_form(context);
}
}
});
$(document).on('click', 'button.ty-btn__ab__qobp', function() {
var button = $(this);
$.ceEvent('one', 'ce.formpre_' + $(this).parents('form').prop('name'), function(form, elm) {
return fn_ab__qobp_check_phone(button.parent().find('input[name^="ab__qobp_data[phone]"]').val().trim());
});
$.ceEvent('one', 'ce.formajaxpost_' + $(this).parents('form').prop('name'), function(form, elm) {
if (typeof form.ab__qobp_result !== "undefined" && form.ab__qobp_result !== null && form.ab__qobp_result == 'Y'){
button.parent().parent().animate( { height: "0px", opacity: 0 } , 200, function() { $(this).remove(); } );
$.ceEvent('trigger', 'ce.ab__qobp_order_complete', [form.ab__qobp_order]);
}
});
});
$(document).on('click', 'button[type=submit][name^="dispatch[ab__qobp.place_order"]', function() {
$.ceEvent('on', 'ce.formpre_' + $(this).parents('form').prop('name'), function(form, elm) {
return fn_ab__qobp_check_phone($('input[name^="ab__qobp_data[phone]"]').val().trim());
});
});
$(document).on('click', 'input.ty-checkout__register-methods-radio[type=radio]', function(e){
$('#ab__qobp_checkout').addClass('hidden');
if ($(this).prop('id') == 'ab__quick_order_by_phone_ab__qobp'){
$('#anonymous_checkout,#register_checkout').hide();
$('#ab__qobp_checkout').removeClass('hidden');
}
});
}(Tygh, Tygh.$));
</script>
<script type="text/javascript">
(function (_, $) {
$.ceEvent('on', 'ce.ab__qobp_order_complete', function(ab__qobp_order){
var order = {
order_id : 0,
order_total : 0,
currency : '',
items : [],
};
order.order_id = ab__qobp_order.order_id;
order.order_total = parseFloat(ab__qobp_order.total);
order.currency = ab__qobp_order.secondary_currency;
for (var i in ab__qobp_order.products) {
var p = ab__qobp_order.products[i];
var item = {
id : p.product_id,
name : p.product,
code : p.product_code,
price : parseFloat(p.price),
quantity: parseInt(p.amount),
};
order.items.push(item);
}
});
}(Tygh, Tygh.$));
</script>