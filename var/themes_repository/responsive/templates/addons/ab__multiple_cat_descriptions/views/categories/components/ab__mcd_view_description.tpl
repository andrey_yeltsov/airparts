{strip}
{if !empty($category_data.ab__mcd_descs) and (empty($smarty.request.page) or $smarty.request.page == 1)}
<div class="ab__mcd_descs">
{assign var="mh" value=""}
{if $addons.ab__multiple_cat_descriptions.max_height != "disabled"}
{assign var="mh" value="max-height: {$addons.ab__multiple_cat_descriptions.max_height|default:500}px; overflow-y:auto;"}
{/if}
{foreach from=$category_data.ab__mcd_descs item='d' name='d'}
{assign var="first" value=$smarty.foreach.d.first}
<div class="ab__mcd_descs-section">
<div class="ab__mcd_descs-section-title {if $first}active{/if}" tab_content="c_{$smarty.foreach.d.iteration}">{$d.title nofilter}</div>
<div class="ab__mcd_descs-section-content ty-wysiwyg-content ty-mb-s c_{$smarty.foreach.d.iteration} {if $first}open{/if}" style="{if $first}display: block;{/if}{$mh}">{$d.description nofilter}</div>
</div>
{/foreach}
</div>
{/if}
{/strip}