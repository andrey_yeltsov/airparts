<div class="control-group">
    <label class="control-label">{__("menu_icon")}:</label>
    <div class="controls">
        {include file="common/attach_images.tpl" image_name="mega_m_category_icon" image_object_type="mega_m_category_icon" image_pair=$category_data.mega_m_category_icon image_object_id=$id no_detailed="Y" hide_titles="Y" hide_alt="Y"}
    </div>
</div>

<div class="control-group mm_svg_icon">    
	<label for="svg_icon" class="control-label">
    	{__("svg_icon")}
	    <i class="cm-tooltip icon-question-sign" title="{__("svg_icon_from")}"></i>:
    </label>
    <div class="controls">
	    <div class="mm_svg_icon_preview">{$category_data.mega_m_category_svg_icon nofilter}</div>
        <textarea id="svg_icon_{$id}" name="category_data[mega_m_category_svg_icon]" cols="55" rows="4" class="input-large">{$category_data.mega_m_category_svg_icon}</textarea>
    </div>
</div>

<div class="control-group">
    <label class="control-label">{__("mega_m_category_banner")}:</label>
    <div class="controls">
        {include file="common/attach_images.tpl" image_name="mega_m_category_banner" image_object_type="mega_m_category_banner" image_pair=$category_data.mega_m_category_banner image_object_id=$id no_detailed="Y" hide_titles="Y"}
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="mega_m_category_banner_url">{__("mega_m_category_banner_url")}:</label>
    <div class="controls">
        <input type="text" name="category_data[mega_m_category_banner_url]" id="mega_m_category_banner_url" size="55" value="{$category_data.mega_m_category_banner_url}" class="input-large" />
    </div>
</div>