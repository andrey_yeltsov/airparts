{if $addons.ath_mega_menu.status != 'A' }

	{if ($runtime.company_id && "ULTIMATE"|fn_allowed_for) || "MULTIVENDOR"|fn_allowed_for}
	{include file="common/subheader.tpl" title=__("mega_menu") target="#megamenu_category_setting"}
	
	<fieldset>
		<div id="megamenu_category_setting" class="in collapse">
			{include file="addons/ath_thin_theme_settings/components/category_setting.tpl" prefix="category_data" object_id=$category_data.category_id object_type="C" title=__("megamenu_category_setting")}
		</div>
	</fieldset>
	{/if}
	
{/if}