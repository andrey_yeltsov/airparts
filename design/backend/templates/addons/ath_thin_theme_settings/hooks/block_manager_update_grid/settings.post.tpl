{if $runtime.layout.theme_name == "thin_theme"}
	{if !$grid.parent_id && ($grid.width + $grid.offset == $runtime.layout.width) }
		<div class="control-group cm-no-hide-input">
			<label class="control-label" for="elm_grid_section_class_{$id}">{__("section_class")}</label>
			<div class="controls">
			<input id="elm_grid_section_class_{$id}" name="section_class" value="{$grid.section_class}" type="text" />
			</div>
		</div>
		
		<div class="control-group cm-no-hide-input">
			<label class="control-label">{__("full_width")}</label>
			<div class="controls">
				<label class="radio inline" for="elm_grid_section_width_{$id}_n"><input type="radio" name="section_width" id="elm_grid_section_width_{$id}_n" {if $grid.section_width == "S"}checked="checked"{/if} value="S" />{__("no")}</label>
				<label class="radio inline" for="elm_grid_section_width_{$id}_y"><input type="radio" name="section_width" id="elm_grid_section_width_{$id}_y" {if $grid.section_width == "F"}checked="checked"{/if} value="F" />{__("yes")}</label>
			</div>
		</div>
	{/if}
{/if}