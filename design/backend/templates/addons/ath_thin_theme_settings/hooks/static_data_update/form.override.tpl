{hook name="static_data_update:form"}
<form action="{""|fn_url}" method="post" name="static_data_form_{$id}" enctype="multipart/form-data" class=" form-horizontal">
<input name="section" type="hidden" value="{$section}" />
<input name="param_id" type="hidden" value="{$id}" />

<div class="tabs cm-j-tabs">
    <ul class="nav nav-tabs">
        <li id="tab_general_{$id}" class="cm-js active"><a>{__("general")}</a></li>
    </ul>
</div>

<div class="cm-tabs-content" id="content_tab_general_{$id}">
<fieldset>

    {if $section_data.owner_object}
        {assign var="param_name" value=$section_data.owner_object.param}
        {assign var="request_key" value=$section_data.owner_object.key}    
        {assign var="value" value=$smarty.request.$request_key}

        {*if $static_data[$param_name]}
            {assign var="value" value=$static_data[$param_name]}        
        {/if*}
        
        <input type="hidden" name="static_data[{$param_name}]" value="{$value}" class="input-text-large" />
        <input type="hidden" name="{$request_key}" value="{$value}" class="input-text-large" />
    {/if}

    {if $section_data.multi_level}
    <div class="control-group">
        <label for="parent_{$id}" class="cm-required control-label">{__("parent_item")}:</label>
            <div class="controls">
                <select id="parent_{$id}" name="static_data[parent_id]" class="ath_parent_item" data-id="{$id}">
                    <option value="0">- {__("root_level")} -</option>
                    {foreach from=$parent_items item="i"}
                        {if ($i.id_path|strpos:"`$static_data.id_path`/" === false || $static_data.id_path == "") && $i.param_id != $static_data.param_id || !$id}
                            <option value="{$i.param_id}" {if $static_data.parent_id == $i.param_id}selected="selected"{/if}>{$i.descr|escape|indent:$i.level:"&#166;&nbsp;&nbsp;&nbsp;&nbsp;":"&#166;--&nbsp;" nofilter}</option>
                        {/if}
                    {/foreach}
                </select>
            </div>
    </div>
    {/if}
    
 
    
{*  TH Mega menu options  *}
{* leve 1 *}
<div id="ath_level__1__{$id}" class="ath_mm_settings{if $static_data.parent_id} hidden{/if}">
    <div class="control-group ">
        <label for="content_type_{$id}_l1" class="control-label">{__("content_type")}:</label>
            <div class="controls">
                <select id="content_type_{$id}_l1" name="static_data[content_type_l1]" class="sub_item__type__select" data-id="{$id}">
	                <option value="" {if $static_data.content_type_l1 == ""}selected{/if}>{__("default")}</option>
	                <option value="full_tree" {if $static_data.content_type_l1 == "full_tree"}selected{/if}>{__("full_tree")}</option>
                </select>
            </div>
    </div>
    
    <div class="control-group control-group--drop_down_type{if $static_data.content_type_l1 == "full_tree"} hidden{/if}" id="ath_mm_setting__drop_down_type__{$id}">
        <label for="drop_down_type_{$id}" class="control-label">{__("drop_down_type")}:</label>
            <div class="controls">
                <select id="drop_down_type_{$id}" name="static_data[drop_down_type]" class="sub_item__drop_down_type__select" data-id="{$id}">
	                <option value="columns" {if $static_data.drop_down_type == "columns"}selected{/if}>{__("columns")}</option>
	                <option value="list" {if $static_data.drop_down_type == "list"}selected{/if}>{__("list")}</option>
                </select>
            </div>
    </div>
    
    <div class="control-group">
        <label for="float_right_{$id}" class="control-label">{__("float_right")}</label>
        <div class="controls">
            <input type="hidden" name="static_data[float_right]" value="N" />
            <input type="checkbox" name="static_data[float_right]" id="float_right_{$id}" {if $static_data.float_right == "Y"}checked="checked"{/if} value="Y">
        </div>
	</div>
	
    {if $section_data.icon}
    <div class="control-group control-group--icon{if $static_data.content_type_l1 == "full_tree"} hidden{/if}" id="ath_mm_setting__icon__{$id}">
        <label class="control-label">{__("icon")}:</label>
        <div class="controls">
            {include file="common/attach_images.tpl" image_name="static_data_icon" image_object_type="static_data_icon" image_pair=$static_data.icon no_detailed="Y" hide_titles="Y" hide_alt="Y" image_key=$id image_object_id=$id}
        </div>
    </div>
    {/if}
    
    <div class="control-group control-group--svg-icon{if $static_data.content_type_l1 == "full_tree"} hidden{/if}" id="ath_mm_setting__svg_icon__{$id}">
	    <label for="svg_icon" class="control-label">
	    	{__("svg_icon")}
		    <i class="cm-tooltip icon-question-sign" title="{__("svg_icon_from")}"></i>:
	    </label>
        <div class="controls">
	        <div class="mm_svg_icon_preview">{$static_data.svg_icon nofilter}</div>
            <textarea id="svg_icon_{$id}" name="static_data[svg_icon]" cols="55" rows="4" class="input-large mm_svg_icon_text">{$static_data.svg_icon}</textarea>
        </div>
    </div>
    
</div>

{* level 2 *}
<div id="ath_level__2__{$id}" class="ath_mm_settings{if !$static_data.parent_id} hidden"{/if}>
    <div class="control-group">
        <label for="content_type_{$id}_l2" class="control-label">{__("content_type")}:</label>
            <div class="controls">
                <select id="content_type_{$id}_l2" name="static_data[content_type]" class="sub_item__type__select" data-id="{$id}">
	                <option value="" {if $static_data.content_type == ""}selected{/if}>{__("default")}</option>
{* 	                <option value="banner" {if $static_data.content_type == "banner"}selected{/if}>{__("banner")}</option> *}
	                <option value="wysiwyg" {if $static_data.content_type == "wysiwyg"}selected{/if}>{__("wysiwyg")}</option>
                </select>
            </div>
    </div>
</div>

<hr>

{*  /TH Mega menu options  *}


    <div class="control-group">
        <label for="descr_{$id}" class="cm-required control-label">{__($section_data.descr)}:</label>
        <div class="controls">
            <input type="text" size="40" id="descr_{$id}" name="static_data[descr]" value="{$static_data.descr}" class="input-text-large main-input">
        </div>
    </div>
    {if $section_data.multi_level}
    <div class="control-group">
        <label for="position_{$id}" class="control-label">{__("position_short")}:</label>
        <div class="controls">
            <input type="text" size="2" id="position_{$id}" name="static_data[position]" value="{$static_data.position}" class="input-text-short">
        </div>
    </div>
    {/if}
    
    <div class="control-group control-group--url{if $static_data.content_type == "wysiwyg" || $static_data.content_type_l1 == "full_tree"} hidden{/if}" id="ath_mm_setting__url__{$id}">
        <label for="param_{$id}" class="control-label">{__($section_data.param)}{if $section_data.tooltip}{include file="common/tooltip.tpl" tooltip=__($section_data.tooltip)}{/if}:</label>
        <div class="controls">
            <input type="text" size="40" id="param_{$id}" name="static_data[param]" value="{$static_data.param}" class="input-text-large">
        </div>
    </div>

{*  TH Mega menu options by type  *}
{* leve 1 *}
{* 	full_tree *}
{*
    <div class="control-group ath_mm_setting ath_mm_setting--full_tree{if $static_data.content_type_l1 != "full_tree"} hidden{/if}" id="ath_mm_setting__full_tree__{$id}">
        <label for="full_tree_number_categories_{$id}" class="control-label">{__("full_tree_number_categories")}:</label>
        <div class="controls">
            <input type="text" size="2" id="position_{$id}" name="static_data[full_tree_number_categories]" value="{$static_data.full_tree_number_categories|default:7}" class="input-text-short">
        </div>
    </div>
*}
{* leve 2 *}
{* 	banner *}
    <div class="control-group ath_mm_setting ath_mm_setting--banner{if $static_data.content_type != "banner"} hidden{/if}" id="ath_mm_setting__banner__{$id}">
        <label class="control-label">{__("banner")}:</label>
        <div class="controls">
            {include file="common/attach_images.tpl" image_name="static_data_main_pair" image_object_type="static_data_main_pair" image_pair=$static_data.main_pair no_thumbnail=true hide_titles=true}
        </div>
    </div>
{* 	wysiwyg *}
    <div class="control-group ath_mm_setting ath_mm_setting--wysiwyg{if $static_data.content_type != "wysiwyg"} hidden{/if}" id="ath_mm_setting__wysiwyg__{$id}">
        <label class="control-label">{__("wysiwyg")}:</label>
        <div class="controls">
            <textarea name="static_data[wysiwyg]" cols="55" rows="8" class="cm-wysiwyg input-large">{$static_data.wysiwyg}</textarea>
        </div>
    </div>

{*  /TH Mega menu options  by type *}

    {if $section_data.additional_params}
    {foreach from=$section_data.additional_params key="k" item="p"}
        {if $p.type == "hidden"}    
            <input type="hidden" id="param_{$k}_{$id}" name="static_data[{$p.name}]" value="{$static_data[$p.name]}" class="input-text-large" />
        {else}
            <div class="control-group">
	            
	            {if $p.type != "megabox"}
                <label for="param_{$k}_{$id}" class="control-label">{__($p.title)}{if $p.tooltip}{include file="common/tooltip.tpl" tooltip=__($p.tooltip)}{/if}:</label>
                <div class="controls mixed-controls cm-bs-group">
                {/if}
                    {if $p.type == "checkbox"}
                        <input type="hidden" name="static_data[{$p.name}]" value="N" />
                        <input type="checkbox" id="param_{$k}_{$id}" name="static_data[{$p.name}]" value="Y" {if $static_data[$p.name] == "Y"}checked="checked"{/if} class="checkbox" />
                    {elseif $p.type == "megabox"}
{* ~~~~~~~~~~~~~~~ default ~~~~~~~~~~~~~~~ *}
                    <div class="ath_mm_setting ath_mm_setting--default{if ($static_data.content_type_l1 != "" && !$static_data.parent_id) || ($static_data.content_type != "" && $static_data.parent_id)} hidden{/if}" id="ath_mm_setting__default__{$id}">
	                    <label for="param_{$k}_{$id}" class="control-label">{__($p.title)}{if $p.tooltip}{include file="common/tooltip.tpl" tooltip=__($p.tooltip)}{/if}:</label>
                        {assign var="_megabox_values" value=$static_data[$p.name]|fn_static_data_megabox}
                        <div class="controls mixed-controls cm-bs-group">
                        <div class="cm-bs-container form-inline clearfix">
                            <label class="radio pull-left">
                                <input type="radio" class="cm-bs-trigger" name="static_data[megabox][type][{$p.name}]" {if !$_megabox_values}checked="checked"{/if} value="" onclick="Tygh.$('#un_{$id}').prop('disabled', true);">
                                {__("none")}
                            </label>
                        </div>
                        
                        <div class="cm-bs-container form-inline clearfix">
                            <label class="radio pull-left">
                                <input type="radio" class="cm-bs-trigger" name="static_data[megabox][type][{$p.name}]" {if $_megabox_values.types.C}checked="checked"{/if} value="C" onclick="Tygh.$('#un_{$id}').prop('disabled', false);">
                                {__("category")}:
                            </label>
                            <div class="cm-bs-block pull-left disable-overlay-wrap">
                                {include file="pickers/categories/picker.tpl" data_id="megabox_category_`$id`" input_name="static_data[`$p.name`][C]" item_ids=$_megabox_values.types.C.value hide_link=true hide_delete_button=true default_name=__("all_categories") extra=""}
                                <div class="disable-overlay cm-bs-off"></div>
                            </div>
                        </div>
                
                        <div class="cm-bs-container form-inline clearfix">
                            <label class="radio pull-left">
                                <input type="radio" class="cm-bs-trigger" name="static_data[megabox][type][{$p.name}]" {if $_megabox_values.types.A}checked="checked"{/if} value="A" onclick="Tygh.$('#un_{$id}').prop('disabled', false);">
                                {__("page")}:
                            </label>
                            <div class="cm-bs-block pull-left disable-overlay-wrap">
                                {include file="pickers/pages/picker.tpl" data_id="megabox_page_`$id`" input_name="static_data[`$p.name`][A]" item_ids=$_megabox_values.types.A.value hide_link=true hide_delete_button=true default_name=__("all_pages") extra="" no_container=true prepend=true}
                                <div class="disable-overlay cm-bs-off"></div>
                            </div>
                        </div>
                        <br />
                        <label for="un_{$id}" class="checkbox clearfix">
                            <input type="hidden" name="static_data[megabox][use_item][{$p.name}]" value="N" />
                            <input type="checkbox" name="static_data[megabox][use_item][{$p.name}]" id="un_{$id}" {if $_megabox_values.use_item == "Y"}checked="checked"{/if} value="Y" {if !$_megabox_values}disabled="disabled"{/if}>{__("static_data_use_item")}
                        </label>
                    </div>
                    {elseif $p.type == "select"}
                        <select id="param_{$k}_{$id}" name="static_data[{$p.name}]">
                        {foreach from=$p.values key="vk" item="vv"}
                        <option    value="{$vk}" {if $static_data[$p.name] == $vk}selected="selected"{/if}>{__($vv)}</option>
                        {/foreach}
                        </select>
                    {elseif $p.type == "input"}
                        <input type="text" id="param_{$k}_{$id}" name="static_data[{$p.name}]" value="{$static_data[$p.name]}" class="input-text-large" />
                    {/if}
                    
                {if $p.type != "megabox"}
                	</div>
                {/if}
                    
            </div>        
        {/if}
    {/foreach}
    {/if}

    {if $section_data.has_localization}
        {include file="views/localizations/components/select.tpl" data_name="static_data[localization]" data_from=$static_data.localization}
    {/if}
</fieldset>
<!--content_tab_general_{$id}--></div>

{if ""|fn_allow_save_object:"static_data":$section_data.skip_edition_checking}
    <div class="buttons-container">
        {include file="buttons/save_cancel.tpl" but_name="dispatch[static_data.update]" cancel_action="close" save=$id}
    </div>
{/if}

</form>

{*  TH Mega menu js  *}
<script>
	function fn_mm_show_options() {
		$('.ath_parent_item').change(function() {
	    	if (this.value != 0) {
		    	$('#ath_level__1__' + $(this).data('id')).hide();
		    	$('#ath_level__2__' + $(this).data('id')).show();
	    	} else {
		    	$('#ath_level__2__' + $(this).data('id')).hide();
		    	$('#ath_level__1__' + $(this).data('id')).show();
		    }
		    
		    $('.sub_item__type__select').val(0);
		    $('.ath_mm_setting').hide();
		    $('#ath_mm_setting__default__' + $(this).data('id')).show();
		});
	}
	
	function fn_mm_show_options_by_type() {
		$('.sub_item__type__select').change(function() {			
			$('.ath_mm_setting').hide();
			$('#ath_mm_setting__url__'+ $(this).data('id')).show();
			$('#ath_mm_setting__icon__'+ $(this).data('id')).show();
			$('#ath_mm_setting__drop_down_type__'+ $(this).data('id')).show();
			if (this.value != 0) {
		    	$('#ath_mm_setting__' + this.value + '__' + $(this).data('id')).show();
		    	
		    	if (this.value == 'wysiwyg') {
			    	$('#ath_mm_setting__url__'+ $(this).data('id')).hide();
			    }
		    	if (this.value == 'full_tree') {
			    	$('#ath_mm_setting__url__'+ $(this).data('id')).hide();
			    	$('#ath_mm_setting__icon__'+ $(this).data('id')).hide();
			    	$('#ath_mm_setting__drop_down_type__'+ $(this).data('id')).hide();
		    	}
		    	
	    	} else {
				$('#ath_mm_setting__default__' + $(this).data('id')).show();
		    }
		});
	}
	
	function fn_mm_init() {
		fn_mm_show_options();
		fn_mm_show_options_by_type();
	};
	
	    
    $(document).ready(function() {
		fn_mm_init();
	});
	$(document).ajaxComplete(function(event,request, settings) {
		fn_mm_init();
	});
 
</script>
{*  /TH Mega menu js  *}
{/hook}
