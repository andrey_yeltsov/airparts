{capture name="desc_add"}
{include file="addons/ab__multiple_cat_descriptions/views/ab__mcd_descs/update.tpl"}
{/capture}
<div class="btn-toolbar clearfix cm-toggle-button">
<div class="pull-right shift-left">
{include file="common/popupbox.tpl"
id="desc_add"
content=$smarty.capture.desc_add
text=__("ab__mcd.popup.add")
act="create"
but_text=__("ab__mcd.popup.add")
icon='icon-plus'
}
</div>
</div>
{if !empty($ab__mcd_descs) and is_array($ab__mcd_descs)}
<table class="table table-middle cm-progressbar-status" width="100%">
<thead class="cm-first-sibling">
<tr>
<th width="5%">{__("ab__mcd.position")}</th>
<th width="15%">{__("ab__mcd.title")}</th>
<th width="5%">&nbsp;</th>
<th width="5%">&nbsp;</th>
</tr>
</thead>
<tbody>
{foreach from=$ab__mcd_descs item="i" key="k" name="ab__mcd_descs"}
<tr class="cm-row-item">
<td>{$i.position}</td>
<td>{$i.title}</td>
<td class="right nowrap">
<div class="hidden-tools">
{capture name="tools_list"}
<li>{include file="common/popupbox.tpl"
id="desc_`$k`"
text=__("ab__mcd.popup.edit", ["[name]" => $i.title])
act="edit"
href="ab__mcd_descs.update?desc_id=`$k`&return_url=`$return_url|escape:url`"
}
</li>
<li>{btn type="list" text=__("ab__mcd.update.on_page") href="ab__mcd_descs.update?separate=Y&desc_id=`$k`"}</li>
<li>{btn type="list" text=__("ab__mcd.delete") class="cm-confirm" href="ab__mcd_descs.delete?desc_id=`$k`&return_url=`$return_url|escape:url`"}</li>
{/capture}
{dropdown content=$smarty.capture.tools_list}
</div>
</td>
<td class="right nowrap">
{include file="common/select_popup.tpl" popup_additional_class="dropleft" id=$k status=$i.status hidden=true object_id_name="desc_id" table="ab__mcd_descs" hidden=false}
</td>
</tr>
{/foreach}
</tbody>
</table>
{else}
<p class="no-items">{__("no_data")}</p>
{/if}