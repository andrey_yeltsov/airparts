{$id=$ab__mcd_desc.desc_id|default:0}
{$allow_save=true}
{if $separate}
{script src="js/tygh/tabs.js"}
{capture name="mainbox"}
{/if}
<div id="content_group{$id}">
<form action="{""|fn_url}" method="post" id="update_ab__mcd_descs_form_{$id}" name="update_ab__mcd_descs_form_{$id}" class="form-horizontal form-edit cm-disable-empty-files" enctype="multipart/form-data">
<input type="hidden" name="return_url" value="{$return_url}" />
<input type="hidden" name="desc_id" value="{$id}" />
{if $separate}
<input type="hidden" name="separate" value="Y" >
{/if}
<input type="hidden" name="desc_data[object_id]" value="{$object_id|default:$ab__mcd_desc.object_id}" />
<input type="hidden" name="desc_data[object_type]" value="{$object_type|default:$ab__mcd_desc.object_type}" />
<div class="cm-tabs-content" id="tabs_content_{$id}">
<div id="content_tab_join_{$id}">
<fieldset>
<div class="control-group">
<label class="control-label" for="elm_desc_position_{$id}">{__("ab__mcd.position")}</label>
<div class="controls">
<input type="text" name="desc_data[position]" value="{$ab__mcd_desc.position|default:0}" class="input-micro cm-decimal cm-integer" id="elm_desc_position_{$id}" />
</div>
</div>
<div class="control-group">
<label class="control-label cm-required" for="elm_desc_title_{$id}">{__("ab__mcd.title")}</label>
<div class="controls">
<input class="span8 cm-trim" type="text" name="desc_data[title]" value="{$ab__mcd_desc.title}" id="elm_desc_title_{$id}" />
</div>
</div>
<div class="control-group">
<label class="control-label cm-required" for="elm_desc_description_{$id}">{__("ab__mcd.description")}</label>
<div class="controls">
<textarea class="span8 cm-wysiwyg" type="text" name="desc_data[description]" id="elm_desc_description_{$id}">{$ab__mcd_desc.description}</textarea>
</div>
</div>
{include file="common/select_status.tpl" input_name="desc_data[status]" id="elm_desc_status_{$id}" obj_id=$id obj=$ab__mcd_desc}
</fieldset>
</div>
</div>
<div class="buttons-container">
{if $id}
{if !$allow_save && $shared_product != "Y"}
{assign var="hide_first_button" value=true}
{/if}
{/if}
{if $separate}
{capture name="buttons"}
{include file="buttons/save_cancel.tpl" but_name="dispatch[ab__mcd_descs.update]" but_target_form="update_ab__mcd_descs_form_`$id`" hide_second_button=true save=true}
{/capture}
{else}
{include file="buttons/save_cancel.tpl" but_name="dispatch[ab__mcd_descs.update]" cancel_action="close" extra="" hide_first_button=$hide_first_button save=$id}
{/if}
</div>
</form>
</div>
{if $separate}
{/capture}
{include file="common/mainbox.tpl" title=__("ab__mcd.popup.edit", ["[name]" => $ab__mcd_desc.title]) content=$smarty.capture.mainbox buttons=$smarty.capture.buttons select_languages=true}
{/if}
