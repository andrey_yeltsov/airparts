<div class="control-group">
<label for="abt__yt_mwi__status_{$id}" class="control-label">{__("abt__yt.menu_with_icons.status")}:</label>
<div class="controls">
<input type="hidden" name="static_data[abt__yt_mwi__status]" value="N">
<input type="checkbox" id="abt__yt_mwi__status_{$id}" name="static_data[abt__yt_mwi__status]" value="Y" {if $static_data.abt__yt_mwi__status == 'Y'}checked="checked"{/if}>
</div>
</div>