{if $runtime.layout.theme_name == 'abt__youpitheme'}
<div id="abt__yt_block_settings_{$elm_id}">
<div class="abt-yt-doc">{__('abt__yt.block.availability.show_on',
['[show_on]' => "{__("block_manager.availability.show_on")}: {__("block_manager.availability.phone")}/{__("block_manager.availability.tablet")}/{__("block_manager.availability.desktop")}"])}</div>
</div>
{/if}