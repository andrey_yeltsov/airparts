{__('ab__mb.icons.yt_icons')}
<table class="table table-middle table-responsive ab-mb-table" width="100%">
<thead>
<tr>
<th width="20%">{__("icon")}</th>
<th width="20%">{__("copy")}</th>
<th width="60%">{__("ab__mb.icons.class")}{include file="common/tooltip.tpl" tooltip=__("ab__mb.icons.class.tooltip")}</th>
</tr>
</thead>
<tbody>
{foreach $icons.ypi_social as $icon}
<tr>
<td data-th="{__("icon")}"><span class="ab-mb-icon-example"><i class="s-icon {$icon}"></i></span></td>
<td data-th="{__("copy")}"><a class="ab-mb-copy btn btn-primary">{__("copy")}</a></td>
<td data-th="{__("ab__mb.icons.class")}">s-icon {$icon}</td>
</tr>
{/foreach}
{foreach $icons.ypi_mb_icons as $icon}
<tr class="ab__motivation_block">
<td data-th="{__("icon")}"><span class="ab-mb-icon-example"><i class="material-icons {$icon}"></i></span></td>
<td data-th="{__("copy")}"><a class="ab-mb-copy btn btn-primary">{__("copy")}</a></td>
<td data-th="{__("ab__mb.icons.class")}">material-icons {$icon}</td>
</tr>
{/foreach}
</tbody>
</table>