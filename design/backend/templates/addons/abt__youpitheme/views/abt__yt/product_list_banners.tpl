<div class="control-group">
{$fld="banners_use"}{$elm="abt__yt_$fld"}
<label for="{$d_id}_{$elm}" class="control-label">{__("abt__yt.product_list_banners.{$fld}")}{include file="common/tooltip.tpl" tooltip=__("abt__yt.product_list_banners.{$fld}.tooltip")}:</label>
<div class="controls">
<input type="hidden" name="{$d_name}[{$elm}]" value="N" />
<input type="checkbox" name="{$d_name}[{$elm}]" id="{$d_id}_{$elm}" value="Y" {if $banners_use == "Y"}checked="checked"{/if} />
</div>
</div>
<div class="control-group">
{$fld="banner_max_position"}{$elm="abt__yt_$fld"}
<label for="{$d_id}_{$elm}" class="control-label">{__("abt__yt.product_list_banners.{$fld}")}{include file="common/tooltip.tpl" tooltip=__("abt__yt.product_list_banners.{$fld}.tooltip")}:</label>
<div class="controls">
<select name="{$d_name}[{$elm}]" id="{$d_id}_{$elm}" class="span3">
{foreach array(60,120,180,240,300,360,420,480) as $e}
<option value="{$e}" {if $e == $banner_max_position}selected="selected"{/if}>{$e}</option>
{/foreach}
</select>
</div>
</div>
{if $d_name == "block_data"}<div style="margin-left: -170px;border-top: 1px solid rgb(221, 221, 221);">{/if}
{include file="common/subheader.tpl" meta="" title=__("abt__yt.product_list_banners.params") target="#abt__yt_product_list_banners_params"}
<div id="abt__yt_product_list_banners_params" class="collapsed in">
{include file="addons/abt__youpitheme/views/abt__yt/components/banners_map.tpl"}
<script>
var b2_disabled_position = [{$banner_max_position|fn_abt__yt_get_banner_disabled_position nofilter}];
function set_available_positions(type, e){
var s = e.parent().next().find('select');
if (parseInt(type) == 1){
s.children('option').each(function() {
$(this).attr('disabled', false);
});
}else{
s.children('option').each(function() {
if (b2_disabled_position.indexOf(parseInt($(this).val())) >= 0){
$(this).attr('disabled', true);
}
});
s.val("0");
}
}
</script>
<style>
.abt__yt_product_list_banners select option:disabled{
background-color: #c5c5c5;
color: #c5c5c5;
}
</style>
<table class="table table-middle abt__yt_product_list_banners" width="100%">
<thead class="cm-first-sibling">
<tr>
<th>{__("abt__yt.product_list_banners.params.width")}{include file="common/tooltip.tpl" tooltip=__("abt__yt.product_list_banners.params.width.tooltip")}</th>
<th>{__("abt__yt.product_list_banners.params.position")}{include file="common/tooltip.tpl" tooltip=__("abt__yt.product_list_banners.params.position.tooltip")}</th>
<th>{__("abt__yt.product_list_banners.params.priority")}{include file="common/tooltip.tpl" tooltip=__("abt__yt.product_list_banners.params.priority.tooltip")}</th>
<th>{__("abt__yt.product_list_banners.params.banner_id")}{include file="common/tooltip.tpl" tooltip=__("abt__yt.product_list_banners.params.banner_id.tooltip")}</th>
<th width="8%">&nbsp;</th>
</tr>
</thead>
<tbody>
{$elm="{$d_name}[abt__yt_banners]"}
{$key=0}
{foreach $abt__yt_product_list_banners as $i}
{$key=$i@index}
<tr class="cm-row-item">
{$elm_key="{$d_name}[abt__yt_banners][{$key}]"}
<td>{$field='width'}
<select name="{$elm_key}[{$field}]" class="span1" onchange="set_available_positions(this.value, $(this));">
{$r=array(1,2)}
{if $without_x2}{$r=array(1)}{/if}
{foreach $r as $e}
<option value="{$e}" {if $e == $i.$field}selected="selected"{/if}>{$e}</option>
{/foreach}
</select>
</td>
<td>{$field='position'}
<input type="hidden" name="{$elm_key}[link_id]" value="{$i.link_id}">
<select name="{$elm_key}[{$field}]" class="span1">
<option value="0">---</option>
{foreach range(1,$banner_max_position,1) as $e}
<option value="{$e}" {if $e == $i.$field}selected="selected"{/if}>{$e}</option>
{/foreach}
</select>
</td>
<td>{$field='priority'}
<select name="{$elm_key}[{$field}]" class="span1">
{foreach range(1,5,1) as $e}
<option value="{$e}" {if $e == $i.$field}selected="selected"{/if}>{$e}</option>
{/foreach}
</select>
</td>
<td>{$field='banner_id'}
<select name="{$elm_key}[{$field}]" class="span6">
<option value="0">---</option>
{foreach $abt__yt_banners as $banner_type => $banners}
<optgroup label="{__("abt__yt.banners.types.{$banner_type|lower}")}">
{foreach $banners as $b}
<option value="{$b.banner_id}" {if $b.banner_id == $i.$field}selected="selected"{/if}>{$b.banner}</option>
{/foreach}
</optgroup>
{/foreach}
</select>
{btn type="list" class="hand icon-cog" href="banners.update?banner_id=`$i.$field`&selected_section=abyt" target="_blank"}
</td>
<td class="nowrap right">
{include file="buttons/clone_delete.tpl" microformats="cm-delete-row" no_confirm=true}
</td>
</tr>
{/foreach}
<tr class="{cycle values="table-row , " reset=1}" id="box_add_link" tt="{$key}">
{math equation="x+1" x=$key|default:0 assign="key"}
{$elm_key="{$d_name}[abt__yt_banners][{$key}]"}
<td>{$field='width'}
<select name="{$elm_key}[{$field}]" class="span1" onchange="set_available_positions(this.value, $(this));">
{$r=array(1,2)}
{if $without_x2}{$r=array(1)}{/if}
{foreach $r as $e}
<option value="{$e}">{$e}</option>
{/foreach}
</select>
</td>
<td>{$field='position'}
<select name="{$elm_key}[{$field}]" class="span1">
<option value="0">---</option>
{foreach range(1,$banner_max_position,1) as $e}
<option value="{$e}">{$e}</option>
{/foreach}
</select>
</td>
<td>{$field='priority'}
<select name="{$elm_key}[{$field}]" class="span1">
{foreach range(1,5,1) as $e}
<option value="{$e}">{$e}</option>
{/foreach}
</select>
</td>
<td>{$field='banner_id'}
<select name="{$elm_key}[{$field}]" class="span6">
<option value="0">---</option>
{foreach $abt__yt_banners as $banner_type => $banners}
<optgroup label="{__("abt__yt.banners.types.{$banner_type|lower}")}">
{foreach $banners as $b}
<option value="{$b.banner_id}">{$b.banner}</option>
{/foreach}
</optgroup>
{/foreach}
</select>
</td>
<td class="nowrap right">
{include file="buttons/multiple_buttons.tpl" item_id="add_link"}
</td>
</tr>
</tbody>
</table>
</div>
{if $d_name == "block_data"}</div>{/if}
