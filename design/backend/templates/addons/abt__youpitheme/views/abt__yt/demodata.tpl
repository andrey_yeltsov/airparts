{capture name="mainbox"}
<form action="{""|fn_url}" method="post" name="abt__yt_demo_data_form" id="abt__yt_demo_data_form">
<p>{__("abt__yt.demodata_description")}</p>
<div class="table-responsive-wrapper">
<table class="table table-middle table-responsive" width="100%">
<thead>
<tr>
<th width="60%">{__("abt__yt.demodata.table.description")}</th>
<th width="20%">{__("abt__yt.demodata.table.action")}</th>
</tr>
</thead>
<tbody>
<tr>
<td data-th="{__("abt__yt.demodata.table.description")}">{__("abt__yt.demodata.actions.add_banners")}</td>
<td data-th="{__("abt__yt.demodata.table.action")}">{btn type="list" class="cm-ajax cm-post btn btn-primary" text=__("add") dispatch="dispatch[abt__yt.update_demodata.add_banners]"}</td>
</tr>
<tr>
<td data-th="{__("abt__yt.demodata.table.description")}">{__("abt__yt.demodata.actions.add_menu")}</td>
<td data-th="{__("abt__yt.demodata.table.action")}">{btn type="list" class="cm-ajax cm-post btn btn-primary" text=__("add") dispatch="dispatch[abt__yt.update_demodata.add_menu]"}</td>
</tr>
<tr>
<td data-th="{__("abt__yt.demodata.table.description")}">{__("abt__yt.demodata.actions.add_blog")}</td>
<td data-th="{__("abt__yt.demodata.table.action")}">{btn type="list" class="cm-ajax cm-post btn btn-primary" text=__("add") dispatch="dispatch[abt__yt.update_demodata.add_blog]"}</td>
</tr>
</tbody>
</table>
</div>
</form>
{/capture}
{include file="common/mainbox.tpl"
title=__("abt__yt.demodata")
title_start = __("abt__youpitheme")
title_end = __("abt__yt.demodata")
content=$smarty.capture.mainbox
buttons=$smarty.capture.buttons
content_id="abt__yt_demo_data_form"}