<tr>
<td data-th="{__("icon")}"><span class="yt-icon-example"><i class="s-icon {$icon}"></i></span></td>
<td data-th="{__("copy")}"><a class="ab-yt-icon-copy btn btn-secondary">{__("copy")}</a></td>
<td data-th="{__("abt__yt.icons.class")}">s-icon {$icon}</td>
</tr>