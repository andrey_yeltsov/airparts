<style>
.map {
max-height: 200px;
width: 310px;
padding: 20px 60px;
margin: 0 20px 20px 0;
border: 1px solid #e0e7ef;
background: #eff1f3;
font-size: 14px;
float: left;
overflow: auto;
box-sizing: border-box;
}
.map-button {
display: table-cell;
vertical-align: middle;
height: 225px;
}
.map-button > ul > li > span {
margin-right: 5px;
}
.w4{ padding: 20px 76px }
.w5{ padding: 20px 60px }
.w6{ padding: 20px 44px }
.bm {
height: 32px;
width: 26px;
line-height: 32px;
font-size: 12px;
border-radius: 2px;
display: inline-block;
float: left;
background: white;
border: 1px solid #d4dbe2;
margin: 3px;
text-align: center;
vertical-align: middle;
}
.banner {
background-color: #00afff;
border: 1px solid #00afff;
font-weight: bold;
color: white;
line-height: 32px;
}
.banner2 {
background-color: #FFC107;
border: 1px solid #FFC107;
font-weight: bold;
color: white;
line-height: 32px;
}
</style>
<div>
<div class="map">
{foreach range(1,$banner_max_position, 1) as $p}<div class="bm pos-{$p}">{$p}</div>{/foreach}
</div>
<div class="map-button">
<p>{__("abt__yt.product_list_banners.select_resolution")}</p>
<select onchange="change_banners_map_width(this.value)">
<option value="4">{__("abt__yt.product_list_banners.select_resolution.sr_4")}</option>
<option value="5" selected="selected">{__("abt__yt.product_list_banners.select_resolution.sr_5")}</option>
<option value="6">{__("abt__yt.product_list_banners.select_resolution.sr_6")}</option>
</select>
<br>
{__("abt__yt.product_list_banners.description") nofilter}
</div>
</div>
<script>
function change_banners_map_width (w){
$('div.map').removeClass('w4').removeClass('w5').removeClass('w6').addClass('w' + w);
}
function refresh_banners_map (){
var id = $('#tabs_content_block_' + {$block.block_id} + '_' + {$block.snapping_id} + '_products');
if (!id.length){
var id = $('#content_abt__yt_banners');
}
var trs = id.find('table.abt__yt_product_list_banners tbody tr');
id.find('div.map .bm').removeClass('banner').removeClass('banner2');
if (trs.length){
$.each(trs, function(){
var banner_id = $(this).find('select[name*="banner_id"]').val();
if (parseInt(banner_id)){
var position = parseInt($(this).find('select[name*="position"]').val());
var width = parseInt($(this).find('select[name*="width"]').val());
if (width == 1){
id.find('div.pos-' + position).addClass('banner');
}else if (width == 2){
id.find('div.pos-' + position).addClass('banner2');
id.find('div.pos-' + (position+1) ).addClass('banner2');
}
}
});
}
}
setInterval(function() { refresh_banners_map() }, 1000);
</script>