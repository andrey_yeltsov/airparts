<div class="abt-yt-upgrade-text">{__('abt__yt.upgrade_notifications.text', ['[ver]' => $ver]) nofilter}</div>
{if $notifications}
<div class="abt-yt-upgrade-notifications">
{foreach $notifications as $n}
<div class="abt-yt-upgrade-notification">
<div class="abt-yt-upgrade-notification-title">{$n.title nofilter}</div>
<div class="abt-yt-upgrade-notification-text">{$n.text nofilter}</div>
</div>
{/foreach}
</div>
{/if}
