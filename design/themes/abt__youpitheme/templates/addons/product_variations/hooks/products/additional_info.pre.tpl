{ab__hide_content bot_type="ALL"}
{$limit = $settings.abt__yt.product_list.limit_product_variations}
{if $show_features && $product.variation_features_variants}
    {capture name="variation_features_variants"}
        {$product.variation_features_variants=fn_abt__yt_prepare_variation_features_variants($product.variation_features_variants, $product.abt__yt_features)}
        {foreach $product.variation_features_variants as $variation_feature}
            {if $variation_feature.display_on_catalog === "Y"}
                {hook name="abt__youpitheme:pv_catalog_option"}
                {if $limit > 0}
                <div class="yt-lv__features-item">
                    <p class="yt-lv__features-description">
                        {$variation_feature.description}:
                    </p>

                    {foreach $variation_feature.variants as $variant name=variants}
						{if $smarty.foreach.variants.iteration <= $limit}
                            {if $variant.product.product_type === "V"}
                                <a href="{fn_url("products.view?product_id=`$variant.product_id`")}">
                            {/if}

                                {* Color varian not work *}
                                {if $variation_feature.filter_style == 'color'}
                                    <span class="yt-lv__color-variant{if $variant.active} active{/if}" style="background-color: {$variant.color}">&nbsp;</span>
                                {else}
                                    <span class="yt-lv__features-variant{if $variant.active} active{/if}">
                                   {$variant.variant}
                                </span>
                                {/if}

                            {if $variant.product.product_type === "V"}
                                </a>
                            {/if}
						{/if}
                    {/foreach}
                    {if $variation_feature.variants|count > $limit}<a href="{fn_url("products.view?product_id=`$product.product_id`")}" class="yt-lv__more">({__("more")} +{$variation_feature.variants|count - $limit})</a>{/if}
                </div>
                {/if}
                {/hook}
            {/if}
        {/foreach}
    {/capture}
    {if $smarty.capture.variation_features_variants|trim}
        <div class="yt-lv__item-features">
            {$smarty.capture.variation_features_variants nofilter}
        </div>
    {/if}
{/if}
{/ab__hide_content}