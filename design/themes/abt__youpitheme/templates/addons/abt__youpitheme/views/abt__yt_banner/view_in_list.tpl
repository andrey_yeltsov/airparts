{strip}
    {capture name="banner"}
        

        
        {if $settings.abt__yt.product_list.show_sku == 'N' && $settings.abt__yt.product_list.show_amount == 'N'}
            {assign var="t1" value=19}
        {/if}

        {* Show buttons *}
        {if $type_list == 'category' && $settings.abt__yt.product_list.show_buttons == 'N' || $b.has_buttons =='false'}
            {assign var="t2" value=42}
        {/if}

        {$th = $t1|default:0 + $t2|default:0}

        {$abt__yt_gl_item_height=$settings.abt__yt.product_list.height_list_prblock - $th}

        

        {capture name="banners_wrapper_class_list"}
            {hook name="abt__yt:banners_wrapper_class_list"}
                abyt-a-bg-banner {$b.abt__yt_color_scheme}
                {if $b.abt__yt_how_to_open == 'in_popup'} abyt-in-popup-{$b.link_id}{/if}
                {if $b.abt__yt_content_full_width =="Y"} width-full{else} width-half{/if}
                {if $b.abt__yt_background_color == '#ffffff' and $b.abt__yt_background_color_use == 'Y'} white-bg{/if}
                {if $type_list == 'category' && $settings.abt__yt.product_list.show_buttons == 'N' || $b.has_buttons =='false'} no-buttons{/if}
                {if $settings.abt__yt.product_list.show_sku == 'N' && $settings.abt__yt.product_list.show_amount == 'N'} no-amount{/if}
                {if $b.abt__yt_data_type == "blog" && $b.abt__yt_youtube_use == 'Y' && $b.abt__yt_youtube_id} abyt-video{/if}
            {/hook}
        {/capture}

        {capture name="banners_wrapper_styles"}
            {hook name="abt__yt:banners_wrapper_styles"}
                height: {$abt__yt_gl_item_height|intval + 2}px;
                {if $b.abt__yt_background_color_use == 'Y' or !empty($b.abt__yt_background_image.icon.image_path)} background:
                    {if ($b.abt__yt_background_color_use == 'Y')} {$b.abt__yt_background_color}{/if}
                    {if !empty($b.abt__yt_background_image.icon.image_path)} url('{$b.abt__yt_background_image.icon.image_path}') no-repeat center{/if};
                {/if}
            {/hook}
        {/capture}

        <div class="{$smarty.capture.banners_wrapper_class_list|strip nofilter}" style="{$smarty.capture.banners_wrapper_styles|strip nofilter}">

            <div class="abyt-a-content {if !empty($b.abt__yt_main_image) && is_array($b.abt__yt_main_image)}double-blocks{/if} valign-{$b.abt__yt_content_valign} align-{$b.abt__yt_content_align}" style="height: {$abt__yt_gl_item_height + 2}px">

                {if !empty($b.abt__yt_main_image) && is_array($b.abt__yt_main_image)}
                    <div class="abyt-a-img{if $b.abt__yt_data_type == "promotion"} promotion{/if} {if $b.abt__yt_data_type == "blog" && $b.abt__yt_youtube_use == 'Y' && $b.abt__yt_youtube_id} video{/if}">
                        {include file="common/image.tpl" images=$b.abt__yt_main_image}
                    </div>
                {/if}

                {if $b.abt__yt_data_type == "promotion" && $b.abt__yt_countdown_use == "Y" && $b.promotion_data.to_date > $smarty.const.TIME}
                    <div class="pd-grid-cd-counter"><div class="cd-counter-content"><span data-countdown="{$b.promotion_data.to_date|date_format:"%Y/%m/%d %H:%M:%S"}" data-str="{if $b.promotion_data.left_days != 0}<span class='days'>%-D {__("abt__yt.banners.countdown.left_days", [$b.promotion_data.left_days])}</span> {/if}%H:%M:%S"></span></div></div>
                {/if}

                <div class="box bg-opacity-{$b.abt__yt_content_bg}" style="{if $b.abt__yt_content_bg == 'C' && $b.abt__yt_content_bg_color_use == 'Y'}background-color: {$b.abt__yt_content_bg_color};{/if}{if !empty($b.abt__yt_padding)}padding:{$b.abt__yt_padding}{/if}">
                    
                    {if $b.abt__yt_title}<{$b.abt__yt_title_tag} class="abyt-a-title {if $b.abt__yt_title_shadow =="Y"}shadow{/if} weight-{$b.abt__yt_title_font_weight}" style="font-size: {$b.abt__yt_title_font_size};{if $b.abt__yt_title_color_use == 'Y'}color: {$b.abt__yt_title_color};{/if}">{$b.abt__yt_title}</{$b.abt__yt_title_tag}>{/if}
                    {if $b.abt__yt_description}<div class="abyt-a-descr" style="{if $b.abt__yt_description_color_use == 'Y'}color: {$b.abt__yt_description_color};{/if}{if $b.abt__yt_description_bg_color_use == 'Y'}background-color:{$b.abt__yt_description_bg_color};position: relative;left: 5px;box-shadow: -5px 0 0 1px {$b.abt__yt_description_bg_color}, 5px 0 0 1px {$b.abt__yt_description_bg_color};{/if} font-size: {$b.abt__yt_description_font_size};">{$b.abt__yt_description nofilter}</div>{/if}

                {if $b.abt__yt_button_use == 'Y'}
                    <div class="abyt-a-button">
                        {$button_style=""}
                        {if $b.abt__yt_button_text_color_use == 'Y'}{$button_style="`$button_style`color:`$b.abt__yt_button_text_color`;"}{/if}
                        {if $b.abt__yt_button_color_use == 'Y'}{$button_style="`$button_style`background:`$b.abt__yt_button_color`;"}{/if}
                        {if $button_style}{$button_style="style={$button_style}"}{/if}
                        <span class="ty-btn ty-btn__primary" {$button_style}>{$b.abt__yt_button_text|default:"button"}</span>
                    </div>
                {/if}
            </div>

        </div>

        
        {if $b.abt__yt_how_to_open == 'in_popup'}
            <div class="abt-yt-bp-container" data-link="{$b.link_id}"></div>
        {/if}
        </div>
    {/capture}

    {if $b.abt__yt_how_to_open|in_array:['in_new_window','in_this_window']}
        {$url=$b.abt__yt_url}
        {if $b.abt__yt_data_type == "blog" && $b.abt__yt_page_id > 0}
            {$url="pages.view&page_id={$b.abt__yt_page_id}"}
        {elseif $b.abt__yt_data_type == "promotion" && $b.abt__yt_promotion_id > 0}
            {$url="promotions.view&promotion_id={$b.abt__yt_promotion_id}"}
        {/if}
        <a href="{$url|fn_url}" {if $b.abt__yt_how_to_open == 'in_new_window'}target="_blank"{/if}>{$smarty.capture.banner nofilter}</a>
    {else}
        {$smarty.capture.banner nofilter}
    {/if}
{/strip}