<script>
    (function(_, $) {
        $.extend(_, {
            abt__yt: {
                settings: {$settings.abt__yt|json_encode nofilter},
                device: "{$settings.abt__device}",

                functions: {
                    toggle_menu: function() {
                        var active_class = 'ty-menu__item-toggle-active';
                        var active_submenu_class = 'ty-menu__items-show';
                        var $self = $(this);
                        setTimeout(function(){
                            if ($self.hasClass(active_class)) {
                                $(".ty-menu__item-toggle." + active_class).removeClass(active_class);
                                $('.cm-responsive-menu-submenu.' + active_submenu_class).removeClass(active_submenu_class);
                                $self.addClass(active_class);
                                $self.parent().find('.cm-responsive-menu-submenu').first().addClass(active_submenu_class);
                            }
                        }, 40);
                    },
                },

                temporary: { },
            }
        });

        if (window.innerWidth > 767) {
            $('body').data('ca-scroll-to-elm-offset', 64);
        }
    }(Tygh, Tygh.$));
</script>
{script src="js/addons/abt__youpitheme/jquery.countdown.min.js"}
{script src="js/addons/abt__youpitheme/jquery.popupoverlay.min.js"}
{script src="js/addons/abt__youpitheme/abt__yt_func.js"}
{script src="js/addons/abt__youpitheme/abt__yt_grid_tabs.js"}

{if $settings.abt__yt.general.use_lazy_load_for_images === "Y"}
    {script src="js/addons/abt__youpitheme/abt__yt_lazy_load.js"}
{/if}
