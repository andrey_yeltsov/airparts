<div class="row-fluid ty-discussion-post {cycle values=", ty-discussion-post_even"}" id="post_{$post.post_id}">
    <div class="span4">
        <span class="ty-discussion-post__author">{$post.name}</span>
        {if $discussion.type == "R" || $discussion.type == "B" && $post.rating_value > 0}
            <div class="clearfix ty-discussion-post__rating">
                {include file="addons/discussion/views/discussion/components/stars.tpl" stars=$post.rating_value|fn_get_discussion_rating}
            </div>
        {/if}
        <span class="ty-discussion-post__date">{$post.timestamp|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}</span>
    </div>

    <div class="span12">
        {if $discussion.type == "C" || $discussion.type == "B"}
            <div class="ty-discussion-post__message">{$post.message|escape|nl2br nofilter}</div>
        {/if}
    </div>
</div>