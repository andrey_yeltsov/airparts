{if $page.description && $page.page_type == $smarty.const.PAGE_TYPE_BLOG}
    <div class="ty-blog__date"><i class="material-icons">&#xE916;</i> {$page.timestamp|date_format:"`$settings.Appearance.date_format`"}</div>
    <div class="ty-blog__author"><i class="material-icons">&#xE7FF;</i> {$page.author}</div>

    {if $b.abt__yt_youtube_use == 'Y' and $b.abt__yt_youtube_id}
        <div class="ty-blog__video-block">
            <iframe id="youtube-player" width="600" height="400" src="{$b|fn_abt__yt_build_youtube_link}&enablejsapi=1&version=3" frameborder="0" allowfullscreen="true" allowscriptaccess="always"></iframe>
        </div>
    {elseif $page.main_pair}
        <div class="ty-blog__img-block">
            {include file="common/image.tpl" obj_id=$page.page_id image_height=400 images=$page.main_pair lazy_load=true}
        </div>
    {/if}
{/if}
