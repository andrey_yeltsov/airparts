<ul class="ty-menu__submenu-items cm-responsive-menu-submenu dropdown-column-item{if $item1.abt__yt_mwi__dropdown == "Y"} tree-level-dropdown hover-zone2{else} row-filling{$dropdown_class}{if $item1.abt__yt_mwi__text && $item1.abt__yt_mwi__text_position != 'bottom'} with-pic{/if}{/if}{if $block.properties.abt_menu_icon_items == 'Y'} with-icon-items{/if} clearfix" style="min-height: {$settings.abt__yt.general.menu_min_height}px">
    {$Viewlimit=$block.properties.no_hidden_elements_second_level_view|default:5}
    {hook name="blocks:topmenu_dropdown_2levels_row_elements"}
        <li class="ty-menu__submenu-col">
            {foreach from=$item1.$childs item="item2" name="item2"}
                <div class="ty-top-mine__submenu-col {if $item2.abt__yt_mwi__status == 'Y' && $item2.abt__yt_mwi__icon}mw-icon{/if} second-lvl"{if $item1.abt__yt_mwi__dropdown == "N"} style="width: {$col_width}%"{/if}>
                    {assign var="item2_url" value=$item2|fn_form_dropdown_object_link:$block.type}
                    <div class="ty-menu__submenu-item-header{if $item2.active || $item2|fn_check_is_active_menu_item:$block.type} ty-menu__submenu-item-header-active{/if}{if $item2.class} {$item2.class}{/if}">
                        <a href="{if $item2_url}{$item2_url}{else}{''|fn_url}{/if}" class="ty-menu__submenu-link{if empty($item2.$childs)} no-items{/if}">{if $block.properties.abt_menu_icon_items == 'Y' && $item2.abt__yt_mwi__status == 'Y' && $item2.abt__yt_mwi__icon && $item1.abt__yt_mwi__dropdown == "N" && $settings.abt__device != 'mobile'}<div class="mwi-icon">{include file="common/image.tpl" images=$item2.abt__yt_mwi__icon class="mwi-img" no_ids=true}</div>{/if}<bdi>{$item2.$name}
                            {if $item2.abt__yt_mwi__status == 'Y' && $item2.abt__yt_mwi__label}
                               <span class="m-label" style="color: {$item2.abt__yt_mwi__label_color};background-color: {$item2.abt__yt_mwi__label_background}">{$item2.abt__yt_mwi__label}</span>
                            {/if}
                            </bdi>
                        </a>
                        {if $item2.$childs && $item1.abt__yt_mwi__dropdown == "Y"}<i class="icon-right-dir material-icons">&#xE315;</i>{/if}
                    </div>
                    {if !empty($item2.$childs)}
                        <a class="ty-menu__item-toggle visible-phone cm-responsive-menu-toggle">
                            <i class="ty-menu__icon-open material-icons">&#xE145;</i>
                            <i class="ty-menu__icon-hide material-icons">&#xE15B;</i>
                        </a>
                        <div class="ty-menu__submenu"{if $item1.abt__yt_mwi__dropdown == "Y"} style="min-height: {$settings.abt__yt.general.menu_min_height}px"{/if}>
                            {if $item1.abt__yt_mwi__dropdown == "Y"}
                                <div class="sub-title-two-level"><bdi>{$item2.$name}</bdi></div>
                                {$max_amount3=2*$block.properties.elements_per_column_third_level_view}
                                {$item2.$childs=array_slice($item2.$childs, 0, $max_amount3, true)}
                                {foreach from=array_chunk($item2.$childs, ceil($item2.$childs|count / 2), true) item="item2_childs"}
                                    <ul class="ty-menu__submenu-list tree-level-col cm-responsive-menu-submenu">
                                        {if $item2_childs}
                                            {hook name="blocks:topmenu_dropdown_3levels_col_elements"}
                                            {foreach from=$item2_childs item="item3" name="item3"}
                                                {assign var="item3_url" value=$item3|fn_form_dropdown_object_link:$block.type}
                                                <li class="ty-menu__submenu-item{if $item3.active || $item3|fn_check_is_active_menu_item:$block.type} ty-menu__submenu-item-active{/if}{if $item3.class} {$item3.class}{/if}">
                                                    <a{if $item3_url} href="{$item3_url}"{/if} class="ty-menu__submenu-link"><bdi>{$item3.$name}
                                                        {if $item3.abt__yt_mwi__status == 'Y' && $item3.abt__yt_mwi__label}
                                                            <span class="m-label" style="color: {$item3.abt__yt_mwi__label_color}; background-color: {$item3.abt__yt_mwi__label_background}">{$item3.abt__yt_mwi__label}</span>
                                                        {/if}</bdi>
                                                    </a>
                                                </li>
                                            {/foreach}
                                            {/hook}
                                        {/if}
                                    </ul>
                                {/foreach}
                            {else}
                                <ul class="ty-menu__submenu-list {if $item1.abt__yt_mwi__dropdown == "Y"}tree-level-col {elseif $item2.$childs|count > $Viewlimit}hiddenCol {/if}cm-responsive-menu-submenu"{if $item2.$childs|count > $Viewlimit && $item1.abt__yt_mwi__dropdown == "N"} style="height: {$Viewlimit * 21}px;"{/if}>
                                    {if $item2.$childs}
                                        {if $item1.abt__yt_mwi__dropdown == "Y"}<li class="sub-title-two-level"><bdi>{$item2.$name}</bdi></li>{/if}
                                        {hook name="blocks:topmenu_dropdown_3levels_col_elements"}
                                        {foreach from=$item2.$childs item="item3" name="item3"}
                                            {assign var="item3_url" value=$item3|fn_form_dropdown_object_link:$block.type}
                                            <li class="ty-menu__submenu-item{if $item3.active || $item3|fn_check_is_active_menu_item:$block.type} ty-menu__submenu-item-active{/if}">
                                                <a{if $item3_url} href="{$item3_url}"{/if} class="ty-menu__submenu-link"><bdi>{$item3.$name}
                                                    {if $item3.abt__yt_mwi__status == 'Y' && $item3.abt__yt_mwi__label}
                                                        <span class="m-label" style="color: {$item3.abt__yt_mwi__label_color}; background-color: {$item3.abt__yt_mwi__label_background}">{$item3.abt__yt_mwi__label}</span>
                                                    {/if}
                                                    </bdi>
                                                </a>
                                            </li>
                                        {/foreach}
                                        {/hook}
                                    {/if}
                                </ul>
                            {/if}

                            {if $item2.$childs|count > $Viewlimit && $item1.abt__yt_mwi__dropdown !="Y"}
                                <span class="link-more"><span>{__("more")}</span><i class="material-icons">&#xE5DB;</i></span>
                            {/if}

                            {if $item1.abt__yt_mwi__status == 'Y' && !$item1.abt__yt_mwi__text && $item2.abt__yt_mwi__text && $settings.abt__device != 'mobile'}
                                <li class="mwi-html {if $item2.abt__yt_mwi__dropdown == "Y"}bottom{else}{$item2.abt__yt_mwi__text_position}{/if}">{$item2.abt__yt_mwi__text nofilter}</li>
                            {/if}
                        </div>
                    {/if}
                </div>
            {/foreach}

            {if $item1.abt__yt_mwi__status == 'Y' && $item1.abt__yt_mwi__text && $settings.abt__device != 'mobile'}
                <div class="mwi-html{if $item1.abt__yt_mwi__dropdown == "Y"} bottom{else} {$item1.abt__yt_mwi__text_position}{/if}">{$item1.abt__yt_mwi__text nofilter}</div>
            {else}
                {if $item1.show_more && $item1_url}
                    <div class="ty-menu__submenu-item ty-menu__submenu-alt-link">
                        <a href="{$item1_url}">{__("text_topmenu_more", ["[item]" => $item1.$name])}</a>
                    </div>
                {/if}
            {/if}
        </li>
    {/hook}
</ul>