<div id="breadcrumbs_{$block.block_id}">
    {$show_only_first_and_last = $settings.abt__device == 'mobile' && $settings.abt__yt.general.breadcrumbs_view == 'show_only_first_and_last'}
    {$print_all_crumbs = $print_all_crumbs|default:in_array($settings.abt__yt.general.breadcrumbs_view, ['hide_with_btn', 'do_not_hide'])}

    {if $breadcrumbs && $breadcrumbs|@sizeof > 1}
        <div itemscope itemtype="http://schema.org/BreadcrumbList" class="ty-breadcrumbs clearfix {$settings.abt__yt.general.breadcrumbs_view}">
            {strip}
                {$last_with_link_index = $breadcrumbs|count - 2}
                {foreach from=$breadcrumbs item="bc" name="bcn" key="key"}
                    {if ((($show_only_first_and_last && ($key == $last_with_link_index)) || $print_all_crumbs) || $settings.abt__device != 'mobile') && $key != 0}
                        <span class="ty-breadcrumbs__slash">/</span>
                    {/if}
                    <span{if $bc.link} itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"{/if}>
                        {if $bc.link}
                            <a itemprop="item" href="{$bc.link|fn_url}" class="ty-breadcrumbs__a{if $additional_class} {$additional_class}{/if}"{if $bc.nofollow} rel="nofollow"{/if}>
                                <meta itemprop="position" content="{$key+1}" />
                                <meta itemprop="name" content="{$bc.title|strip_tags|escape:"html" nofilter}" />
                                {if ($show_only_first_and_last && ($key == $last_with_link_index) || $key == 0) || $print_all_crumbs || $settings.abt__device != 'mobile'}
                                    <bdi>{$bc.title|strip_tags|escape:"html" nofilter}</bdi>
                                {/if}
                            </a>
                        {elseif !$show_only_first_and_last && $settings.abt__device != 'mobile'}
                            <span class="ty-breadcrumbs__current">
                                <bdi>{$bc.title|strip_tags|escape:"html" nofilter}</bdi>
                            </span>
                        {/if}
                    </span>
                {/foreach}
                {include file="common/view_tools.tpl"}
            {/strip}
        </div>
    {/if}
    <!--breadcrumbs_{$block.block_id}--></div>
