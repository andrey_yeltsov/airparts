{script src="js/lib/owlcarousel/owl.carousel.min.js"}
<script>
(function(_, $) {
    $.ceEvent('on', 'ce.commoninit', function(context) {
        var elm = context.find('#scroll_list_{$block.block_id}');

        if (elm.length) {
            $('.ty-float-left:contains(.ty-scroller-list),.ty-float-right:contains(.ty-scroller-list)').css('width', '100%');

            var item = {$block.properties.item_quantity|default:5},
                itemsDesktop = 5,
                itemsDesktopSmall = 4,
                itemsTablet = 3,
                itemsTabletSmall = 2,
                itemsMobile = 1;

            if (item == 6) {
                itemsDesktop = item - 1;
                itemsDesktopSmall = item - 2;
                itemsTablet = item - 3;
                itemsTabletSmall = item - 4;
            } else if (item > 3) {
                itemsDesktop = item - 1;
                itemsDesktopSmall = item - 1;
                itemsTablet = item - 2;
                itemsTabletSmall = item - 3;
            } else if (item == 1) {
                itemsDesktop = itemsDesktopSmall = itemsTablet = 1;
            } else {
                itemsDesktop = item;
                itemsDesktopSmall = itemsTablet = item - 1;
            }

            var desktop = [1500, itemsDesktop],
                desktopSmall = [1279, itemsDesktopSmall],
                tablet = [940, itemsTablet],
                tabletSmall = [520, itemsTabletSmall],
                mobile = [350, itemsMobile];

            {if $block.properties.outside_navigation == "Y"}
                function outsideNav () {
                    if(this.options.items >= this.itemsAmount){
                        $("#owl_outside_nav_{$block.block_id}").hide();
                    } else {
                        $("#owl_outside_nav_{$block.block_id}").show();
                    }
                }
            {/if}

            elm.owlCarousel({
                direction: '{$language_direction}',
                items: item,
                itemsDesktop: desktop,
                itemsDesktopSmall: desktopSmall,
                itemsTablet: tablet,
                itemsTabletSmall: tabletSmall,
                itemsMobile: mobile,
                {if $block.properties.scroll_per_page == "Y"}
                scrollPerPage: true,
                {/if}
                {if $block.properties.not_scroll_automatically == "Y"}
                autoPlay: false,
                {else}
                autoPlay: '{$block.properties.pause_delay * 1000|default:0}',
                {/if}
                lazyLoad: true,
                slideSpeed: {$block.properties.speed|default:400},
                stopOnHover: true,
                {if $block.properties.outside_navigation == "N"}
                navigation: true,
                navigationText: ['<i class="material-icons">&#xE408;</i>', '<i class="material-icons">&#xE409;</i>'],
                {/if}
                pagination: false,
            {if $block.properties.outside_navigation == "Y"}
                afterInit: outsideNav,
                afterUpdate : outsideNav
            });

              $('{$prev_selector}').click(function(){
                elm.trigger('owl.prev');
              });
              $('{$next_selector}').click(function(){
                elm.trigger('owl.next');
              });

            {else}
            });
            {/if}

        }
    });
}(Tygh, Tygh.$));
</script>