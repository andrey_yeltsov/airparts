{if $addons.single_step_checkout.status == "A"}
{** Сгенерировать email **}
{$ab__cwe_email=$addons.ab__checkout_without_email.user_email|fn_ab__cwe_format_email}
{if !empty($ab__cwe_email)}
<script type="text/javascript">
(function (_, $) {
$.extend(_, {
ab__cwe: {
email: '{$ab__cwe_email|escape:"javascript"}',
user_id: '{$auth.user_id|default:0|escape:"javascript"}',
settings: {
hide_email: '{$addons.ab__checkout_without_email.hide_email|default:'N'|escape:"javascript"}',
user_email: '{$addons.ab__checkout_without_email.user_email|escape:"javascript"}',
},
abcd__hide: '{$abcd__hide|default:true}'
}
});
{** проверка подстановочного email **}
function fn_ab__cwe_check_cwe_email(text) {
var suffix = '@' + _.ab__cwe.settings.user_email.split('@')[1];
return text.indexOf(suffix) + 1;
}
{** выполнение скрытия информации об email **}
function fn_ab__cwe_hide_cwe_email() {
if (_.ab__cwe.settings.hide_email == 'Y') {
if ($("div[class*='ab__checkout_without_email'] input[type='checkbox']").prop("checked")) {
$('#tygh_billing_adress .ty-order-info__profile-field-item.email').addClass('hidden');
$('#tygh_shipping_adress .ty-order-info__profile-field-item.email').addClass('hidden');
} else {
["#tygh_billing_adress .ty-order-info__profile-field-item.email"
, "#tygh_shipping_adress .ty-order-info__profile-field-item.email"
, "#tygh_order_billing_adress .ty-info-field.email"
, "#tygh_order_shipping_adress .ty-info-field.email"]
.forEach(function (i) {
var e = $(i);
if (!fn_ab__cwe_check_cwe_email(e.text())) {
e.removeClass('hidden')
} else {
e.addClass('hidden')
}
});
}
}
}
{** орбработчик поля email **}
function fn_ab__cwe_email(s) {
var email_field = $("input[name='user_data[email]']");
var email_label = email_field.prev();
var email_block = email_field.parent();
if (email_field.length) {
{** Флажок установлен **}
if (s == true) {
email_block.hide("fast", function () {
email_field.val(_.ab__cwe.email);
});
} else {
{** очистим поле, если там подстановочный email **}
if (fn_ab__cwe_check_cwe_email(email_field.val())) {
email_field.val("");
}
email_block.show("fast");
}
}
}
{** Проверка статуса при обновлении данных **}
$.ceEvent('on', 'ce.commoninit', function () {
var ab__cwe = $("div[class*='ab__checkout_without_email']");
if (ab__cwe.length) {
var email_field = $("input[name='user_data[email]']");
var checkbox = $("div[class*='ab__checkout_without_email'] input[type='checkbox']");
_.ab__cwe.abcd__hide = false;
checkbox.prop("checked", false);
{** Неавторизованный пользователь **}
if (_.ab__cwe.user_id == 0) {
fn_ab__cwe_email(_.ab__cwe.abcd__hide);
} else {
ab__cwe.hide();
}
}
{** скрыть email при необходимости **}
fn_ab__cwe_hide_cwe_email();
});
{** Следить за изменением статуса **}
$(document).on("click", "div[class*='ty-control-group'] input[type='checkbox']", function (e) {
fn_ab__cwe_email($(this).prop("checked"));
{** скрыть email при необходимости **}
fn_ab__cwe_hide_cwe_email();
$.ceAjax('request', '{fn_url('checkout.checkout')}', {
data: {
abcd__hide: $(this).prop("checked")
}
});
});
}(Tygh, Tygh.$));
</script>
{/if}
{elseif $runtime.controller|in_array:["checkout","orders"]}
{** Сгенерировать email **}
{$ab__cwe_email=$addons.ab__checkout_without_email.user_email|fn_ab__cwe_format_email}
{if !empty($ab__cwe_email)}
<script type="text/javascript">
(function(_, $) {
$.extend(_, {
ab__cwe: {
email: '{$ab__cwe_email|escape:"javascript"}',
user_id: '{$auth.user_id|default:0|escape:"javascript"}',
settings: {
hide_email: '{$addons.ab__checkout_without_email.hide_email|default:'N'|escape:"javascript"}',
user_email: '{$addons.ab__checkout_without_email.user_email|escape:"javascript"}',
},
}
});
{** проверка подстановочного email **}
function fn_ab__cwe_check_cwe_email (text){
var suffix = '@' + _.ab__cwe.settings.user_email.split('@')[1];
return text.indexOf(suffix)+1;
}
{** выполнение скрытия информации об email **}
function fn_ab__cwe_hide_cwe_email (){
if (_.ab__cwe.settings.hide_email == 'Y'){
if ($("div[class*='ab__checkout_without_email'] input[type='checkbox']").prop("checked") ){
$('#tygh_billing_adress .ty-order-info__profile-field-item.email').addClass('hidden');
$('#tygh_shipping_adress .ty-order-info__profile-field-item.email').addClass('hidden');
}else{
["#tygh_billing_adress .ty-order-info__profile-field-item.email"
, "#tygh_shipping_adress .ty-order-info__profile-field-item.email"
, "#tygh_order_billing_adress .ty-info-field.email"
, "#tygh_order_shipping_adress .ty-info-field.email"]
.forEach(function(i) {
var e = $(i);
if (!fn_ab__cwe_check_cwe_email(e.text())){
e.removeClass('hidden')
}else {
e.addClass('hidden')
}
});
}
}
}
{** орбработчик поля email **}
function fn_ab__cwe_email(s){
var email_field = $("input[name='user_data[email]']");
var email_label = email_field.prev();
var email_block = email_field.parent();
if (email_field.length){
{** Флажок установлен **}
if (s == true){
email_block.hide("fast", function(){
email_field.val(_.ab__cwe.email);
});
}else{
{** очистим поле, если там подстановочный email **}
if (fn_ab__cwe_check_cwe_email(email_field.val())){
email_field.val("");
}
email_block.show("fast");
}
}
}
{** Проверка статуса при обновлении данных **}
$.ceEvent('on', 'ce.commoninit', function() {
var ab__cwe = $("div[class*='ab__checkout_without_email']");
if (ab__cwe.length){
{** Неавторизованный пользователь **}
if (_.ab__cwe.user_id == 0){
fn_ab__cwe_email(ab__cwe.find("input[type='checkbox']").prop("checked"));
}else{
ab__cwe.hide();
}
}
{** скрыть email при необходимости **}
fn_ab__cwe_hide_cwe_email();
});
{** Следить за изменением статуса **}
$(document).on("click", "div[class*='ab__checkout_without_email'] input[type='checkbox']", function(e){
fn_ab__cwe_email($(this).prop("checked"));
{** скрыть email при необходимости **}
fn_ab__cwe_hide_cwe_email();
});
}(Tygh, Tygh.$));
</script>
{/if}
{/if}