{strip}
<div class="ab__qobp_content">
{assign var="user_phone" value=""}
{if isset($smarty.session.cart.user_data.phone) and !empty($smarty.session.cart.user_data.phone)}
{if $smarty.session.cart.user_data.phone|trim|fn_ab__qobp_check_phone}
{assign var="user_phone" value=$smarty.session.cart.user_data.phone|trim}
{/if}
{/if}
<input type="text" name="ab__qobp_data[phone]" class="input-medium ab__qobp_phone cm-mask-phone" placeholder="{__('ab__qobp.placeholder')}"{if $user_phone} value="{$user_phone}"{/if}>
{include file="buttons/button.tpl" but_id="ab__qobp_product" but_text=__("ab__qobp.button")
but_name="dispatch[ab__qobp.`$mode`]" but_meta="ty-btn__secondary ty-btn__ab__qobp"}
</div>
{/strip}