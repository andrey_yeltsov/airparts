{* Variables

$ath_image_width
$ath_image_height
$include_from

* END | Variables *}

	{assign var="ath_prod_effect" value=$addons.ath_prod_image_effects.effects}
	{if !empty($product.prod_image_effect_two) && $product.prod_image_effect_two != "global"}
		{assign var="ath_prod_effect" value=$product.prod_image_effect_two}
	{elseif !empty($block.properties.prod_image_effects_two) && $block.properties.prod_image_effects_two != "global"}
		{assign var="ath_prod_effect" value=$block.properties.prod_image_effects_two}
	{/if}

	{capture name="main_icon"}
	
		{if $include_from != "product_details"}
			<a href="{"products.view?product_id=`$product.product_id`"|fn_url}">
				{include file="common/image.tpl" obj_id=$obj_id_prefix images=$product.main_pair image_width=$ath_image_width image_height=$ath_image_height lazy=$addons.ath_thin_theme_settings.lazy}
			</a>
		{else}
			{include file="common/image.tpl" obj_id=$obj_id_prefix images=$product.main_pair image_width=$ath_image_width image_height=$ath_image_height lazy=$addons.ath_thin_theme_settings.lazy}
		{/if}
	{/capture}


	{if $product.image_pairs && $addons.ath_prod_image_effects.images !="only_one_image" }

		{if true}
			<div class="ath_pie__image-wrapper ath_pie__image-wrapper--two ath_pie__image-wrapper--{$ath_prod_effect} ath_pie__image-wrapper--hover-{$addons.ath_prod_image_effects.hover}">
				<div class="ath_pie__image-wrapper__inner">
					<div class="ath_pie__two ath_pie__two--front">
						{$smarty.capture.main_icon nofilter}
					</div>
					<div class="ath_pie__two ath_pie__two--back">
					{foreach from=$product.image_pairs item="image_pair"}
						{if $image_pair}
							{if $include_from != "product_details"}
								<a href="{"products.view?product_id=`$product.product_id`"|fn_url}">
									{include file="common/image.tpl" no_ids=true images=$image_pair image_width=$ath_image_width image_height=$ath_image_height lazy_load=$ath_lazy_load}
								</a>
							{else}
								{include file="common/image.tpl" no_ids=true images=$image_pair image_width=$ath_image_width image_height=$ath_image_height lazy_load=$ath_lazy_load}
							{/if}
							
							{break}
						{/if}
					{/foreach}
					</div>
				</div>
			</div>
		{else}
			<div class="ath_pie__image-wrapper ath_pie__image-wrapper--gallery">
				<div class="ath_pie__gallery__item">
					{$smarty.capture.main_icon nofilter}
				</div>
			{foreach from=$product.image_pairs item="image_pair"}
				{if $image_pair}
					<div class="ath_pie__gallery__item">
						<a href="{"products.view?product_id=`$product.product_id`"|fn_url}">
							{include file="common/image.tpl" no_ids=true images=$image_pair image_width=$ath_image_width image_height=$ath_image_heights}
						</a>
					</div>
				{/if}
			{/foreach}
			</div>
		{/if}

	{else}
		<div class="ath_pie__image-wrapper ath_pie__image-wrapper--one">
			{$smarty.capture.main_icon nofilter}
		</div>
	{/if}
