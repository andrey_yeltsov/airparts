{if $addons.ath_prod_image_effects.blocks_products_small_items_tpl == 'Y' && $block.properties.prod_image_effects_two != "none" && $product.prod_image_effect_two != "none"}    
    {hook name="products:product_small_item"}
    <li class="ty-template-small__item clearfix">
        {assign var="form_open" value="form_open_`$obj_id`"}
        {$smarty.capture.$form_open nofilter}
            <div class="ty-template-small__item-img">
	            {* TH *}

                <a href="{"products.view?product_id=`$product.product_id`"|fn_url}" class="ty-template-small__item-img__link">
	                
{*
	                {include file="common/image.tpl" lazy=$addons.ath_thin_theme_settings.lazy image_width=$small_items_image_width image_height=$small_items_image_height images=$product.main_pair obj_id=$obj_id_prefix no_ids=true}
	                
*}
	                
	                {include 
					    file="addons/ath_prod_image_effects/common/image_effects.tpl"
					    ath_image_width=$small_items_image_width|default:40
					    ath_image_height=$small_items_image_height|default:40
					    include_from="small_items"
					}

	                
	                {assign var="product_labels" value="product_labels_`$obj_prefix``$obj_id`"}
	                {$smarty.capture.$product_labels nofilter}

	            {* /TH *}
	                
                </a>

            </div>
            <div class="ty-template-small__item-description">
                {if $block.properties.item_number == "Y"}{$smarty.foreach.products.iteration}.&nbsp;{/if}
                {assign var="name" value="name_$obj_id"}<bdi>{$smarty.capture.$name nofilter}</bdi>

                {if $show_price}
                <div class="ty-template-small__item-price">
	                {* TH *}
                    {assign var="price" value="price_`$obj_id`"}
                    {$smarty.capture.$price nofilter}
                    
                    {assign var="old_price" value="old_price_`$obj_id`"}
                    {if $smarty.capture.$old_price|trim}{$smarty.capture.$old_price nofilter}{/if}
                    {* /TH *}
                </div>
                {/if}

                {assign var="rating" value="rating_$obj_id"}
                {$smarty.capture.$rating nofilter}

                {assign var="add_to_cart" value="add_to_cart_`$obj_id`"}
                {if $smarty.capture.$add_to_cart|trim}<p>{$smarty.capture.$add_to_cart nofilter}</p>{/if}
            </div>
        {assign var="form_close" value="form_close_`$obj_id`"}
        {$smarty.capture.$form_close nofilter}
    </li>
    {/hook}
{/if}