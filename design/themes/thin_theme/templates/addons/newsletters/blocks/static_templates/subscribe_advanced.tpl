{** block-description:tmpl_newsletters_subscription_advanced **}
{if $addons.newsletters}
<div class="th_footer-form-block no-help">
    <form action="{""|fn_url}" method="post" name="subscribe_form">
        <input type="hidden" name="redirect_url" value="{$config.current_url}" />
        <input type="hidden" name="newsletter_format" value="2" />

        <div class="ty-footer-form-block__form ty-control-group">

            <label class="cm-required cm-email hidden" for="subscr_email{$block.block_id}">{__("email")}</label>
            <input type="text" name="subscribe_email" id="subscr_email{$block.block_id}" size="20" placeholder="{__("email")}" class="cm-hint ty-valign-top th_subscribe-input" />

            {include file="buttons/button.tpl" but_role="submit" but_name="dispatch[newsletters.add_subscriber]" but_text=__("subscribe") but_meta="ty-btn__subscribe"}

        </div>
        
    </form>
</div>
{/if}