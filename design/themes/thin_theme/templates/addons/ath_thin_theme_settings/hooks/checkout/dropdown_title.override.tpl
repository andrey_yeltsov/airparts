{hook name="checkout:dropdown_title"}
        <i class="th_minicart__icon icon-tt-cart filled"></i>                 
    {if $smarty.session.cart.amount}
        <span class="th_minicart__title ty-hand">
        	<span class="tt_minicart__text">{__("small_cart")}</span>
            <span class="tt_minicart__amount">{$smarty.session.cart.amount}</span>
        </span>
    {else}
        <span class="th_minicart__title empty-cart ty-hand">
        	<span class="tt_minicart__text">{__("small_cart")}</span>
        	<span class="tt_minicart__amount">0</span>
        </span>
    {/if}
{/hook}