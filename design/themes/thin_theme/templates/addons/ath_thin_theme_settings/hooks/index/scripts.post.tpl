{if $addons.ath_thin_theme_settings.lazy == "Y"}
	{script src="js/addons/ath_thin_theme_settings/lazyload.min.js"}
	
	<script>		
		var lazyLoadInstance = new LazyLoad({
		    elements_selector: ".lazy"
		});
		
		$(document).ajaxComplete(function(event,request, settings) {
			var lazyLoadInstance = new LazyLoad({
			    elements_selector: ".lazy"
			});	
		});
	</script>
{/if}


{script src="js/addons/ath_thin_theme_settings/jquery.customSelect.min.js"}
{script src="js/addons/ath_thin_theme_settings/classie.js"}
{script src="js/addons/ath_thin_theme_settings/jquery.menu-aim.js"}
{script src="js/addons/ath_thin_theme_settings/owl.carousel.min.js"}

{if $addons.ath_thin_theme_settings.sticky_header_desktop == "Y"}
	<script>		
	//stiky header
		_eStikyElem = $('.tt_header-menu__section');
		
		eStikyCancel = function() {
			$('.tygh-content').css({ 'padding-top' : 0 });
			_eStikyElem.removeClass('ath_eStik__fixed');
		}	
			
		eStikyCheck = function(_eStikyDot,_eStikyHeight) {
			
			$(window).scroll(function(){
				
			  var sticky = $('.tygh-header'),
			      scroll = $(window).scrollTop();
				
				if ($(window).scrollTop() > _eStikyDot){
					$('.tygh-content').css({ 'padding-top': _eStikyHeight });
					_eStikyElem.addClass('ath_eStik__fixed');
				} else {
					eStikyCancel();
				}
			});
		}
		
		eStikyInit = function() {
			eStikyCancel();
			
			if ($(window).width() > 767) {
				var _eStikyDot = _eStikyElem.offset().top;
				var _eStikyHeight = _eStikyElem.height();
		
				eStikyCheck(_eStikyDot,_eStikyHeight);
			}
		};	
	
		eStikyInit();
		
		$(document).ready(function(){
			eStikyInit();
		});
		
		$(window).resize(function(){
			eStikyInit();
		});	
	</script>
{/if}
{if $addons.ath_thin_theme_settings.sticky_header_mobile == "Y"}
	<script>					
	//stiky header Mob
		_eStikyMobElem = $('.header-grid');
		
		eStikyMobCancel = function() {
			$('.tygh-content').css({ 'padding-top' : 0 });
			_eStikyMobElem.removeClass('ath_eStikMob__fixed');
		}	
			
		eStikyMobCheck = function(_eStikyMobDot,_eStikyMobHeight) {
			
			$(window).scroll(function(){
				
			  var sticky = $('.tygh-header'),
			      scroll = $(window).scrollTop();
				
				if ($(window).scrollTop() > _eStikyMobDot){
					$('.tygh-content').css({ 'padding-top': _eStikyMobHeight });
					_eStikyMobElem.addClass('ath_eStikMob__fixed');
				} else {
					eStikyMobCancel();
				}
			});
		}
		
		eStikyMobInit = function() {
			eStikyMobCancel();
			
			if ($(window).width() < 767) {
				var _eStikyMobDot = _eStikyMobElem.offset().top + 150;
				var _eStikyMobHeight = _eStikyMobElem.height();
		
				eStikyMobCheck(_eStikyMobDot,_eStikyMobHeight);
			}
		};	
	
		eStikyMobInit();
		
		$(document).ready(function(){
			eStikyMobInit();
		});
		
		$(window).resize(function(){
			eStikyMobInit();
		});		
	</script>
{/if}
	