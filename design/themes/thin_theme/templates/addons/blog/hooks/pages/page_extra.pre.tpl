{if $page.page_type == $smarty.const.PAGE_TYPE_BLOG}

{if $subpages}

    {capture name="mainbox_title"}{/capture}

    <div class="th_blog">

        {include file="common/pagination.tpl"}
        
        <div class="grid-list grid-list--blog clearfix">

        {foreach from=$subpages item="subpage"}
			<div class="ty-column{$addons.ath_thin_theme_settings.blog_columns|default:2} ty-column--blog">
	            <div class="th_blog__item">
	                {if $subpage.main_pair}
		                <a href="{"pages.view?page_id=`$subpage.page_id`"|fn_url}">
		                    <div class="th_blog__img-block">
		                        {include file="common/image.tpl" lazy=$addons.ath_thin_theme_settings.lazy obj_id=$subpage.page_id images=$subpage.main_pair image_width=$addons.ath_thin_theme_settings.blog_img_width|default:386 image_height=$addons.ath_thin_theme_settings.blog_img_height|default:218 }
		                    </div>
			                <div class="th_blog__date">
				                <span class="th_blog__date__day">{$subpage.timestamp|date_format:"%d"}</span>
				                <span class="th_blog__date__month">{$subpage.timestamp|date_format:"%b"}</span>
				            </div>
		                </a>
					{else}
		                <div class="th_blog__date">
			                <span class="th_blog__date__day">{$subpage.timestamp|date_format:"%d"}</span>
			                <span class="th_blog__date__month">{$subpage.timestamp|date_format:"%b"}</span>
			            </div>					
	                {/if}
	                <a href="{"pages.view?page_id=`$subpage.page_id`"|fn_url}" class="th_blog__post-title__contener">
	                    <span class="th_blog__post-title">
	                        {$subpage.page}
	                    </span>
						<span class="th_blog__author">{__("by")} {$subpage.author}</span>
	                </a>
	                
	            </div>
			</div>
        {/foreach}
        
		</div>

        {include file="common/pagination.tpl"}
    </div>

{/if}

{if $page.description}
    {capture name="mainbox_title"}<span class="ty-blog__post-title" {live_edit name="page:page:{$page.page_id}"}>{$page.page}</span>{/capture}
{/if}

{/if}
