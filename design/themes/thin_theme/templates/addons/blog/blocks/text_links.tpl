{** block-description:blog.text_links **}

{assign var="parent_id" value=$block.content.items.parent_page_id}
{if $items}
<div class="ty-blog-text-links">
    <ul>
    {foreach from=$items item="page" name="fe_blog"}
        <li class="ty-blog-text-links__item">
            <a href="{"pages.view?page_id=`$page.page_id`"|fn_url}">

                <div class="th_blog__date">
	                <span class="th_blog__date__day">{$page.timestamp|date_format:"%d"}</span>
	                <span class="th_blog__date__month">{$page.timestamp|date_format:"%b"}</span>
	            </div>
				
				<div class="ty-blog-sidebox__title">
		            <span class="th_blog__name">{$page.page}</span>
		            <span class="th_blog__author">{__("by")} {$page.author}</span>
				</div>
	            
	        </a>
        </li>
    {/foreach}
    </ul>
    {if $parent_id}
        <div class="ty-mtb-xs ty-left">
            {include file="buttons/button.tpl" but_href="pages.view?page_id=`$parent_id`" but_text=__("view_all") but_role="text" but_meta="ty-btn__secondary blog-ty-text-links__button"}
        </div>
    {/if}
</div>
{/if}