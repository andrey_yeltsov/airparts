{** block-description:blog.recent_posts **}

{if $items}

<div class="ty-blog-sidebox">
    <ul class="ty-blog-sidebox__list">
{foreach from=$items item="page"}
        <li class="ty-blog-sidebox__item">
            <a href="{"pages.view?page_id=`$page.page_id`"|fn_url}">

                <div class="th_blog__date">
	                <span class="th_blog__date__day">{$page.timestamp|date_format:"%d"}</span>
	                <span class="th_blog__date__month">{$page.timestamp|date_format:"%b"}</span>
	            </div>
				
				<div class="ty-blog-sidebox__title">
		            <span class="th_blog__name">{$page.page}</span>
		            <span class="th_blog__author">{__("by")} {$page.author}</span>
				</div>
	            
	            </a>
        </li>
{/foreach}
    </ul>
</div>

{/if}