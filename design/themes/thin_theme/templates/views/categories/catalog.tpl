{* 4.7.2 *}
{hook name="categories:catalog"}
{include file="common/catalog_grid.tpl" items=$root_categories columns=$addons.ath_thin_theme_settings.categories_columns image_width=$settings.Thumbnails.category_lists_thumbnail_width image_height=$settings.Thumbnails.category_lists_thumbnail_height}
{/hook}