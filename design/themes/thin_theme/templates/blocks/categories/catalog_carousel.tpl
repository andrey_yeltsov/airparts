{** block-description: catalog_carousel_description **}

{assign var="obj_prefix" value="`$block.block_id`000"}

{if $block.properties.outside_navigation == "Y"}
    <div class="owl-theme ty-owl-controls">
        <div class="owl-controls clickable owl-controls-outside"  id="owl_outside_nav_{$block.block_id}">
            <div class="owl-buttons">
                <div id="owl_prev_{$obj_prefix}" class="owl-prev"><i class="ty-icon-left-open-thin"></i></div>
                <div id="owl_next_{$obj_prefix}" class="owl-next"><i class="ty-icon-right-open-thin"></i></div>
            </div>
        </div>
    </div>
{/if}

<div class="categories-catalog__scroll__wrapper">
	<div id="scroll_list_{$block.block_id}" class="owl-carousel ty-scroller-list categories-catalog__scroll grid-list--categories-catalog clearfix">
	{foreach from=$items item="category"}
	    {if $category}
	        <div class="ty-column--categories-catalog ty-column__scroll-item ty-scroller-list__item">
	            <a href="{"categories.view?category_id=`$category.category_id`"|fn_url}" class="ath_subcategories-link">
	                {if $category.main_pair}
	                    {include file="common/image.tpl"
	                        show_detailed_link=false
	                        images=$category.main_pair
	                        no_ids=true
	                        image_id="category_image"
	                        image_width=$image_width
	                        image_height=$image_height
	                        class="ath_subcategories-img"
	                    }
	                {/if}
	                <span class="ath_subcategories-name">
	                	{$category.category}
	                </span>
	            </a>
	        </div>
	    {/if}
	{/foreach}
	</div>
</div>

{include file="common/scroller_init.tpl" prev_selector="#owl_prev_`$obj_prefix`" next_selector="#owl_next_`$obj_prefix`"}
