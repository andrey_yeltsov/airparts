{** block-description:grid_simple **}
{* TH *}

{if $block.properties.hide_add_to_cart_button == "Y"}
    {assign var="_show_add_to_cart" value=false}
{else}
    {assign var="_show_add_to_cart" value=true}
{/if}

{assign var="_show_name" value="true"}

{include file="blocks/list_templates/grid_simple.tpl"
columns=$block.properties.number_of_columns
products=$items 
obj_prefix="`$block.block_id`000" 
item_number=$block.properties.item_number 
show_name=false 
show_trunc_name=true 
show_price=true
show_old_price=true

show_product_labels=true
show_discount_label=true
show_shipping_label=true
show_list_discount=true
product_labels_mini=true

show_rating=true 
show_add_to_cart=$_show_add_to_cart 
show_list_buttons=false
add_to_cart_meta="text-button-add"
but_role="text"}
