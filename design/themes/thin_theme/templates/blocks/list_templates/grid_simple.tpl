{* TH *}
{assign var="small_items_image_width" value="70"}
{assign var="small_items_image_height" value="70"}

<ul class="ty-template-small grid-list">
{foreach from=$products item="product" name="products"}
    {assign var="obj_id" value=$product.product_id}
    {assign var="obj_id_prefix" value="`$obj_prefix``$product.product_id`"}
    {include file="common/product_data.tpl" product=$product}
    {hook name="products:product_small_item__grid_simple"}
    <li class="ty-template-small__item clearfix ty-column{$columns}">
        {assign var="form_open" value="form_open_`$obj_id`"}
        {$smarty.capture.$form_open nofilter}
            <div class="ty-template-small__item-img">
                <a href="{"products.view?product_id=`$product.product_id`"|fn_url}">{include file="common/image.tpl" lazy=$addons.ath_thin_theme_settings.lazy image_width=$small_items_image_width image_height=$small_items_image_height images=$product.main_pair obj_id=$obj_id_prefix no_ids=true}
	                
	                {assign var="product_labels" value="product_labels_`$obj_prefix``$obj_id`"}
	                {$smarty.capture.$product_labels nofilter}
	                
                </a>
            </div>
            <div class="ty-template-small__item-description">
                {if $block.properties.item_number == "Y"}{$smarty.foreach.products.iteration}.&nbsp;{/if}
                {assign var="name" value="name_$obj_id"}{$smarty.capture.$name nofilter}
				
				

                {if $show_price}
                <div class="ty-template-small__item-price">
                    {assign var="price" value="price_`$obj_id`"}
                    {$smarty.capture.$price nofilter}
                    
                    {assign var="old_price" value="old_price_`$obj_id`"}
                    {if $smarty.capture.$old_price|trim}{$smarty.capture.$old_price nofilter}{/if}
                </div>
                {/if}

                {assign var="rating" value="rating_$obj_id"}
                {$smarty.capture.$rating nofilter}

                {assign var="add_to_cart" value="add_to_cart_`$obj_id`"}
                {if $smarty.capture.$add_to_cart|trim}<p>{$smarty.capture.$add_to_cart nofilter}</p>{/if}
            </div>
        {assign var="form_close" value="form_close_`$obj_id`"}
        {$smarty.capture.$form_close nofilter}
    </li>
    {/hook}
{/foreach}
</ul>