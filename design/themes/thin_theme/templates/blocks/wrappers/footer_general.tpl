{* 4.7.2 *}
{if $content|trim}
    <div class="{$sidebox_wrapper|default:"ty-footer"}{if isset($hide_wrapper)} cm-hidden-wrapper{/if}{if $hide_wrapper} hidden{/if}{if $block.user_class} {$block.user_class}{/if}{if $content_alignment == "RIGHT"} ty-float-right{elseif $content_alignment == "LEFT"} ty-float-left{/if}">
	    {* TH *}
        <h4 class="ty-footer-general__header ty-footer-general__header--no-hiden {if $header_class} {$header_class}{/if}">
            {hook name="wrapper:footer_general_title"}
            {if $smarty.capture.title|trim}
                {$smarty.capture.title nofilter}
            {else}
                <span>{$title nofilter}</span>
            {/if}
            {/hook}

        </h4>
        <div class="ty-footer-general__body ty-footer-general__body--no-hiden">{$content|default:"&nbsp;" nofilter}</div>
	    {* TH *}
    </div>

{/if}