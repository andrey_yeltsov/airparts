{* 4.7.2 *}
{$obj_prefix = "`$block.block_id`000"}

{if $block.properties.outside_navigation == "Y"}
    <div class="owl-theme ty-owl-controls">
        <div class="owl-controls clickable owl-controls-outside" id="owl_outside_nav_{$block.block_id}">
            <div class="owl-buttons">
                <div id="owl_prev_{$obj_prefix}" class="owl-prev"><i class="ty-icon-left-open-thin"></i></div>
                <div id="owl_next_{$obj_prefix}" class="owl-next"><i class="ty-icon-right-open-thin"></i></div>
            </div>
        </div>
    </div>
{/if}

<div id="scroll_list_{$block.block_id}" class="owl-carousel{* TH *} th_brand-scroller{* /TH *}">
    {foreach from=$brands item="brand" name="for_brands"}
	        {* TH *}
			{if $smarty.foreach.for_brands.index == $block.properties.limit}
				{break}
			{/if}
			
            {include file="common/image.tpl" assign="object_img" class="th_brand-scroller__img" image_width=$block.properties.thumbnail_width image_height=$block.properties.thumbnail_width images=$brand.image_pair no_ids=true lazy_load=true obj_id="scr_`$block.block_id`000`$brand.variant_id`"} 
            {* /TH *}
            <div class="ty-center">
	            {* TH *}
                <a href="{"product_features.view?variant_id=`$brand.variant_id`"|fn_url}" class="th_brand-scroller__link">{$object_img nofilter}</a>
	            {* /TH *}
            </div>
    {/foreach}
</div>
{include file="common/scroller_init.tpl" items=$brands prev_selector="#owl_prev_`$obj_prefix`" next_selector="#owl_next_`$obj_prefix`"}
