{** block-description:original_promo **}

<div class="th_simple_banner--tt__wrapper">
	<div class="th_simple_banner--tt">
		
	{foreach from=$items item="banner" key="key"}
	    {if $banner.type == "G" && $banner.main_pair.image_id}
	    
		    {if $banner.url != ""}<a href="{$banner.url|fn_url}" class="th_simple_banner--tt__link" {if $banner.target == "B"}target="_blank"{/if}>{/if}
			    
	        {include file="common/image.tpl" lazy=$addons.ath_thin_theme_settings.lazy images=$banner.main_pair image_auto_size=true}
	
		    <div class="th_simple_banner--tt__block">
			    {if $block.properties.top_line}
			    <div class="th_simple_banner--tt__top-line">{__($block.properties.top_line)}</div>
			    {/if}
			    {if $block.properties.title}
			    <div class="th_simple_banner--tt__title">{__($block.properties.title)}</div>
			    {/if}
			    {if $block.properties.btn_text}
			    <span class="ty-btn ty-btn__primary">{__($block.properties.btn_text)}</span>
			    {/if}
		    </div>
		    
		   {if $banner.url != ""}</a>{/if}
	
	    {/if}
	{/foreach}
		
		
	</div>
</div>