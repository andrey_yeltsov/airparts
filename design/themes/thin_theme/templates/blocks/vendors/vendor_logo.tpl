{** block-description:block_vendor_logo **}
{* 4.7.2 *}
{* TH *}
<div class="th_vendor-logo__container">
	<a class="th_vendor-logo__link" href="{"companies.view?company_id=`$vendor_info.company_id`"|fn_url}">
		{include file="common/image.tpl" images=$vendor_info.logos.theme.image image_height="110" alt=$vendor_info.logos.theme.image.alt class="th-vendor-logo"}</a>
{* /TH *}
</div>
