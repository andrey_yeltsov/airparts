/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2020   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and accept    *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
(function(_, $) {
function fn_abt__yt_next_banner (id, banner_list, lS_key){
var new_banner= 0;
var old_banner = 0;
if(localStorage.getItem(lS_key + id) !== null){
old_banner = localStorage.getItem(lS_key + id);
}
var banners = banner_list.split(',');
if (banners.length === 1){
new_banner = banners[0];
}else if (banners.length > 1){
var curr_pos = banners.indexOf(old_banner);
if (curr_pos === -1) {
new_banner = banners[0];
} else {
banners[banners.length] = banners[0];
new_banner = banners[curr_pos + 1];
}
}
localStorage.setItem(lS_key + id, new_banner);
return new_banner;
}
function fn_abt__yt_init_countdown(e){
e.find('span[data-countdown]').each(function() {
var cd = $(this);
cd.countdown(cd.data('countdown'), function(event) {
cd.html(event.strftime(cd.data('str')));
});
});
}
$.ceEvent('on', 'ce.commoninit', function(context) {
fn_abt__yt_init_countdown(context);
});
$.ceEvent('on', 'ce.commoninit', function(context) {
var banners = context.find("div[class*=bnw-x]:not('.loaded')");
if (banners.length) {
var lS_key = "abyt_";
var bs = [];
var result_ids = [];
banners.each(function () {
var $self = $(this);
var parent = $self.parents(".ty-scroller-list, .grid-list");
var has_buy_buttons = parent.find(".ty-grid-list__control");
var t = $self.children('div');
var obj = t.data('o'),
obj_id = t.data('i'),
obj_banners = t.data('b'),
id = t.attr('id');
result_ids.push(id);
bs.push({
id: id,
banner: fn_abt__yt_next_banner(id, obj_banners.toString(), lS_key),
obj_id: obj_id.toString(),
obj: obj,
has_buttons: !!has_buy_buttons.length
});
});
if (bs.length) {
$.ceAjax('request', fn_url('abt__yt.get_banner'), {
result_ids: result_ids.join(','),
method: 'post',
data: {banners: bs,},
caching: true,
hidden: true
});
}
}
});
$.ceEvent('on', 'ce.commoninit', function(context) {
(function(){
if (window.innerWidth >= 768) {
var filters_wrap = context.find(".ty-product-filters__wrapper");
if (filters_wrap.length && !filters_wrap.parent().hasClass("vertical-position")) {
filters_wrap.parent().css("display", "block");
var btn = filters_wrap.find(".h-filters-more-btn");
var btn_w = btn.outerWidth(true);
var max_w = filters_wrap.width() - (filters_wrap.find(".ypi-white-cfbt").width() + (context.find(".ypi-white-vfbt").outerWidth(true) || 0)) - btn_w - 10;
var max_w_with_btn = max_w - btn_w;
var hide_filters = false;
var filters_w_counter = 0;
filters_wrap.find(".ty-horizontal-product-filters-dropdown").each(function () {
var filter = $(this);
filters_w_counter += Math.ceil(filter.width());
if (filters_w_counter > max_w) {
filter.addClass("hidden");
hide_filters = true;
} else if (filters_w_counter > max_w_with_btn) {
filter.addClass("if-big-w-hidden");
}
});
if (hide_filters === true) {
filters_wrap.find(".if-big-w-hidden").addClass("hidden");
btn.removeClass("hidden");
btn.click(function () {
filters_wrap.find(".ty-horizontal-product-filters-dropdown.hidden").removeClass("hidden");
btn.hide();
});
}
}
}
})();
});
$(document).on('click', 'div[class*=abyt-in-popup-]', function() {
var bp_container = $(this).find("div.abt-yt-bp-container:not('.loaded')");
if (bp_container.length){
var link = bp_container.data('link');
$.ceAjax('request', fn_url('abt__yt.show_banner_in_popup'), {
method: 'get',
data : {link_id : link,},
hidden: true,
callback: function(response) {
bp_container
.addClass('loaded')
.html(response.data)
.popup({
scrolllock: true,
openelement : '.abyt-in-popup-' + link,
closeelement : '.abyt-closer-' + link,
transition: 'all 0.1s',
color: 'black',
escape: false,
blur: false,
outline: true,
opacity: 0.5,
onclose : function (){
var player = $('#youtube-player-' + link);
if (player.length){
player[0].contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
}
},
})
.popup('show');
$.commonInit(bp_container);
},
});
}
});
$.ceEvent('on', 'dispatch_event_pre', function(e, jelm, processed) {
var link = jelm.is('a') ? jelm : jelm.parent();
if (e.type === 'click' && link.hasClass('cm-ajax') && link.data('caEventName') === 'ce.abt__yt_more_products_callback') {
link.parent().addClass('load');
$('#tygh_container').addClass('loading-more-products');
}
});
$.ceEvent('on', 'ce.ajaxlink.done.ce.abt__yt_more_products_callback', function(e, data) {
$('.more-products-link.load').removeClass('load').hide();
$("#tygh_container").removeClass("loading-more-products");
});
function fn_abt__yt_load_video (elm){
var id = elm.data('banner-youtube-id'), params = elm.data('banner-youtube-params');
elm.addClass('loaded').empty().append($('<iframe>', {
src: "https://www.youtube.com/embed/"+ id +"?" + params,
frameborder: 0,
allowfullscreen: 'true',
allowscriptaccess: 'always',
})
);
}
$(_.doc).on('click', 'a[data-content="video"]:not(.loaded),div.abyt-a-video .box:not(.loaded),div[data-banner-youtube-id]', function(e) {
if ($(e.target).attr('data-banner-youtube-id') || $(e.target).attr('data-type')){
var elm = $(e.target);
if ($(e.target).attr('data-type')) elm = elm.parent();
$(this).addClass('loaded');
fn_abt__yt_load_video(elm);
return false;
}
return true;
});
$(document).on('click', 'button[type=submit][name^="dispatch[checkout.add"]', function() {
$.ceEvent('on', 'ce.notificationshow', function(notification) {
if (document.documentElement.clientWidth <= 767){
if (notification.find('.ty-ab__ia_joins').length){
notification.addClass('with-ab-ia-joins');
$("html, body").animate({scrollTop: 0}, 400);
}
}
});
});
$.ceEvent('on', 'ce.commoninit', function(context) {
var filter = $('.cm-horizontal-filters'),
container = $('.ypi-filters-container');
if (container.length) {
if (filter.length && !$.contains(container, filter)) {
filter.appendTo(container);
filter.css('visibility', 'visible');
} else if (!filter.length) {
container.addClass('ypi-nofilters');
}
}
});
$(document).ready(function () {
var top_panel = $("#tygh_main_container > .tygh-top-panel"),
header = $('#tygh_main_container > .tygh-header'),
b = $('body'),
top_panel_height = top_panel.height(),
header_height = header.height(),
height = top_panel_height + header_height,
fixed = 'fixed-top';
$(window).on("resize scroll",function(e){
if (document.documentElement.clientWidth > 767){
var scroll = $(window).scrollTop();
if (scroll >= height && !b.hasClass(fixed)){
header.css('padding-top', top_panel_height + 'px');
b.addClass(fixed);
window.setTimeout(function(){b.addClass('show');}, 50);
}else if (scroll < height && b.hasClass(fixed)){
header.css('padding-top', '');
b.removeClass(fixed + " show menu_fixed-top");
}
}
});
});
$(document).ready(function () {
if (document.documentElement.clientWidth >= 1124) {
var b = $('body'),
delta = 50,
sticky_height = 0,
sticky_top = 0,
sticky_height_top = 0,
product_panel_max_height = 300,
fixed = 'fixed-product-form';
$(window).on("resize scroll",function(e){
var p_detail = $(".ty-product-detail"),
scroll = $(window).scrollTop(),
tabs = $(".ypi-dt-product-tabs"),
sticky = $(".sticky-block"),
p_sticky = sticky.parent();
if (tabs.length){
var product_block = $('.ty-product-block__wrapper > .ypi-product-block__left'),
tabs_height = tabs.height();
if (tabs_height >= product_panel_max_height && product_block.length){
sticky_height_top = (sticky_height + sticky_top === 0) ? 280 : sticky_height + sticky_top;
if (
(!b.hasClass(fixed) && scroll + delta > tabs.offset().top && scroll + (sticky_height_top) < tabs.offset().top + tabs_height)
){
p_sticky.css("height", p_sticky.height());
b.addClass(fixed).addClass('hide-cloudzoom');
p_detail.addClass("fixed-form").addClass('view');
if (_.language_direction === 'ltr'){
sticky.css("right", product_block.offset().left).css("opacity", 0).stop().fadeTo(300, 1);
}else{
sticky.css("left", ($(window).width() - (product_block.offset().left + product_block.outerWidth()))).css("opacity", 0).stop().fadeTo(300, 1);
}
sticky_height = sticky.outerHeight();
sticky_top = sticky.position().top;
}else if (
(b.hasClass(fixed) && scroll + delta < tabs.offset().top)
||
(b.hasClass(fixed) && scroll + (sticky_height_top) > tabs.offset().top + tabs_height)
){
p_sticky.css("height", "");
b.removeClass(fixed).removeClass('hide-cloudzoom');
p_detail.removeClass("fixed-form").removeClass("view");
}
}
}
});
$(window).trigger('scroll');
}
});
(function(_, $){
var container,
menu,
wrapper,
show_more_btn,
items,
mobile_menu,
items_widths,
total_width,
total_sections = 1,
current_menu_section = 1,
prev_width = 0;
function show_menu_item(elem) {
elem.removeClass('ab__menu_hidden');
}
function hide_menu_item(elem) {
elem.addClass('ab__menu_hidden');
}
function toggle_menu_items() {
show_more_btn.html(current_menu_section + '/' + total_sections + "<i></i>");
var arr,
widths = $.extend(true, [], items_widths),
section;
if (current_menu_section === total_sections) {
arr = $(items.get().reverse());
section = 1;
widths.reverse();
show_more_btn.addClass('last-section');
} else {
arr = items;
section = current_menu_section;
show_more_btn.removeClass('last-section');
}
var sum = 0;
arr.each(function(i) {
var $item = $(this);
sum += widths[i];
if (section > 1 && sum > total_width) {
sum = widths[i];
section -= 1;
show_menu_item($item);
} else if (section > 1 || sum > total_width) {
hide_menu_item($item);
} else {
show_menu_item($item);
}
});
}
function ab__menu_init() {
var new_width = window.innerWidth;
if (new_width == prev_width || new_width <= 767) {
return true;
}
prev_width = new_width;
container = $('.abt_up-menu.extended');
menu = $('.ty-menu__items', container);
wrapper = menu.parent();
show_more_btn = $('.abt_yp_menu-show_more', container);
items = menu.children().filter(':not(.visible-phone)');
mobile_menu = menu.children().filter('.visible-phone');
if (mobile_menu.css('display') === 'none') {
container.addClass('extended');
var sum = 0;
items_widths = [];
total_width = container.innerWidth() - show_more_btn.outerWidth() - 5;
menu.css('visibility', 'hidden');
show_menu_item(items);
items.css('width', 'auto');
items.each(function(i) {
var item = $(this);
items_widths[i] = item.outerWidth();
item.css('width', item.innerWidth());
sum += items_widths[i];
});
menu.css('width', sum);
total_sections = Math.ceil(sum/total_width);
if (total_sections > 1) {
wrapper.css('width', total_width);
show_more_btn.css('display', 'inline-block');
} else {
show_more_btn.hide();
}
current_menu_section = _get_init_menu_section();
toggle_menu_items();
menu.css('visibility', 'visible');
show_more_btn.off('click');
show_more_btn.click(function(){
if (current_menu_section === total_sections) {
current_menu_section = 1;
} else {
current_menu_section += 1;
}
toggle_menu_items();
});
} else {
show_menu_item(items);
items.css('width', 'auto');
menu.css('width', 'auto');
wrapper.css('width', 'auto');
container.removeClass('extended');
}
function _get_init_menu_section() {
var sum = 0;
var section = 1;
var active_found = false;
items.each(function() {
if (!active_found) {
var item = $(this);
sum += item.outerWidth(true);
if (sum >= total_width) {
section++;
sum = 0;
}
if (item.hasClass("ty-menu__item-active")) {
active_found = true;
}
}
});
return active_found === true ? section : 1;
}
}
$(document).ready(function(){
setTimeout(ab__menu_init, 500);
$(window).resize(function(){
ab__menu_init();
});
if (window.addEventListener) {
window.addEventListener('orientationchange', function() {
ab__menu_init();
}, false);
}
$.ceEvent('on', 'ce.commoninit', function(context) {
if ($('.abt_up-menu.extended', context).length) {
ab__menu_init();
}
});
});
})(Tygh, Tygh.$);
$(_.doc).on('click', '.ab__show_qty_discounts', function() {
var product_id = $(this).data('caProductId');
if (product_id !== undefined) {
var container = $('#qty_discounts_' + product_id);
if (container.is(':empty')) {
$.ceAjax('request', fn_url('abt__yt.qty_discounts?product_id=' + product_id), {
method: 'get',
result_ids: 'qty_discounts_' + product_id
});
}
container.toggleClass('hidden');
}
});
(function($) {
$(document).on('click', ".abt-yt-show-more-features span", function() {
var btn = $(this);
var parent = btn.parents(".ty-grid-list__item");
if (parent.hasClass("abt-yt-loaded-features")) {
setTimeout(function(){
parent.removeClass("abt-yt-loaded-features");
parent.find(".uploaded-additional-features").css("display", "none");
}, 125);
} else {
var featuresL= parent.find(".abt-yt-full-features-list");
var productId = parseInt(featuresL.attr("data-product-id"));
var categoryId = parseInt(featuresL.attr("data-main-category-id"));
parent.find("div[id^='product_features_update_'] > *").addClass("initial-feature");
parent.find(".uploaded-additional-features").css("display", "block");
parent.addClass("abt-yt-loaded-features");
if (!parent.hasClass("loaded") && productId > 0 && categoryId > 0) {
var block_middle = parent.find(".block-middle");
addIndicator( block_middle );
$.ceAjax('request', fn_url('abt__yt.get_features'), {
method: 'post',
data: {
product: productId,
category: categoryId,
existing_ids: fn_abt__yt_get_existing_features(parent)
},
hidden: true,
caching: false,
callback: function (d) {
block_middle.removeIndicator();
if (d.getted_features != void(0)) {
parent.addClass("loaded");
parent.find("div[id^='product_features_update_']").append('<div class="uploaded-additional-features">' + d.getted_features + '</div>');
}
}
});
}
}
function addIndicator( elem ) {
elem.append('<div class="abt-indicator-wrapper"><div class="abt-indicator-circle"><span></span></div></div>');
elem.removeIndicator = function newFuncQ() {
elem.find(".abt-indicator-wrapper").remove();
elem.removeIndicator = false;
}
}
function fn_abt__yt_get_existing_features(parent) {
var features = parent.find("div[id^='product_features_update_'] > em .ty-control-group");
var features_string_arr = [];
features.each(function(index){
features_string_arr[index] = $(this).attr("data-feature-id");
});
return features_string_arr.join(',');
}
});
var h_menu = $(".top-menu-grid .abt_up-menu .ty-menu__items");
if (h_menu.length) {
var elems = h_menu.find(".ty-menu__item");
elems.mouseenter(function(event) {
if (event.relatedTarget !== null) {
var $this = $(this);
var subelem = $this.find(".ty-menu__submenu-items").css("display", "none");
var timeout = 250;
setTimeout(function () {
subelem.css("display", '');
}, timeout);
}
});
var menu_contextmenu_timeout;
$(document).contextmenu(function (event) {
var submenu = $(".ty-menu__submenu");
if (submenu.is(event.target) || submenu.has(event.target).length !== 0) {
clearTimeout(menu_contextmenu_timeout);
var menu_elem = $(event.target).parents(".ty-menu__item");
menu_elem.attr("tabindex", "-1").focus();
menu_contextmenu_timeout = setTimeout(function(){
menu_elem.blur();
}, 1500);
}
});
}
})($);
}(Tygh, Tygh.$));
(function(_, $) {
$.ceEvent('on', 'ce.commoninit', function (context) {
$(document).on('click', '.abt_up-menu li.ty-menu__item > a.cm-responsive-menu-toggle', _.abt__yt.functions.toggle_menu);
context.find("span.link-more").on("mouseover", function() {
var _self = $(this);
_self.prev().addClass('view');
_self.addClass("hidden");
});
});
$(document).on('ready', function () {
if ( _.abt__yt.settings.general.breadcrumbs_view === "hide_with_btn" && (_.abt__yt.device === "mobile" || _.abt__yt.device === "tablet") && window.innerWidth <= 768 ) {
(function() {
var main_content_breadcrumbs = $(".main-content-grid[class^='span']");
var m_c_b_w = main_content_breadcrumbs.outerWidth();
var mobile_breadcrumbs = $(".ty-breadcrumbs").css({
"display": "inline-block",
"white-space": "nowrap",
});
var m_b_w = mobile_breadcrumbs.outerWidth(true);
if (m_b_w >= m_c_b_w) {
mobile_breadcrumbs.addClass("long");
}
mobile_breadcrumbs.css({
"display": '',
"white-space": '',
});
})();
}
$(document).on('click', '.ypi-menu__fixed-button', function () {
$('.ypi-menu__fixed-button').toggleClass('active');
$('body').toggleClass('menu_fixed-top');
});
$(document).on('click', '.closer_fixed-menu', function () {
$('body').removeClass('menu_fixed-top');
$('.ypi-menu__fixed-button').removeClass('active');
});
$(document).on('click', '.ypi-filters-container .ypi-white-vfbt', function () {
$('.ty-horizontal-product-filters').toggleClass('vertical-position');
$('.ty-horizontal-product-filters-dropdown__content').toggleClass('hidden').css('display','inline-block');
$('.ty-horizontal-product-filters .v-filters-header').toggleClass('hidden');
$('.ypi-filters-container .ypi-white-vfbt').toggleClass('open');
$('body').toggleClass('body-not-scroll');
});
$(document).on('click', '.ty-horizontal-product-filters.vertical-position .closer', function () {
$('.ty-horizontal-product-filters').removeClass('vertical-position');
$('.v-filters-header').addClass('hidden');
$('.ty-horizontal-product-filters-dropdown__content').toggleClass('hidden').css('display','none');
$('.ty-horizontal-product-filters-dropdown__wrapper').removeClass('open');
$('.ypi-filters-container .ypi-white-vfbt').removeClass('open');
$('body').removeClass('body-not-scroll');
});
$(document).on('click', '.enable-v-filters .ypi-filters-container .ypi-white-vfbt', function () {
$('.enable-v-filters').addClass('mobile-view');
});
$(document).on('click', '.enable-v-filters .v-filter .closer', function () {
$('.enable-v-filters').removeClass('mobile-view');
$('.ypi-filters-container .ypi-white-vfbt').removeClass('open');
$('body').removeClass('body-not-scroll');
});
$(document).on('click', '.ToggleItem', function () {
$(this).toggleClass('is-open');
});
});
}(Tygh, Tygh.$));
(function (_, $) {
function fn_abt_timer_menu () {
var timer, timer2, opened_menu = null, opened_menu2 = null, second_lvl;
$('.first-lvl').hover(function () {
var elem = $(this).children('.ty-menu__submenu').children('ul');
clearTimeout(timer);
timer = setTimeout(function () {
if (opened_menu !== null) {
opened_menu.hide();
opened_menu = null;
}
opened_menu = elem.show();
}, 100);
});
$('.hover-zone').mouseleave(function () {
clearTimeout(timer);
if (opened_menu !== null) {
opened_menu.hide();
opened_menu = null;
}
});
$('.second-lvl').hover(function () {
second_lvl = $(this);
var elem = $(this).children('.ty-menu__submenu');
clearTimeout(timer2);
timer2 = setTimeout(function () {
if (opened_menu2 !== null) {
second_lvl.removeClass('hover2');
opened_menu2 = null;
}
$('.second-lvl').removeClass('hover2');
second_lvl.addClass('hover2');
}, 100);
});
$('.hover-zone2').mouseleave(function () {
clearTimeout(timer2);
$('.second-lvl').removeClass('hover2');
if (opened_menu2 !== null) {
opened_menu2.hide();
$('.second-lvl').removeClass('hover2');
opened_menu2 = null;
}
});
}
if (document.documentElement.clientWidth >= 768) {
if ($('.top-menu-grid-vetrtical .ty-dropdown-box__title').hasClass('open')) {
$('.top-menu-grid-vetrtical .ty-dropdown-box__content').removeClass('hover-zone');
} else {
$('.top-menu-grid-vetrtical .ty-dropdown-box__content').addClass('hover-zone');
$('.hover-zone').hover(
function () {
$('body').addClass('shadow')
},
function () {
$('body').removeClass('shadow')
}
);
}
$(document).ready(function () {
if (!$('div.abtam').length){
fn_abt_timer_menu();
}
});
}
$(document).ready(function () {
(function(){
var abtam = $('div.abtam');
if (abtam.length){
var ids = [];
abtam.each(function(){
ids.push($(this).attr('id'));
});
$.ceAjax('request', fn_url('abt__yt.load_menu'), {
result_ids: ids.join(','),
method: 'post',
hidden: true,
callback: function(data) {
if (document.documentElement.clientWidth >= 768) {
fn_abt_timer_menu();
}
$(document).on('click', '.abt_up-menu li.ty-menu__item > a.cm-responsive-menu-toggle', _.abt__yt.functions.toggle_menu);
}
});
}
})();
});
})(Tygh, Tygh.$);