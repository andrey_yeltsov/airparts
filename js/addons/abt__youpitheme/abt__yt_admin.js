/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2020   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and accept    *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
(function(_, $) {
$.ceEvent('on', 'ce.ajaxdone', function(elms, scripts, params, response_data, response_text) {
if (/abt_group_bt_/.test(params.result_ids) && elms.length) {
var form = $('form', elms[0]);
if (form.length) {
$('.buttons-container .btn', form).attr('name', 'dispatch[abt__yt_buy_together.update]');
}
}
});
function abt__yt_copy_to_clipboard( text ) {
if (window.clipboardData && window.clipboardData.setData) {
return clipboardData.setData("Text", text);
} else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
var textarea = document.createElement("textarea");
textarea.textContent = text;
textarea.style.position = "fixed";
document.body.appendChild(textarea);
textarea.select();
try {
return document.execCommand("copy");
} catch (ex) {
console.warn("Copy to clipboard failed", ex);
return false;
} finally {
document.body.removeChild(textarea);
}
}
}
$(".ab-yt-icon-copy").click( function(e) {
var link = $(this);
var tr = link.parents("tr");
var text = tr.find("td:nth-child(3)").text();
abt__yt_copy_to_clipboard(text);
$.ceNotification("show", {
type: 'N',
title: Tygh.tr("notice"),
message: _.tr("abt__yt_copied_to_clipboar").replace("[text]", text)
});
e.preventDefault();
});
}(Tygh, Tygh.$));
