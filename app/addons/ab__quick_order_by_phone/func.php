<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2019   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and  accept   *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
use Tygh\Registry;
use Tygh\BlockManager\Block;
use Tygh\BlockManager\Layout;
if (!defined('BOOTSTRAP')) { die('Access denied'); }
function fn_settings_variants_addons_ab__quick_order_by_phone_order_status(){
$statuses = fn_get_statuses();
$data = array();
if (is_array($statuses) and !empty($statuses)){
foreach ($statuses as $id=>$s){
$data[$id] = $s['description'];
}
}
return (is_array($data) and !empty($data))?$data:false;
}
function fn_settings_variants_addons_ab__quick_order_by_phone_shipping_method(){
$data = array(0=>'---') + fn_get_shippings(true);
return (is_array($data) and !empty($data))?$data:false;
}
function fn_settings_variants_addons_ab__quick_order_by_phone_amount_of_quick_orderds(){
$data = range(0, 20, 1);
return (is_array($data) and !empty($data))?$data:false;
}
function fn_ab__qobp_check_phone ($p){
return preg_match('/^([\+]+)*[0-9\x20\x28\x29\-]{10,20}$/', $p);
}
function fn_ab__qobp_format_email ($e){
if (strlen($e) > 5 and strpos($e,'@') !== false){
list($a, $b) = explode('@', $e);
return $a . microtime(true) . '@' . $b;
}
return false;
}
function fn_ab__qobp_is_bot ($user_agent){
$crawlers_agents = array('googlebot','googlebot-mobile','googlebot-image','googlebot-news','googlebot-video','mediapartners-google','bingbot','slurp','java','wget','curl','commons-httpclient','python-urllib','libwww','httpunit','nutch','go-http-client','phpcrawl','msnbot','jyxobot','fast-webcrawler','fast enterprise crawler','biglotron','teoma','convera','seekbot','gigabot','gigablast','exabot','ngbot','ia_archiver','gingercrawler','webmon','httrack','webcrawler','grub.org','usinenouvellecrawler','antibot','netresearchserver','speedy','fluffy','bibnum.bnf','findlink','msrbot','panscient','yacybot','aisearchbot','ioi','ips-agent','tagoobot','mj12bot','dotbot','woriobot','yanga','buzzbot','mlbot','yandexbot','purebot','linguee bot','voyager','cyberpatrol','voilabot','baiduspider','citeseerxbot','spbot','twengabot','postrank','turnitinbot','scribdbot','page2rss','sitebot','linkdex','adidxbot','blekkobot','ezooms','dotbot','mail.ru_bot','discobot','heritrix','findthatfile','europarchive.org','nerdbynature.bot','sistrix crawler','ahrefsbot','aboundex','domaincrawler','wbsearchbot','summify','ccbot','edisterbot','seznambot','ec2linkfinder','gslfbot','aihitbot','intelium_bot','facebookexternalhit','yeti','retrevopageanalyzer','lb-spider','sogou','lssbot','careerbot','wotbox','wocbot','ichiro','duckduckbot','lssrocketcrawler','drupact','webcompanycrawler','acoonbot','openindexspider','gnam gnam spider','web-archive-net.com.bot','backlinkcrawler','coccoc','integromedb','content crawler spider','toplistbot','seokicks-robot','it2media-domain-crawler','ip-web-crawler.com','siteexplorer.info','elisabot','proximic','changedetection','blexbot','arabot','wesee:search','niki-bot','crystalsemanticsbot','rogerbot','360spider','psbot','interfaxscanbot','lipperhey seo service','cc metadata scaper','g00g1e.net','grapeshotcrawler','urlappendbot','brainobot','fr-crawler','binlar','simplecrawler','livelapbot','twitterbot','cxensebot','smtbot','bnf.fr_bot','a6-indexer','admantx','facebot','twitterbot','orangebot','memorybot','advbot','megaindex','semanticscholarbot','ltx71','nerdybot','xovibot','bubing','qwantify','archive.org_bot','applebot','tweetmemebot','crawler4j','findxbot','semrushbot','yoozbot','lipperhey','y!j-asr','domain re-animator bot','addthis','screaming frog seo spider','metauri','scrapy','livelapbot','openhosebot','capsulechecker','collection@infegy.com','istellabot','deusu','betabot','cliqzbot','mojeekbot','netestate ne crawler','safesearch microdata crawler','gluten free crawler','sonic','sysomos','trove','deadlinkchecker',);
$user_agent = strtolower($user_agent);
foreach ($crawlers_agents as $agent) {
if (strpos($user_agent, $agent) !== false){
return $agent;
}
}
return false;
}
function fn_ab__quick_order_by_phone_change_order_status($status_to, $status_from, $order_info, &$force_notification, $order_statuses, $place_order){
$email_mask = Registry::get('addons.ab__quick_order_by_phone.user_email');
if (!empty($email_mask) and strlen($email_mask)){
list(,$email_end) = explode('@', $email_mask);
if (isset($order_info['email']) and strpos($order_info['email'], "@".$email_end)!==false){
$force_notification['C'] = false;
}
}
}
function fn_ab__quick_order_by_phone_check_product ($product, $mode){
if (!empty($product)) {
$ab__qobp_settings = Registry::get('addons.ab__quick_order_by_phone');
$ab__qobp_amount_orders = (isset($_SESSION['ab__qobp']['amount_orders']))?$_SESSION['ab__qobp']['amount_orders']:0;
$show_form = 'Y';
if ($show_form == 'Y'){
if ($mode == 'view' and $ab__qobp_settings['show_on_product_page'] == 'N'){
$show_form = 'N';
}
if ($mode == 'quick_view' and $ab__qobp_settings['show_on_quick_product_page'] == 'N'){
$show_form = 'N';
}
}
if ($show_form == 'Y'){
if (fn_ab__qobp_is_bot($_SERVER['HTTP_USER_AGENT']) !== false){
$show_form = 'N';
}
}
if ($show_form == 'Y'){
if ($ab__qobp_settings['amount_of_quick_orderds'] != 0 and $ab__qobp_amount_orders >= $ab__qobp_settings['amount_of_quick_orderds']){
$show_form = 'N';
}
}
if ($show_form == 'Y'){
$amount = 0;
if (!$amount and !empty($_REQUEST['ab__qobp']['product_amount']) and $_REQUEST['ab__qobp']['product_amount'] > 0){
$amount = $_REQUEST['ab__qobp']['product_amount'];
}
if (!$amount and $product['amount']){
$amount = $product['amount'];
}
if ($ab__qobp_settings['only_in_stock'] == 'Y' and $amount < 1 ) {
$show_form = 'N';
}
}
if ($show_form == 'Y'){
if (floatval($ab__qobp_settings['price_from']) > 0 and floatval($ab__qobp_settings['price_to']) > 0){
if ($product['price'] < floatval($ab__qobp_settings['price_from']) or $product['price'] > floatval($ab__qobp_settings['price_to'])){
$show_form = 'N';
}
}elseif (floatval($ab__qobp_settings['price_from']) > 0){
if ($product['price'] < floatval($ab__qobp_settings['price_from'])){
$show_form = 'N';
}
}elseif (floatval($ab__qobp_settings['price_to']) > 0){
if ($product['price'] > floatval($ab__qobp_settings['price_to'])){
$show_form = 'N';
}
}
}
Registry::get('view')->assign('ab__qobp_mode', $mode);
Registry::get('view')->assign('ab__qobp_show_form', $show_form);
Registry::get('view')->assign('ab__qobp_show_form_in_block', 'Y');
}
}
function fn_ab__quick_order_by_phone_after_options_calculation ($mode, $data) {
if (isset($data['product_data']) and (count($data['product_data']) == 1)){
$product_id = 0;
foreach ($data['product_data'] as $product_id=>$p){}
if (intval($product_id)){
$auth = "";
$product = fn_get_product_data($product_id, $_SESSION['auth'], CART_LANGUAGE, '?:products.product_id,?:products.amount', false, false, false, false);
fn_ab__quick_order_by_phone_check_product ($product, $mode);
}
}
}