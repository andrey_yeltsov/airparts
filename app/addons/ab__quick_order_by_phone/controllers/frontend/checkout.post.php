<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2019   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and  accept   *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
use Tygh\Registry;
if (!defined('BOOTSTRAP')) { die('Access denied'); }
if ($mode == 'cart' or $mode == 'checkout') {
$ab__qobp_settings = Registry::get('addons.ab__quick_order_by_phone');
$ab__qobp_amount_orders = (isset($_SESSION['ab__qobp']['amount_orders']))?$_SESSION['ab__qobp']['amount_orders']:0;
$show_form = 'Y';
if ($show_form == 'Y'){
if ($mode == 'cart' and $ab__qobp_settings['show_on_cart_page'] == 'N'){
$show_form = 'N';
}
if ($mode == 'checkout' and $ab__qobp_settings['show_on_checkout_page'] == 'N'){
$show_form = 'N';
}
}
if ($show_form == 'Y' ){
if (fn_ab__qobp_is_bot($_SERVER['HTTP_USER_AGENT']) !== false){
$show_form = 'N';
}
}
if ($show_form == 'Y' ){
if ($ab__qobp_settings['amount_of_quick_orderds'] != 0 and $ab__qobp_amount_orders >= $ab__qobp_settings['amount_of_quick_orderds']){
$show_form = 'N';
}
}
Registry::get('view')->assign('ab__qobp_show_form', $show_form);
if (!empty($_REQUEST['ab__qobp']) and $_REQUEST['ab__qobp'] == 'complete' and !empty($_SESSION['ab__qobp']['last_order'])){
Registry::get('view')->assign('ab__qobp_order', fn_get_order_info($_SESSION['ab__qobp']['last_order']));
$_SESSION['ab__qobp']['last_order'] = 0;
}
}
