<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2019   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and  accept   *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
if (!defined('BOOTSTRAP')) {die('Access denied');}
use Tygh\Registry;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
$min_order_amount = Registry::ifGet('settings.Checkout.min_order_amount', 0);
if ($mode == 'add'){
if (fn_ab__qobp_is_bot($_SERVER['HTTP_USER_AGENT']) !== false) exit;
if (!isset($_SESSION['ab__qobp']['amount_orders'])){$_SESSION['ab__qobp']['amount_orders'] = 0;}
$ab__qobp_settings = Registry::get('addons.ab__quick_order_by_phone');
if ($ab__qobp_settings['amount_of_quick_orderds'] == 0 or $ab__qobp_settings['amount_of_quick_orderds'] > $_SESSION['ab__qobp']['amount_orders']){
$auth = & $_SESSION['auth'];
$cart = array();
$product = array();
Registry::get('ajax')->assign('ab__qobp_result', 'N');
if (isset($_REQUEST['product_data']) and !empty($_REQUEST['product_data'])){
$p_id = 0;
foreach ($_REQUEST['product_data'] as $k=>$v){$p_id = $k; break;}
if (!isset($_REQUEST['ab__qobp_data']['phone']) or !fn_ab__qobp_check_phone(trim($_REQUEST['ab__qobp_data']['phone']))){
fn_set_notification('E', __('error'), __('ab__qobp.error_phone'));
exit;
}
$_REQUEST['product_data'][$p_id]['amount'] = isset($_REQUEST['product_data'][$p_id]['amount'])?abs(intval($_REQUEST['product_data'][$p_id]['amount'])):1;
$_REQUEST['product_data'][$p_id]['product_id'] = intval($_REQUEST['product_data'][$p_id]['product_id']);
fn_add_product_to_cart($_REQUEST['product_data'], $cart, $auth);
fn_save_cart_content($cart, $auth['user_id']);
$cart_copy = $cart['products'];
$product_copy = array_shift($cart_copy);
$total = floatval($product_copy['amount'] * $product_copy['price']);
if ($total >= $min_order_amount){
fn_ab__qobp_create_order($cart, $auth, $ab__qobp_settings);
}else{
Registry::get('view')->assign('current_order_amount', $total);
Registry::get('view')->assign('min_order_amount', $min_order_amount);
fn_set_notification('E', __('error'), Registry::get('view')->fetch('string:' . __('ab__qobp.min_order_amount_required')));
}
}
}else{
fn_set_notification('E', __('error'), __('ab__qobp.limit_quick_orders_in_session_was_reached'));
}
exit;
}
if ($mode == 'place_order'){
$ab__qobp_settings = Registry::get('addons.ab__quick_order_by_phone');
if (fn_ab__qobp_is_bot($_SERVER['HTTP_USER_AGENT']) !== false) exit;
if (!isset($_SESSION['ab__qobp']['amount_orders'])){$_SESSION['ab__qobp']['amount_orders'] = 0;}
if ($ab__qobp_settings['amount_of_quick_orderds'] == 0 or $ab__qobp_settings['amount_of_quick_orderds'] > $_SESSION['ab__qobp']['amount_orders']){
$cart = &$_SESSION['cart'];
$auth = &$_SESSION['auth'];
if ($cart['total'] && $cart['total'] > $min_order_amount){
if (!fn_cart_is_empty($cart)){
$order_id = fn_ab__qobp_create_order($cart, $auth, $ab__qobp_settings);
if ($order_id !== false){
fn_clear_cart($cart);
}
}
}else{
Registry::get('view')->assign('current_order_amount', $cart['total']);
Registry::get('view')->assign('min_order_amount', $min_order_amount);
fn_set_notification('E', __('error'), Registry::get('view')->fetch('string:' . __('ab__qobp.min_order_amount_required')));
}
}else{
fn_set_notification('E', __('error'), __('ab__qobp.limit_quick_orders_in_session_was_reached'));
}
return array(CONTROLLER_STATUS_REDIRECT, 'checkout.cart?ab__qobp=complete');
}
}
function fn_ab__qobp_create_order (&$cart, &$auth, $ab__qobp_settings){
$res = false;
$cart['shipping_required'] = true;
$cart['change_cart_products'] = true;
$cart['calculate_shipping'] = true;
$cart['chosen_shipping'][0] = $ab__qobp_settings['shipping_method'];
fn_calculate_cart_content($cart, $auth, 'S', false, 'F', true);
$cart['amount_failed'] = false;
$cart['shipping_failed'] = false;
$cart['company_shipping_failed'] = false;
unset($_SESSION['cart']['payment_id'], $_SESSION['cart']['payment_method_data']);
if (intval($auth['user_id'])){
$user_info = db_get_row("SELECT email, firstname, phone FROM ?:users WHERE user_id = ?i", $auth['user_id']);
if (is_array($user_info) and !empty($user_info)){
$user_data = array(
'user_id' => $auth['user_id'],
'email' => $user_info['email'],
'firstname' => $user_info['firstname'],
'b_firstname' => $user_info['firstname'],
's_firstname' => $user_info['firstname'],
'phone' => trim($_REQUEST['ab__qobp_data']['phone']),
'b_phone' => trim($_REQUEST['ab__qobp_data']['phone']),
's_phone' => trim($_REQUEST['ab__qobp_data']['phone']),
);
}
}else{
$user_data = array(
'email' => fn_ab__qobp_format_email(trim($ab__qobp_settings['user_email'])),
'firstname' => trim($ab__qobp_settings['user_name']),
'b_firstname' => trim($ab__qobp_settings['user_name']),
's_firstname' => trim($ab__qobp_settings['user_name']),
'phone' => trim($_REQUEST['ab__qobp_data']['phone']),
'b_phone' => trim($_REQUEST['ab__qobp_data']['phone']),
's_phone' => trim($_REQUEST['ab__qobp_data']['phone']),
);
}
if (isset($cart['user_data'])){
$cart['user_data'] = array_merge($cart['user_data'], $user_data);
}else{
$cart = array_merge($cart, $user_data);
}
list($order_id) = fn_place_order($cart, $auth, 'save');
if (intval($order_id)){
if (in_array($ab__qobp_settings['order_status'], range('A', 'Z'))){
$current_status = db_get_field("SELECT status FROM ?:orders WHERE order_id = ?s", $order_id);
if ($ab__qobp_settings['order_status'] != $current_status){
db_query("UPDATE ?:orders SET status = ?s WHERE order_id = ?i", $ab__qobp_settings['order_status'], $order_id);
}
}
fn_set_notification('N', __('notice'), __('ab__qobp.order_was_successfully_placed'), 'S');
$_SESSION['ab__qobp']['amount_orders'] += 1;
$_SESSION['ab__qobp']['last_order'] = $order_id;
if (defined('AJAX_REQUEST')) {
Registry::get('ajax')->assign('ab__qobp_result', 'Y');
Registry::get('ajax')->assign('ab__qobp_order', fn_get_order_info($order_id));
}
$res = $order_id;
}
return $res;
}
