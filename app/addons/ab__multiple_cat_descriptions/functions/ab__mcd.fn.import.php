<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2019   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and  accept   *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
if (!defined('BOOTSTRAP')) { die('Access denied'); }
use Tygh\Registry;
function fn_ab__multiple_cat_descriptions_install_demodata() {
$answer = array();
foreach( array('fn_ab__mcd_install_cat_descr') as $func ) {
if ( function_exists( $func )) {
$val = $func('A');
if( !$val ) return false;
$answer[$func] = $val;
}
}
return $answer;
}
function fn_ab__mcd_install_cat_descr( $status ) {
$path_part = 'ab__data/ab__multiple_cat_descriptions/demodata/descriptions';
$path = Registry::get('config.dir.var') . $path_part;
$data = fn_get_contents("{$path}/data.json");
if ( empty($data) ) {
return false;
}
$company = fn_get_runtime_company_id();
$cat_id = db_get_field("SELECT category_id FROM ?:categories WHERE level=1 AND status='A' AND is_trash='N' AND company_id={$company}");
if ( !empty($cat_id) ) {
$data = json_decode($data, true);
$langs = array_keys(fn_get_languages());
$descriptions = array();
foreach ($data as $d) {
$d['status'] = $status;
$d['object_id'] = $cat_id;
$d['object_type'] = 'category';
$id = fn_ab__mcd_upd_desc($d);
if (in_array('ru', $langs)) {
fn_ab__mcd_upd_desc(array_merge($d, $d['ru']), $id, 'ru');
}
$descriptions[] = $id;
}
$descriptions = implode(', ', $descriptions);
$str = '<a target="_blank" href="' . fn_url('categories.update&category_id=' . $cat_id . '&selected_section=ab__mcd_tab') . '">category</a>';
fn_set_notification("N", __('notice'), "Descriptions {$descriptions} in {$str} was installed successfully!", 'S');
return $cat_id;
}
return false;
}