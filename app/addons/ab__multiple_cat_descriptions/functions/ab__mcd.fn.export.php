<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2019   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and  accept   *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
if (!defined('BOOTSTRAP')) { die('Access denied'); }
use Tygh\Registry;
function fn_ab__mcd_export_cat_descr( $categories ) {
$data = array();
$path_part = 'ab__data/ab__multiple_cat_descriptions/demodata/descriptions';
$path = Registry::get('config.dir.var') . $path_part;
fn_rm($path);
fn_mkdir($path);
foreach ($categories as $cat) {
$arr = fn_ab__mcd_get_descs(array(
'object_id' => $cat,
'object_type' => 'category'
), 'en');
$arr['ru'] = fn_ab__mcd_get_descs(array(
'object_id' => $cat,
'object_type' => 'category'
), 'ru');
$arr = fn_ab__mcd_unset_in_descr($arr);
$arr['ru'] = fn_ab__mcd_unset_in_descr($arr['ru']);
$data[] = $arr;
}
fn_put_contents("{$path}/data.json", json_encode($data, JSON_PRETTY_PRINT));
$categories = implode(', ', $categories);
fn_set_notification("N", __('notice'), "Categories {$categories} descriptions was exported successfully!");
}
function fn_ab__mcd_unset_in_descr( $descs ) {
$arr = array('object_id', 'status', 'object_type');
foreach($descs as &$descr) {
foreach($descr as &$d) {
foreach($arr as $uns) {
if ( !empty($d[$uns]) ) { unset($d[$uns]); }
}
}
}
return $descs;
}