<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2019   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and  accept   *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
if (!defined('BOOTSTRAP')) {die('Access denied');}
use Tygh\Registry;
$schema['central']['ab__addons']['items']['ab__multiple_cat_descriptions'] = array(
'attrs' => array('class'=>'is-addon'),
'href' => 'ab__mcd_descs.help',
'position' => 10,
'subitems' => array(
'ab__mcd_descs.settings' => array(
'href' => 'addons.update&addon=ab__multiple_cat_descriptions',
'position' => 0,
),
'ab__mcd_descs.demodata' => array(
'href' => 'ab__mcd_descs.demodata',
'position' => 200,
),
'ab__mcd_descs.help' => array(
'href' => 'ab__mcd_descs.help',
'position' => 10000,
),
/*'ab__mcd_descs.export' => array(
'attrs' => array(
'class'=>'is-addon'
),
'href' => 'ab__mcd_descs.export',
'position' => 300
)*/
),
);
return $schema;
