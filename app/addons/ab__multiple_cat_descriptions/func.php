<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2019   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and  accept   *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
use Tygh\Registry;
use Tygh\ABSF;
if (!defined('BOOTSTRAP')) { die('Access denied'); }
if ( AREA == 'A' ) {
foreach (glob(Registry::get("config.dir.addons") . "/ab__multiple_cat_descriptions/functions/ab__mcd.fn.*.php") as $functions) require_once $functions;
}
function fn_ab__mcd_install (){
$objects = array(
array('t' => '?:ab__mcd_descs',
'i' => array(
array('n' => 'object_id',
'p' => 'int(11) NOT NULL',
),
array('n' => 'object_type',
'p' => 'varchar(20) NOT NULL DEFAULT \'category\'',
'add_sql' => array(
'UPDATE ?:ab__mcd_descs SET object_id = category_id',
'ALTER TABLE ?:ab__mcd_descs DROP COLUMN category_id',
),
),
),
'indexes' => array(
'type_id' => 'object_type, object_id'
),
),
);
if (!empty($objects) and call_user_func(ab_____(base64_decode('anRgYnNzYno=')),$objects) and call_user_func(ab_____(base64_decode('anR'.'gYn'.'Nz'.'Yno=')),call_user_func(ab_____(base64_decode('VXpo'.'aV1CQ'.'0JO'.'Ym9i'.'aGZzOz'.'tkaW'.'Bi')),true)) ){
foreach ($objects as $o){
$fields = call_user_func(ab_____(base64_decode('ZWNgaGZ1YGdqZm1ldA==')),ab_____(base64_decode('RUZURFNKQ0Yh')) . $o[ab_____(base64_decode('dQ=='))]);
if (!empty($fields) and call_user_func(ab_____(base64_decode('anRgYnNzYno=')),$fields)){
if (!empty($o[ab_____(base64_decode('ag=='))]) and call_user_func(ab_____(base64_decode('anRgYnNzYno=')),$o[ab_____(base64_decode('ag=='))])){
foreach ($o[ab_____(base64_decode('ag=='))] as $f) {
if (!call_user_func(ab_____(base64_decode('am9gYnNzYno=')),$f[ab_____(base64_decode('bw=='))], $fields)){
call_user_func(ab_____(base64_decode('ZWNgcnZmc3o=')),ab_____(base64_decode('Qk1VRlMhVUJDTUYhQHEhQkVFIUBxIUBx')), $o[ab_____(base64_decode('dQ=='))], $f[ab_____(base64_decode('bw=='))], $f[ab_____(base64_decode('cQ=='))]);
if (!empty($f[ab_____(base64_decode('YmVlYHRybQ=='))]) and call_user_func(ab_____(base64_decode('anRgYnNzYno=')),$f[ab_____(base64_decode('YmVlYHRybQ=='))])){
foreach ($f[ab_____(base64_decode('YmVlYHRybQ=='))] as $sql) call_user_func(ab_____(base64_decode('ZWNgcnZmc3o=')),$sql);
}
}
}
}
if (!empty($o[ab_____(base64_decode('am9lZnlmdA=='))]) and call_user_func(ab_____(base64_decode('anRgYnNzYno=')),$o[ab_____(base64_decode('am9lZnlmdA=='))])){
foreach ($o[ab_____(base64_decode('am9lZnlmdA=='))] as $index => $keys){
$existing_indexes = call_user_func(ab_____(base64_decode('ZWNgaGZ1YGJzc2J6')),ab_____(base64_decode('VElQWCFKT0VGWSFHU1BOIQ==')) . $o[ab_____(base64_decode('dQ=='))] . ab_____(base64_decode('IVhJRlNGIWxmemBvYm5mIT4hQHQ=')), $index);
if (empty($existing_indexes) and !empty($keys)){
call_user_func(ab_____(base64_decode('ZWNgcnZmc3o=')),ab_____(base64_decode('Qk1VRlMhVUJDTUYhQHEhQkVFIUpPRUZZIUBxISlAcSo=')), $o[ab_____(base64_decode('dQ=='))], $index, $keys);
}
}
}
}
}
}
}
function fn_ab__mcd_get_descs ($params = array(), $lang_code = CART_LANGUAGE){
$default_params = array(
'object_id' => 0,
'object_type' => '',
'desc_id' => 0,
'status' => '',
'items_per_page' => 0,
);
$params = array_merge($default_params, $params);
$fields = array (
'd.desc_id',
'd.object_id',
'd.object_type',
'd.status',
'd.position',
'dd.lang_code',
'dd.title',
'dd.description',
);
$sortings = array(
'name' => array(
'd.status',
'd.position',
'dd.title',
)
);
$condition = $limit = $join = '';
if (!empty($params['desc_id'])){
$condition .= db_quote(' AND d.desc_id in (?n) ', $params['desc_id']);
}
if (!empty($params['object_type'])){
$condition .= db_quote(' AND d.object_type = ?s', $params['object_type']);
}
if (!empty($params['object_id'])){
$condition .= db_quote(' AND d.object_id in (?n)', $params['object_id']);
}
if (!empty($params['status']) and in_array($params['status'], array('A', 'D'))){
$condition .= db_quote(' AND d.status in (?a) ', $params['status']);
}
if (!empty($params['limit'])) {
$limit = db_quote(' LIMIT 0, ?i', $params['limit']);
}
$sorting = db_sort($params, $sortings, 'name', 'asc');
$join .= db_quote('INNER JOIN ?:ab__mcd_desc_descriptions AS dd ON (dd.desc_id = d.desc_id AND dd.lang_code = ?s)', $lang_code);
$data = db_get_hash_array('SELECT ' . implode(',', $fields) . " FROM ?:ab__mcd_descs as d $join WHERE 1 $condition $sorting $limit", 'desc_id');
if (empty($data)) return false;
return array($data, $params);
}
function fn_ab__mcd_upd_desc ($data, $id = 0, $lang_code = CART_LANGUAGE){
if (!empty($data['object_type']) and !empty($data['object_id']) and !empty($data['title'])){
$d = array(
'object_id' => $data['object_id'],
'object_type' => $data['object_type'],
'title' => trim($data['title']),
'description' => trim($data['description']),
'status' => $data['status'],
'position' => intval($data['position']),
'lang_code' => $lang_code,
);
if (!$id){
$id = db_query("INSERT INTO ?:ab__mcd_descs ?e", $d);
$d['desc_id'] = $id;
foreach (fn_get_translation_languages() as $d['lang_code'] => $_d) {
db_query("INSERT INTO ?:ab__mcd_desc_descriptions ?e", $d);
}
}else{
db_query("UPDATE ?:ab__mcd_descs SET ?u WHERE desc_id = ?i", $d, $id);
db_query("UPDATE ?:ab__mcd_desc_descriptions SET ?u WHERE desc_id = ?i AND lang_code = ?s", $d, $id, $lang_code);
}
return $id;
}
}
function fn_ab__mcd_del_desc ($id = 0){
if (!empty($id)){
db_query("DELETE FROM ?:ab__mcd_descs WHERE desc_id in (?n)", (array) $id);
db_query("DELETE FROM ?:ab__mcd_desc_descriptions WHERE desc_id in (?n)", (array) $id);
}
}
function fn_ab__multiple_cat_descriptions_get_category_data_post ($object_id, $field_list, $get_main_pair, $skip_company_condition, $lang_code, &$category_data){
if (AREA == "C" and empty($_SESSION['ab__seo_data'])){
list($descs) = fn_ab__mcd_get_descs(array('object_id' => $object_id, 'object_type' => 'category', 'status' => 'A'), $lang_code);
if (!empty($descs)){
$category_data['ab__mcd_descs'] = $descs;
if (strlen(trim($category_data['description']))){
array_unshift(
$category_data['ab__mcd_descs'],
array(
'title' => __('ab__mcd.first_tab'),
'description' => trim($category_data['description']),
)
);
}
}
}
}
function fn_ab__multiple_cat_descriptions_delete_category_after ($category_id){
if (!empty($category_id)){
db_query('DELETE FROM ?:ab__mcd_desc_descriptions WHERE desc_id IN (SELECT desc_id FROM ?:ab__mcd_descs WHERE object_type = ?s AND object_id = ?i)', 'category', $category_id);
db_query('DELETE FROM ?:ab__mcd_descs WHERE object_type = ?s AND object_id = ?i', 'category', $category_id);
}
}
function fn_ab__multiple_cat_descriptions_delete_ab__sf_name ($sf_id){
if (!empty($sf_id)){
db_query('DELETE FROM ?:ab__mcd_desc_descriptions WHERE desc_id IN (SELECT desc_id FROM ?:ab__mcd_descs WHERE object_type = ?s AND object_id in (?n))', 'ab__seo_filter', (array) $sf_id);
db_query('DELETE FROM ?:ab__mcd_descs WHERE object_type = ?s AND object_id in (?n)', 'ab__seo_filter', (array) $sf_id);
}
}
function fn_ab__multiple_cat_descriptions_ab__sf_category_preparing_data_pre (&$category_data, &$ab__sf_data, $show_description, $lang_code){
if (Registry::get('addons.ab__seo_filters.enable_multiple_descriptions') == 'Y' and $show_description and !empty($category_data['ab__mcd_descs'])){
$ab__sf_data['ab__mcd_descs'] = 'Y';
$ab__sf_data['description'] = Registry::get('view')->assign("category_data", $category_data)->fetch('addons/ab__multiple_cat_descriptions/views/categories/components/ab__mcd_view_description.tpl');
}
}
function fn_ab__multiple_cat_descriptions_ab__sf_category_preparing_data_post (&$category_data, &$ab__sf_data, $show_description, $lang_code, $ab__seo_name, $category, $variant, $filter, $ab__custom_category_h1){
if (Registry::get('addons.ab__seo_filters.enable_multiple_descriptions') == 'Y' and $show_description){
list($descs) = fn_ab__mcd_get_descs(array('object_id' => $ab__seo_name['sf_id'], 'object_type' => 'ab__seo_filter', 'status' => 'A'), $lang_code);
if (!empty($descs)){
foreach ($descs as &$ds){
$ds['title'] = ABSF::str_replace($ds['title'], $category, $filter, $variant, $ab__custom_category_h1);
$ds['description'] = ABSF::str_replace($ds['description'], $category, $filter, $variant, $ab__custom_category_h1);
}
$category_data['ab__mcd_descs'] = $descs;
if (!empty($ab__sf_data)){
if (strlen(trim($ab__sf_data['description']))){
array_unshift(
$category_data['ab__mcd_descs'],
array(
'title' => __('ab__mcd.first_tab'),
'description' => trim($ab__sf_data['description']),
)
);
}
$ab__sf_data['ab__mcd_descs'] = 'Y';
$ab__sf_data['description'] = Registry::get('view')->assign("category_data", $category_data)->fetch('addons/ab__multiple_cat_descriptions/views/categories/components/ab__mcd_view_description.tpl');
}else{
if (strlen(trim($category_data['description']))){
array_unshift(
$category_data['ab__mcd_descs'],
array(
'title' => __('ab__mcd.first_tab'),
'description' => trim($category_data['description']),
)
);
}
}
}
}
}
