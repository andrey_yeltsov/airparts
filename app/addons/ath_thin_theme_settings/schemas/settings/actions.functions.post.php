<?php
/***************************************************************************
*                                                                          *
*   (c) 2017 ThemeHills - Premium themes and addons					       *
*                                                                          *
****************************************************************************/

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_settings_actions_addons_ath_thin_theme_settings() {
	
	fn_check_license_ath_thin_theme_settings('exit');
	
	return true;
}

