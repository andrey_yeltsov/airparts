<?php
/***************************************************************************
*                                                                          *
*   (c) 2016 ThemeHills - Themes and addons     					       *
*                                                                          *
****************************************************************************/
	
$schema['blocks/products/products_grid_simple.tpl'] = array (
    'settings' => array(
        'number_of_columns' =>  array (
            'type' => 'input',
            'default_value' => 4
        ),
    ),
    'bulk_modifier' => array (
        'fn_gather_additional_products_data' => array (
            'products' => '#this',
            'params' => array (
                'get_icon' => true,
                'get_detailed' => true,
                'get_options' => true,
            ),
        ),
    ),
);

if (fn_allowed_for('MULTIVENDOR') || fn_allowed_for('MULTIVENDOR:ULTIMATE')) {
	$schema['blocks/vendor_list_templates/vendor_logo_and_name.tpl'] = array (
	    'settings' => array(
	        'number_of_columns' => array (
	            'type' => 'input',
	            'default_value' => 5
	        ),
	        'show_name' => array (
	            'type' => 'checkbox',
	            'default_value' => 'Y'
	        ),
	        'show_location' => array (
	            'type' => 'checkbox',
	            'default_value' => 'N'
	        ),
	        'show_products_count' => array (
	            'type' => 'checkbox',
	            'default_value' => 'N'
	        ),
	        'show_icon_in_title' => array (
	            'type' => 'checkbox',
	            'default_value' => 'Y'
	        ),
	        'show_all_vendors_btn' => array (
	            'type' => 'checkbox',
	            'default_value' => 'Y'
	        ),
            'all_vendors_btn_type' => array(
                'type' => 'selectbox',
                'values' => array(
                    'ty-btn__primary' => 'theme_editor.primary_button',
                    'ty-btn__secondary' => 'theme_editor.secondary_button',
                    'ty-btn__tertiary' => 'theme_editor.tertiary_button',
                ),
                'default_value' => 'ty-btn__tertiary'
            ),
	    ),
	    'fillings' => array ('all', 'manually'),
	    'params' => array (
	        'status' => 'A',
	    ),
	);
}

$schema['blocks/our_brands.tpl']['settings']['limit'] = array(
	'type' => 'input',
	'default_value' => 14
);

return $schema;