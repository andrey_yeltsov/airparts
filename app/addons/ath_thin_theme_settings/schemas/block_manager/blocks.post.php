<?php
/***************************************************************************
*                                                                          *
*   (c) 2016 ThemeHills - Themes and addons     					       *
*                                                                          *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

$schema['banners']['templates']['blocks/banners/original_promo.tpl']['settings'] = array (
    'top_line' => array (
        'type' => 'input',
        'default_value' => ''
    ),
    'title' => array (
        'type' => 'input',
        'default_value' => ''
    ),
    'btn_text' => array (
        'type' => 'input',
        'default_value' => 'btn_detail'
    ),
);

$schema['banners']['templates']['blocks/banners/owl2_carousel.tpl']['settings'] = array (
	'pause_time' => array (
        'type' => 'input',
        'default_value' => '5'
    ),
    'arrows' => array(
        'type' => 'checkbox',
        'default_value' => 'Y'
    ),
    'dot' => array(
        'type' => 'checkbox',
        'default_value' => 'N'
    ),
    'animate_in' => array(
        'type' => 'selectbox',
        'values' => fn_tt_get_json_list_settings('animate-in-config.json'),
        'default_value' => 'fadeIn'
    ),
    'animate_out' => array(
        'type' => 'selectbox',
        'values' => fn_tt_get_json_list_settings('animate-out-config.json'),
        'default_value' => 'fadeOut'
    ),
);

if(Registry::get('addons.ath_mega_menu.status') != 'A' && Registry::get('addons.ath_emerald_theme_settings.status') != 'A') {

	$schema['mega_menu'] = array(
	    'templates' => array(
	        'blocks/mega_menu/mega_menu.tpl' => array(
				'settings' => array(
				    'language' => array(
				        'type' => 'checkbox',
				        'default_value' => 'Y'
				    ),
				    'currency' => array(
				        'type' => 'checkbox',
				        'default_value' => 'Y'
				    ),
				    
				   'full_tree_l1' => array (
				        'type' => 'input',
				        'default_value' => '8'
				    ),
				    'full_tree_l2' => array (
				        'type' => 'input',
				        'default_value' => '6'
				    ),
					'full_tree_l3' => array (
				        'type' => 'input',
				        'default_value' => '6'
				    ),
				    
				),
	        ),
	    ),
	    'content' => array(
	        'items' => array(
	            'type' => 'function',
	            'function' => array('fn_tt_get_mega_menu_items')
	        ),
	        'menu' => array(
	            'type' => 'template',
	            'template' => 'views/menus/components/block_settings.tpl',
	            'hide_label' => true,
	            'data_function' => array('fn_get_menus'),
	        ),
	    ),
	    'wrappers' => 'blocks/wrappers',
	    'cache' => true
	);

}


$schema['categories_catalog'] = array(
	'content' => array(
	    'items' => array(
	        'type' => 'enum',
	        'object' => 'categories',
	        'items_function' => 'fn_get_categories',
	        'remove_indent' => true,
	        'hide_label' => true,
	        'fillings' => array(

	            'parent_category' => array(
	                'params' => array(
	                    'plain' => true,
	                    'get_images' => true,
	                    'max_nesting_level' => 1,
	                ),
	                'update_params' => array(
	                    'request' => array('%CATEGORY_ID'),
	                ),
	                'settings' => array(
	                    'parent_category_id' => array(
	                        'type' => 'picker',
	                        'default_value' => '0',
	                        'picker' => 'pickers/categories/picker.tpl',
	                        'picker_params' => array(
	                            'multiple' => false,
	                            'use_keys' => 'N',
	                            'default_name' => __('root_level'),
	                        ),
	                    ),
	                    'sort_by' => array(
	                        'type' => 'selectbox',
	                        'values' => array(
	                            'position' => 'position',
	                            'name' => 'name',
	                        ),
	                        'default_value' => 'position'
	                    ),
	                ),
	            ),
	            
	            'manually' => array(
	                'params' => array(
	                    'plain' => true,
	                    'get_images' => true,
	                    'simple' => false,
	                    'group_by_level' => false,
	                ),
	                'picker' => 'pickers/categories/picker.tpl',
	                'picker_params' => array(
	                    'multiple' => true,
	                    'use_keys' => 'N',
	                    'status' => 'A',
	                    'positions' => true,
	                ),
	            ),
	            
                'subcategories_tree_cat' => array(
                    'params' => array(
	                    'get_images' => true,
                        'request' => array(
                            'category_id' => '%CATEGORY_ID%'
                        ),
                    ),
                    'settings' => array(
                        'sort_by' => array(
                            'type' => 'selectbox',
                            'values' => array(
                                'position' => 'position',
                                'name' => 'name',
                            ),
                            'default_value' => 'position'
                        ),
                    ),
                ),
	        ),
	    )
	),
	'templates' => array(
	    'blocks/categories/catalog_grid.tpl' => array(
		    'settings' => array(
		    	'columns' => array (
			        'type' => 'input',
			        'default_value' => '4'
			    ),
		    	'image_width' => array (
			        'type' => 'input',
			        'default_value' => '350'
			    ),
		    	'image_height' => array (
			        'type' => 'input',
			        'default_value' => '400'
			    ),
		    ),
	    ),
	    'blocks/categories/catalog_carousel.tpl' => array(
		    'settings' => array(
		    	'image_width' => array (
			        'type' => 'input',
			        'default_value' => '350'
			    ),
		    	'image_height' => array (
			        'type' => 'input',
			        'default_value' => '400'
			    ),
	            'not_scroll_automatically' => array (
	                'type' => 'checkbox',
	                'default_value' => 'N'
	            ),
	            'scroll_per_page' =>  array (
	                'type' => 'checkbox',
	                'default_value' => 'N'
	            ),
	            'speed' =>  array (
	                'type' => 'input',
	                'default_value' => 400
	            ),
	            'pause_delay' =>  array (
	                'type' => 'input',
	                'default_value' => 3
	            ),
	            'item_quantity' =>  array (
	                'type' => 'input',
	                'default_value' => 5
	            ),
	            'outside_navigation' => array (
	                'type' => 'checkbox',
	                'default_value' => 'Y'
	            ),
		    ), 
	    ),
	),
	'wrappers' => 'blocks/wrappers',
	'cache' => array(
	    'update_handlers' => array('categories', 'category_descriptions'),
	    'session_handlers' => array('%CURRENT_CATEGORY_ID%'),
	    'request_handlers' => array('%CATEGORY_ID%')
	),
);

	$schema['payment_svg_icon'] = array(
	    'templates' => array(
	        'blocks/payment_svg_icon.tpl' => array()
	    ),
		'settings' => array(
		    'checkout2' => array(
		        'type' => 'checkbox',
		        'default_value' => 'Y'
		    ),
		    'amazon' => array(
		        'type' => 'checkbox',
		        'default_value' => 'Y'
		    ),
		    'amex' => array(
		        'type' => 'checkbox',
		        'default_value' => 'Y'
		    ),
		    'discover' => array(
		        'type' => 'checkbox',
		        'default_value' => 'Y'
		    ),
		    'maestro' => array(
		        'type' => 'checkbox',
		        'default_value' => 'Y'
		    ),
		    'master_card' => array(
		        'type' => 'checkbox',
		        'default_value' => 'Y'
		    ),
		    'paypal' => array(
		        'type' => 'checkbox',
		        'default_value' => 'Y'
		    ),
		    'visa' => array(
		        'type' => 'checkbox',
		        'default_value' => 'Y'
		    ),
		    'webmoney' => array(
		        'type' => 'checkbox',
		        'default_value' => 'Y'
		    ),
		    'yandex_money' => array(
		        'type' => 'checkbox',
		        'default_value' => 'Y'
		    ),
		    'apple_pay' => array(
		        'type' => 'checkbox',
		        'default_value' => 'Y'
		    ),
		    'google_pay' => array(
		        'type' => 'checkbox',
		        'default_value' => 'Y'
		    ),
		    'carte_bleue' => array(
		        'type' => 'checkbox',
		        'default_value' => 'Y'
		    )
		),
	    'wrappers' => 'blocks/wrappers',
	    'cache' => true
	);

return $schema;