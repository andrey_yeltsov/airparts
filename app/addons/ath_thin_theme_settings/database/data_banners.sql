REPLACE INTO ?:banner_descriptions (`banner_id`, `banner`, `url`, `description`, `lang_code`) VALUES(9370, 'Home Big Gift #1', 'gift_certificates.add', '', 'en');
REPLACE INTO ?:banner_images (`banner_image_id`, `banner_id`, `lang_code`) VALUES(71310, 9370, 'en');

REPLACE INTO ?:banner_descriptions (`banner_id`, `banner`, `url`, `description`, `lang_code`) VALUES(9371, 'Home Big Gift #2', 'products.on_sale', '', 'en');
REPLACE INTO ?:banner_images (`banner_image_id`, `banner_id`, `lang_code`) VALUES(71311, 9371, 'en');

REPLACE INTO ?:banner_descriptions (`banner_id`, `banner`, `url`, `description`, `lang_code`) VALUES(9372, 'Home Products', 'product_features.view&variant_id=91', '', 'en');
REPLACE INTO ?:banner_images (`banner_image_id`, `banner_id`, `lang_code`) VALUES(71312, 9372, 'en');

REPLACE INTO ?:banner_descriptions (`banner_id`, `banner`, `url`, `description`, `lang_code`) VALUES(9373, 'Home Product', 'products.view&product_id=155', '', 'en');
REPLACE INTO ?:banner_images (`banner_image_id`, `banner_id`, `lang_code`) VALUES(71313, 9373, 'en');

REPLACE INTO ?:banner_descriptions (`banner_id`, `banner`, `url`, `description`, `lang_code`) VALUES(9374, 'Home Large #1', 'products.on_sale', '', 'en');
REPLACE INTO ?:banner_images (`banner_image_id`, `banner_id`, `lang_code`) VALUES(71314, 9374, 'en');

REPLACE INTO ?:banner_descriptions (`banner_id`, `banner`, `url`, `description`, `lang_code`) VALUES(9375, 'Home Large #2', 'gift_certificates.add', '', 'en');
REPLACE INTO ?:banner_images (`banner_image_id`, `banner_id`, `lang_code`) VALUES(71315, 9375, 'en');

REPLACE INTO ?:banner_descriptions (`banner_id`, `banner`, `url`, `description`, `lang_code`) VALUES(9376, 'Home New Man', 'categories.view&category_id=224&sort_by=timestamp&sort_order=desc', '', 'en');
REPLACE INTO ?:banner_images (`banner_image_id`, `banner_id`, `lang_code`) VALUES(71316, 9376, 'en');

REPLACE INTO ?:banner_descriptions (`banner_id`, `banner`, `url`, `description`, `lang_code`) VALUES(9377, 'Home New Woman', 'categories.view&category_id=225&sort_by=timestamp&sort_order=desc', '', 'en');
REPLACE INTO ?:banner_images (`banner_image_id`, `banner_id`, `lang_code`) VALUES(71317, 9377, 'en');

REPLACE INTO ?:banner_descriptions (`banner_id`, `banner`, `url`, `description`, `lang_code`) VALUES(9378, 'Home New Trend', 'products.bestsellers', '', 'en');
REPLACE INTO ?:banner_images (`banner_image_id`, `banner_id`, `lang_code`) VALUES(71318, 9378, 'en');

REPLACE INTO ?:banner_descriptions (`banner_id`, `banner`, `url`, `description`, `lang_code`) VALUES(9379, 'On Sale Large', '', '', 'en');
REPLACE INTO ?:banner_images (`banner_image_id`, `banner_id`, `lang_code`) VALUES(71319, 9379, 'en');
