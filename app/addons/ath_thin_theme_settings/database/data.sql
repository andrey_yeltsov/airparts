REPLACE INTO ?:banners (`banner_id`, `status`, `type`, `target`, `localization`, `timestamp`) VALUES(9370, 'A', 'G', 'T', '', UNIX_TIMESTAMP(NOW()) - 3000);
REPLACE INTO ?:images (`image_id`, `image_path`, `image_x`, `image_y`) VALUES(9370, 'b_home_gift_1.png', 1500, 320);
REPLACE INTO ?:images_links (`object_id`, `object_type`, `image_id`, `detailed_id`, `type`, `position`) VALUES(71310, 'promo', 9370, 0, 'M', 0);

REPLACE INTO ?:banners (`banner_id`, `status`, `type`, `target`, `localization`, `timestamp`) VALUES(9371, 'A', 'G', 'T', '', UNIX_TIMESTAMP(NOW()) - 3000);
REPLACE INTO ?:images (`image_id`, `image_path`, `image_x`, `image_y`) VALUES(9371, 'b_home_gift_2.png', 1500, 320);
REPLACE INTO ?:images_links (`object_id`, `object_type`, `image_id`, `detailed_id`, `type`, `position`) VALUES(71311, 'promo', 9371, 0, 'M', 0);

REPLACE INTO ?:banners (`banner_id`, `status`, `type`, `target`, `localization`, `timestamp`) VALUES(9372, 'A', 'G', 'T', '', UNIX_TIMESTAMP(NOW()) - 3000);
REPLACE INTO ?:images (`image_id`, `image_path`, `image_x`, `image_y`) VALUES(9372, 'b_home_products.png', 1500, 320);
REPLACE INTO ?:images_links (`object_id`, `object_type`, `image_id`, `detailed_id`, `type`, `position`) VALUES(71312, 'promo', 9372, 0, 'M', 0);

REPLACE INTO ?:banners (`banner_id`, `status`, `type`, `target`, `localization`, `timestamp`) VALUES(9373, 'A', 'G', 'T', '', UNIX_TIMESTAMP(NOW()) - 3000);
REPLACE INTO ?:images (`image_id`, `image_path`, `image_x`, `image_y`) VALUES(9373, 'b_home_product.png', 1500, 320);
REPLACE INTO ?:images_links (`object_id`, `object_type`, `image_id`, `detailed_id`, `type`, `position`) VALUES(71313, 'promo', 9373, 0, 'M', 0);

REPLACE INTO ?:banners (`banner_id`, `status`, `type`, `target`, `localization`, `timestamp`) VALUES(9374, 'A', 'G', 'T', '', UNIX_TIMESTAMP(NOW()) - 3000);
REPLACE INTO ?:images (`image_id`, `image_path`, `image_x`, `image_y`) VALUES(9374, 'b_large.png', 1200, 140);
REPLACE INTO ?:images_links (`object_id`, `object_type`, `image_id`, `detailed_id`, `type`, `position`) VALUES(71314, 'promo', 9374, 0, 'M', 0);

REPLACE INTO ?:banners (`banner_id`, `status`, `type`, `target`, `localization`, `timestamp`) VALUES(9375, 'A', 'G', 'T', '', UNIX_TIMESTAMP(NOW()) - 3000);
REPLACE INTO ?:images (`image_id`, `image_path`, `image_x`, `image_y`) VALUES(9375, 'b_large_2.png', 1200, 92);
REPLACE INTO ?:images_links (`object_id`, `object_type`, `image_id`, `detailed_id`, `type`, `position`) VALUES(71315, 'promo', 9375, 0, 'M', 0);

REPLACE INTO ?:banners (`banner_id`, `status`, `type`, `target`, `localization`, `timestamp`) VALUES(9376, 'A', 'G', 'T', '', UNIX_TIMESTAMP(NOW()) - 3000);
REPLACE INTO ?:images (`image_id`, `image_path`, `image_x`, `image_y`) VALUES(9376, 'b_sm_man_v2.png', 480, 480);
REPLACE INTO ?:images_links (`object_id`, `object_type`, `image_id`, `detailed_id`, `type`, `position`) VALUES(71316, 'promo', 9376, 0, 'M', 0);

REPLACE INTO ?:banners (`banner_id`, `status`, `type`, `target`, `localization`, `timestamp`) VALUES(9377, 'A', 'G', 'T', '', UNIX_TIMESTAMP(NOW()) - 3000);
REPLACE INTO ?:images (`image_id`, `image_path`, `image_x`, `image_y`) VALUES(9377, 'b_sm_woman_v2.png', 480, 480);
REPLACE INTO ?:images_links (`object_id`, `object_type`, `image_id`, `detailed_id`, `type`, `position`) VALUES(71317, 'promo', 9377, 0, 'M', 0);

REPLACE INTO ?:banners (`banner_id`, `status`, `type`, `target`, `localization`, `timestamp`) VALUES(9378, 'A', 'G', 'T', '', UNIX_TIMESTAMP(NOW()) - 3000);
REPLACE INTO ?:images (`image_id`, `image_path`, `image_x`, `image_y`) VALUES(9378, 'b_sm_trend_v2.png', 480, 480);
REPLACE INTO ?:images_links (`object_id`, `object_type`, `image_id`, `detailed_id`, `type`, `position`) VALUES(71318, 'promo', 9378, 0, 'M', 0);

REPLACE INTO ?:banners (`banner_id`, `status`, `type`, `target`, `localization`, `timestamp`) VALUES(9379, 'A', 'G', 'T', '', UNIX_TIMESTAMP(NOW()) - 3000);
REPLACE INTO ?:images (`image_id`, `image_path`, `image_x`, `image_y`) VALUES(9379, 'b_large_sale.png', 1200, 202);
REPLACE INTO ?:images_links (`object_id`, `object_type`, `image_id`, `detailed_id`, `type`, `position`) VALUES(71319, 'promo', 9379, 0, 'M', 0);
