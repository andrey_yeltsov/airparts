<?php
/***************************************************************************
*                                                                          *
*   (c) 2016 ThemeHills - Themes and addons     					       *
*                                                                          *
****************************************************************************/

if ( !defined('AREA') ) { die('Access denied'); }

fn_register_hooks(
    'get_static_data',
    'update_category_post',
    'get_category_data_post',
    'get_categories'
);
