<?php
/***************************************************************************
*                                                                          *
*   (c) 2016 ThemeHills - Themes and addons     					       *
*                                                                          *
****************************************************************************/

use Tygh\Http;
use Tygh\Menu;
use Tygh\Registry;
use Tygh\Languages\Languages;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_get_tt_banners_id() {
	$tt_banners_id = array(
		'9370',
		'9371',
		'9372',
		'9373',
		'9374',
		'9375',
		'9376',
		'9377',
		'9378',
		'9379'
	);
	
	return $tt_banners_id;
}


function fn_th_banners_clone_image($banners, $lang_code) {
	$tt_banners_id = fn_get_tt_banners_id();
	
    foreach ($banners as $banner) {
	    
	    if (in_array($banner['banner_id'], $tt_banners_id)) {

	        if (empty($banner['main_pair']['pair_id'])) {
	            continue;
	        }
	
	        $data_banner_image = array(
	            'banner_id' => $banner['banner_id'],
	            'lang_code' => $lang_code
	        );
	        $banner_image_id = db_query("REPLACE INTO ?:banner_images ?e", $data_banner_image);
	        fn_add_image_link($banner_image_id, $banner['main_pair']['pair_id']);
        
		}
        
    }
}

function fn_th_banners_clone_description($banners, $lang_code) {
	$tt_banners_id = fn_get_tt_banners_id();
	
    foreach ($banners as $banner) {
	    
	    if (in_array($banner['banner_id'], $tt_banners_id)) {
	    
		    $banner_data = fn_get_banner_data($banner['banner_id'], $banner['lang_code']);
		    
		    $data_banner_description = array(
	            'banner_id'   => $banner['banner_id'],
	            'banner'      => $banner_data['banner'],
	            'url'         => $banner_data['url'],
	            'description' => $banner_data['description'],
	            'lang_code'   => $lang_code
	        );
	
	        db_query("REPLACE INTO ?:banner_descriptions ?e", $data_banner_description);
        
        }
        
    }
}

function fn_ath_thin_theme_settings_install() {
	
    $banners = db_get_hash_multi_array("SELECT ?:banners.banner_id, ?:banner_images.banner_image_id, ?:banner_images.lang_code FROM ?:banners LEFT JOIN ?:banner_images ON ?:banner_images.banner_id = ?:banners.banner_id", array('lang_code', 'banner_id'));
	
    $langs = array_keys(Languages::getAll());

    if (!empty($langs)) {
        $clone_lang = DEFAULT_LANGUAGE;

        if (in_array(DEFAULT_LANGUAGE, $langs)) {
            $clone_lang = 'en';
        }

        foreach ($banners[$clone_lang] as $banner_id => &$banner) {
            $banner['main_pair'] = fn_get_image_pairs($banner['banner_image_id'], 'promo', 'M', true, false, $clone_lang);
        }

        foreach ($langs as $need_clone_lang) {
            fn_th_banners_clone_image($banners[$clone_lang], $need_clone_lang);
        }
        
        foreach ($langs as $need_clone_lang) {
            fn_th_banners_clone_description($banners[$clone_lang], $need_clone_lang);
        }
        
    }

    foreach ($banners['en'] as $banner_id => &$banner) {
        $banner['main_pair'] = fn_get_image_pairs($banner['banner_image_id'], 'promo', 'M', true, false, 'en');
    }

    if (!in_array('en', $langs)) {
        $banner_images_ids = db_get_fields("SELECT banner_image_id FROM ?:banner_images WHERE lang_code = ?s", 'en');
        foreach ($banner_images_ids as $banner_image_id) {
            fn_delete_image_pairs($banner_image_id, 'promo');
        }

        if (!empty($banner_images_ids)) {
            db_query("DELETE FROM ?:banner_images WHERE banner_image_id IN (?n)", $banner_images_ids);
        }
    }

    return true;
}

//mega menu
function fn_ath_thin_theme_settings_get_static_data($params, &$fields, $condition, $sorting, $lang_code) {
    $fields[] = "sd.content_type_l1";
    $fields[] = "sd.drop_down_type";
    $fields[] = "sd.content_type";
    $fields[] = "sd.float_right";
    $fields[] = "sd.full_tree_number_categories";
    $fields[] = "sd.wysiwyg";
    $fields[] = "sd.svg_icon";
}

function fn_tt_get_mega_menu_items($value, $block, $block_scheme) {
    $menu_items = array();
    
    if (!empty($block['content']['menu']) && Menu::getStatus($block['content']['menu']) == 'A') {
        $params = array(
            'section' => 'A',
            'get_params' => true,
            'icon_name' => 'static_data_icon',
            'multi_level' => true,
            'use_localization' => true,
            'status' => 'A',
            'generate_levels' => true,
            'request' => array(
                'menu_id' => $block['content']['menu'],
            )
        );

        $menu_items = fn_top_menu_form(fn_get_static_data($params));
        
        
        
        fn_dropdown_appearance_cut_second_third_levels($menu_items, 'subitems', $block['properties']);
        

        
         foreach ($menu_items as &$menu_item) {
            if (count($menu_item['subitems'])>0 || $menu_item['content_type_l1']=='full_tree') {
	            $menu_item['have_subitems'] = 'Y';
            } else {
	            $menu_item['have_subitems'] = 'N';
            }
            
            if (count($menu_item['subitems'])>0) {
	            $menu_item['subitems_count'] = count($menu_item['subitems']);
            }
            
            if ($menu_item['content_type_l1']=='full_tree') {

	            $params['plain'] = false;
	            $params['group_by_level'] = true;
	            $params['max_nesting_level'] = 3;
	            $params['get_images'] = true;
	            
	            $menu_item['subitems'] = fn_get_categories( $params );   
         
	            foreach ($menu_item['subitems']['0'] as &$cat_l1) {
		            $cat_l1['mega_m_category_icon'] = fn_get_image_pairs($cat_l1['category_id'], 'mega_m_category_icon', 'M', true, true, DESCR_SL);
					$cat_l1['mega_m_category_banner'] = fn_get_image_pairs($cat_l1['category_id'], 'mega_m_category_banner', 'M', true, true, DESCR_SL);
					
					if ($cat_l1['subcategories']) {
						foreach ($cat_l1['subcategories'] as &$cat_l2) {
							$cat_l2['mega_m_category_icon'] = fn_get_image_pairs($cat_l2['category_id'], 'mega_m_category_icon', 'M', true, true, DESCR_SL);
						}
					}
	            }
            }        }
    }
    
    return $menu_items;
}

function fn_ath_thin_theme_settings_update_category_post($category_data, $category_id, $lang_code) {
	fn_attach_image_pairs('mega_m_category_icon', 'mega_m_category_icon', $category_id, DESCR_SL);
	fn_attach_image_pairs('mega_m_category_banner', 'mega_m_category_banner', $category_id, DESCR_SL);
}

function fn_ath_thin_theme_settings_get_category_data_post($category_id, $field_list, $get_main_pair, $skip_company_condition, $lang_code, &$category_data) {
	$category_data['mega_m_category_icon'] = fn_get_image_pairs($category_id, 'mega_m_category_icon', 'M', true, true, $lang_code);
	$category_data['mega_m_category_banner'] = fn_get_image_pairs($category_id, 'mega_m_category_banner', 'M', true, true, $lang_code);
}

function fn_ath_thin_theme_settings_get_categories($params, $join, $condition, &$fields, $group_by, $sortings, $lang_code) {
	$fields[] = '?:category_descriptions.mega_m_category_banner_url';
	$fields[] = '?:category_descriptions.mega_m_category_svg_icon';
}

if (! function_exists('array_column')) {
    function array_column(array $input, $columnKey, $indexKey = null) {
        $array = array();
        foreach ($input as $value) {
            if ( !array_key_exists($columnKey, $value)) {
                trigger_error("Key \"$columnKey\" does not exist in array");
                return false;
            }
            if (is_null($indexKey)) {
                $array[] = $value[$columnKey];
            }
            else {
                if ( !array_key_exists($indexKey, $value)) {
                    trigger_error("Key \"$indexKey\" does not exist in array");
                    return false;
                }
                if ( ! is_scalar($value[$indexKey])) {
                    trigger_error("Key \"$indexKey\" does not contain scalar value");
                    return false;
                }
                $array[$value[$indexKey]] = $value[$columnKey];
            }
        }
        return $array;
    }
}
 
function fn_activate_license_ath_thin_theme_settings( $api_params, $license_server_url ) {
	$api_params['slm_action'] = 'slm_activate';
	return json_decode(Http::get($license_server_url, $api_params), true);
}

function fn_check_license_ath_thin_theme_settings($type) {
	$name = 'ath_thin_theme_settings';
		
	$api_params = array(
		'slm_action' => 'slm_check',
		'secret_key' => '588dcea47787b9.48200647',
		'license_key' => Registry::get('addons.'.$name.'.license'),
		'registered_domain' => Registry::get('config.http_host')
	);
	if (fn_allowed_for('ULTIMATE') && !Registry::get('runtime.simple_ultimate')) {
		$api_params['product_ref'] = 'lt';
	}
	if (fn_allowed_for('MULTIVENDOR')) {
		$api_params['product_ref'] = 'mv';
	}
	if (fn_allowed_for('MULTIVENDOR:ULTIMATE')) {
		$api_params['product_ref'] = 'mvplt';
	}

	$license_server_url = 'http://licenses.themehills.com';

	$response = json_decode(Http::get($license_server_url, $api_params), true);

	if ($response == '') {
		$ativation = false;
		$message = 'license_error_not_available';
	} elseif ($response['result'] == 'success') {
		if ($response['status'] == 'active') {
			$check_host = array_search(Registry::get('config.http_host'), array_column($response['registered_domains'], 'registered_domain'));
			if ($check_host !== false) {
				$ativation = true;
			} elseif ($response['max_allowed_domains'] > count($response['registered_domains'])) {	
				$activate_response = fn_activate_license_ath_thin_theme_settings($api_params, $license_server_url);
				if ($activate_response['result'] == 'success') 
					$ativation = true;
				else {			
					$ativation = false;
					$message = 'license_error_'.$name.'_already_activated';
				}
			} else {
				$ativation = false;
				$message = 'license_error_'.$name.'_domain_limit';
			}
			if ( ((fn_allowed_for('ULTIMATE') && !Registry::get('runtime.simple_ultimate')) || fn_allowed_for('MULTIVENDOR')) && ($response['product_ref'] != $api_params['product_ref']) ) {
				$ativation = false;
				$message = 'license_error_'.$name.'_wrong_prod_'.$api_params['product_ref'];
			}
		} elseif ( $response['status'] == 'pending' ) {
			if ( ((fn_allowed_for('ULTIMATE') && !Registry::get('runtime.simple_ultimate')) || fn_allowed_for('MULTIVENDOR')) && ($response['product_ref'] != $api_params['product_ref']) ) {
					$ativation = false;
					$message = 'license_error_'.$name.'_wrong_prod_'.$api_params['product_ref'];
			} else {
				$activate_response = fn_activate_license_ath_thin_theme_settings($api_params, $license_server_url);
				if ($activate_response['result'] == 'success') 
					$ativation = true;
				else {						
					$ativation = false;
					$message = 'license_error_'.$name.'_domain_limit';
				}
			}
		} elseif ( $response['status'] == 'expired' ) {
			$ativation = false;
			$message = 'license_error_'.$name.'_expired';
		} elseif ( $response['status'] == 'blocked' ) {
			$ativation = false;
			$message = 'license_error_'.$name.'_blocked';
		} else {		
			$ativation = false;
			$message = 'license_error_'.$name.'_contect_us';
		}
	} else {
		$ativation = false;
		$message = 'license_error_'.$name.'_contect_us';
	}
	
	if (!$ativation) {		
		db_query("UPDATE ?:addons SET status = ?s WHERE addon = ?s", 'D', $name);
		fn_set_notification('E', __('error'), __($message));
		
		if ($type == 'return') {
			return array(CONTROLLER_STATUS_REDIRECT, 'addons.manage');
		} else {
			exit;
		}
	}
};

//get json list
function fn_tt_get_json_list($file) {
	$result = json_decode(fn_get_contents(__DIR__ . '/assets/json/' . $file), true);
	
	return $result;
}

function fn_tt_get_json_list_settings($file) {
	$anim_effects = fn_tt_get_json_list($file);
	
	$data = array(
        'NoN' => '--'
    );

	foreach ($anim_effects as $anim_group_name => $anim_group) {
		foreach ($anim_group as $anim_effect) {                         
			$data[$anim_effect]=$anim_effect;
		}
	}

	return $data;
}