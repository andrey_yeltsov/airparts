<?php
/***************************************************************************
*                                                                          *
*   (c) 2016 ThemeHills - Themes and addons     					       *
*                                                                          *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$addons = Registry::get('addons');
	if (!isset($addons['ath_mega_menu']['status']) || $addons['ath_mega_menu']['status'] != 'A' ) {
	    if ($mode == 'update') {
	        fn_trusted_vars('static_data');
	    }
    }
}
