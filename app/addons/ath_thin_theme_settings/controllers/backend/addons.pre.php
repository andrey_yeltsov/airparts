<?php
/***************************************************************************
*                                                                          *
*   (c) 2017 ThemeHills - Premium themes and addons					       *
*                                                                          *
****************************************************************************/

use Tygh\Settings;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($mode == 'update_status' && $_REQUEST['addon'] == 'ath_thin_theme_settings' ) {
	    fn_check_license_ath_thin_theme_settings('return');
	}
	if ($mode == 'update' && $_REQUEST['addon'] == 'ath_thin_theme_settings' ) {
		fn_check_license_ath_thin_theme_settings('return');
	}
}
