<?php

if (!defined('BOOTSTRAP')) { die('Access denied'); }

$schema['buybox'] = array(
    'limit' => array(
        'type' => 'input',
        'default_value' => 10,
    ),
);

return $schema;
