<?php


if ( !defined('BOOTSTRAP') ) { die('Access denied'); }

$schema['products']['content']['items']['fillings']['buybox'] = array (
	'params' => array (
		'disable_searchanise' => true,
		'sort_by' => 'price',
		'sort_order' => 'asc',
		'request' => array (
			'current_product_id' => '%PRODUCT_ID%'
		)
	),
);

return $schema;
