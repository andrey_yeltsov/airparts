<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_aaa_buybox_get_products(&$params, $fields, $sortings, &$condition, $join, $sorting, $group_by, $lang_code)
{
    if (@$params['block_data']['content']['items']['filling'] == 'buybox') {

        $product = db_get_row('
            SELECT product_code, company_id
            FROM ?:products
            WHERE product_id = ?i',
            $params['current_product_id']);
        $condition .= db_quote(" AND products.product_code = ?s AND products.company_id <> ?i", $product['product_code'], $product['company_id']);
    }

    return true;
}
