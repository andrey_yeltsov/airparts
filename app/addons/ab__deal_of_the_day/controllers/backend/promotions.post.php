<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2019   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and  accept   *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
use Tygh\Registry;
if (!defined('BOOTSTRAP')) {
die('Access denied');
}
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
if ($mode == 'update') {
if (!empty($_REQUEST['ab__dotd_active_year'])) {
Tygh::$app['session']['ab__dotd_active_year'] = $_REQUEST['ab__dotd_active_year'];
Tygh::$app['session']['ab__dotd_active_month'] = $_REQUEST['ab__dotd_active_month'];
}
}
}
if ($mode == ab_____(base64_decode('dnFlYnVm')) || $mode == ab_____(base64_decode('YmVl'))) {
if (fn_check_view_permissions(ab_____(base64_decode('YmNgYGVmYm1gcGdgdWlmYGViei93amZ4')))) {
Registry::set(ab_____(base64_decode('b2J3amhidWpwby91YmN0L2JjYGBlcHVl')), [
'title' => __('ab__deal_of_the_day'),
'js' => true,
]);
Registry::set(ab_____(base64_decode('b2J3amhidWpwby91YmN0L2JjYGBlcHVlYHRkaWZldm1m')), [
'title' => __(ab_____(base64_decode('YmNgYGVwdWVgdGRpZmV2bWY='))),
'js' => true,
]);
$promotion_data = Tygh::$app['view']->getTemplateVars('promotion_data');
if (!empty($promotion_data)) {
fn_ab__dotd_rebuild_promotions_periods($promotion_data['promotion_id']);
$promotion_data = call_user_func(ab_____(base64_decode('Z29gYmNgYGVwdWVgaGZ1YHFzcG5wdWpwb2B0ZnBgZWJ1Yg==')),$promotion_data, DESCR_SL);
if (!empty($promotion_data['ab__dotd_schedule'])) {
$promotion_data['ab__dotd_schedule'] = unserialize($promotion_data['ab__dotd_schedule']);
}
$years = empty($promotion_data['ab__dotd_schedule']) ? [] : array_keys($promotion_data['ab__dotd_schedule']);
$default_years = [
date('Y', strtotime('-1 year')),
date('Y', strtotime('+1 year')),
date('Y', TIME),
];
foreach ($default_years as $default_year) {
if (!in_array($default_year, $years)) {
$years[] = $default_year;
}
}
sort($years);
Tygh::$app['view']->assign('ab__dotd_years', $years);
Tygh::$app['view']->assign('ab__dotd_active_year', Tygh::$app['session']['ab__dotd_active_year']);
Tygh::$app['view']->assign('ab__dotd_active_month', Tygh::$app['session']['ab__dotd_active_month']);
Tygh::$app['view']->assign('promotion_data', $promotion_data);
}
}
} elseif ($mode == 'picker') {
list($promotions, $search) = fn_get_promotions($_REQUEST, Registry::get('settings.Appearance.admin_elements_per_page'), DESCR_SL);
Tygh::$app['view']->assign('search', $search);
Tygh::$app['view']->assign('promotions', $promotions);
Tygh::$app['view']->display('addons/ab__deal_of_the_day/pickers/promotions/picker_contents.tpl');
exit;
}
