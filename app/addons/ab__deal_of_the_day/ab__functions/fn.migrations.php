<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2019   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and  accept   *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
use Tygh\Themes\Themes;
if (!defined('BOOTSTRAP')) {
die('Access denied');
}
/**
* Runs on addon install
*/
function fn_ab__deal_of_the_day_install()
{
fn_ab__dotd_install_rebuild_tables();
fn_ab__dotd_install_add_layouts();
}
/**
* Rebuilds tables to v3.0.0 format
*/
function fn_ab__dotd_install_rebuild_tables()
{
$is_old_tables = db_has_table('ab__deal_of_the_day');
if ($is_old_tables) {
$data = db_get_array('SELECT * FROM ?:ab__deal_of_the_day');
if (!empty($data)) {
$ab__dotd = $ab__dotd_descriptions = [];
foreach ($data as $promotion) {
$ab__dotd[$promotion['promotion_id']] = [
'promotion_id' => $promotion['promotion_id'],
'filter' => empty($promotion['filter']) ? 'N' : $promotion['filter'],
'use_products_filter' => empty($promotion['use_products_filter']) ? 'Y' : $promotion['use_products_filter'],
'hide_products_block' => empty($promotion['hide_products_block']) ? 'N' : $promotion['hide_products_block'],
'use_schedule' => empty($promotion['use_schedule']) ? 'N' : $promotion['use_schedule'],
'ab__dotd_schedule' => empty($promotion['ab__dotd_schedule']) ? null : $promotion['ab__dotd_schedule'],
];
$ab__dotd_descriptions[] = [
'promotion_id' => $promotion['promotion_id'],
'h1' => empty($promotion['h1']) ? '' : $promotion['h1'],
'page_title' => empty($promotion['page_title']) ? '' : $promotion['page_title'],
'meta_description' => empty($promotion['meta_description']) ? '' : $promotion['meta_description'],
'meta_keywords' => empty($promotion['meta_keywords']) ? '' : $promotion['meta_keywords'],
'lang_code' => $promotion['lang_code'],
];
}
if (!empty($ab__dotd)) {
db_query('REPLACE INTO ?:ab__dotd ?m', $ab__dotd);
}
if (!empty($ab__dotd_descriptions)) {
db_query('REPLACE INTO ?:ab__dotd_descriptions ?m', $ab__dotd_descriptions);
}
}
db_query('DROP TABLE IF EXISTS ?:ab__deal_of_the_day');
}
}
/**
* Move layouts from repo to installed themes
*
* @param string $theme Name of theme to copy (all installed, if empty)
*/
function fn_ab__dotd_install_add_layouts($theme = '')
{
$target_themes = empty($theme) ? fn_get_installed_themes() : [$theme];
$design_dir = fn_get_theme_path('[themes]/', 'C');
$path = 'layouts/addons/ab__deal_of_the_day';
foreach ($target_themes as $theme_name) {
$repo_path = Themes::factory($theme_name)->getThemeRepoPath();
$repo_path = rtrim($repo_path, '/') . '/';
if (is_dir($repo_path . $path)) {
fn_copy($repo_path . $path, $design_dir . $theme_name . '/' . $path);
}
}
}
/**
* Remove layouts from instaled themes
*/
function fn_ab__deal_of_the_day_uninstall()
{
$installed_themes = fn_get_installed_themes();
$design_dir = fn_get_theme_path('[themes]/', 'C');
foreach ($installed_themes as $theme_name) {
$path = $design_dir . $theme_name . '/layouts/addons/ab__deal_of_the_day';
if (is_dir($path)) {
fn_rm($path);
}
}
}
