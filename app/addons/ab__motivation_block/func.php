<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2019   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and  accept   *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
use Tygh\Registry;
use Tygh\Settings;
use Tygh\Languages\Languages;
use Tygh\Navigation\LastView;
if (!defined('BOOTSTRAP')) {
die('Access denied');
}
define('AB__MB_DATA_PATH', Registry::get('config.dir.var') . 'ab__data/ab__motivation_block/');
function fn_ab__mb_install()
{
$objects = array(
array(
'table' => '?:ab__mb_motivation_items',
'field' => 'categories_ids',
'sql' => 'ALTER TABLE ?:ab__mb_motivation_items ADD `categories_ids` text DEFAULT NULL after `icon_color`',
'add_sql' => array(),
),
array(
'table' => '?:ab__mb_motivation_items',
'field' => 'destinations_ids',
'sql' => 'ALTER TABLE ?:ab__mb_motivation_items ADD `destinations_ids` text DEFAULT NULL after `categories_ids`',
'add_sql' => array(),
),
);
if (!empty($objects) && is_array($objects)) {
foreach ($objects as $object) {
$fields = db_get_fields('DESCRIBE ' . $object['table']);
if (!empty($fields) && is_array($fields)) {
$is_present_field = false;
foreach ($fields as $f) {
if ($f == $object['field']) {
$is_present_field = true;
break;
}
}
if (!$is_present_field) {
db_query($object['sql']);
if (!empty($object['add_sql'])) {
foreach ($object['add_sql'] as $sql) {
db_query($sql);
}
}
}
}
}
}
$repo_path = Registry::get('config.dir.design_frontend') . Registry::get('config.base_theme');
$ty_path = $repo_path . '/css/tygh/icons.less';
$file_content = fn_get_contents($ty_path);
$ty_file_dir = Registry::get('config.dir.design_backend') . 'css/addons/ab__motivation_block/';
$ty_file_path = $ty_file_dir . 'ty_icons.less';
if (!is_file($ty_file_path)) {
$file_content = str_replace('../media/fonts/', '../../../media/fonts/addons/ab__motivation_block/', $file_content);
file_put_contents($ty_file_path, $file_content);
$extensions = array('eot', 'woff', 'ttf', 'svg');
$fonts_dir = Registry::get('config.dir.design_backend') . 'media/fonts/addons/ab__motivation_block/';
fn_mkdir($fonts_dir);
for ($i = 0; $i < count($extensions); $i++) {
fn_copy($repo_path . '/media/fonts/glyphs.' . $extensions[$i], $fonts_dir . 'glyphs.' . $extensions[$i]);
}
}
}
function fn_ab__mb_update_motivation_item($motivation_item_data, $motivation_item_id, $lang_code = DESCR_SL, $company_id = 0)
{
$exists = db_get_field('SELECT motivation_item_id FROM ?:ab__mb_motivation_items WHERE motivation_item_id = ?i AND company_id = ?i', $motivation_item_id, $company_id);
if (empty($motivation_item_data['destinations_ids'])) {
$motivation_item_data['destinations_ids'] = null;
}
if (empty($motivation_item_data['categories_ids'])) {
$motivation_item_data['categories_ids'] = null;
}
if (empty($exists)) {
$motivation_item_data['company_id'] = fn_allowed_for('ULTIMATE') ? fn_get_runtime_company_id() : $company_id;
$motivation_item_data['motivation_item_id'] = $motivation_item_id = db_query('INSERT INTO ?:ab__mb_motivation_items ?e', $motivation_item_data);
foreach (Languages::getAll() as $motivation_item_data['lang_code'] => $v) {
db_query('INSERT INTO ?:ab__mb_motivation_item_descriptions ?e', $motivation_item_data);
}
} else {
db_query('UPDATE ?:ab__mb_motivation_items SET ?u WHERE motivation_item_id = ?i AND company_id = ?i', $motivation_item_data, $motivation_item_id, $company_id);
db_query('UPDATE ?:ab__mb_motivation_item_descriptions SET ?u WHERE motivation_item_id = ?i AND company_id = ?i AND lang_code = ?s', $motivation_item_data, $motivation_item_id, $company_id, $lang_code);
}
if (fn_allowed_for('ULTIMATE') || $company_id == 0) {
fn_attach_image_pairs('ab__mb_img', 'motivation_item', $motivation_item_id);
}
return $motivation_item_id;
}
function fn_ab__mb_delete_motivation_item($motivation_item_id)
{
if (!empty($motivation_item_id)) {
db_query('DELETE FROM ?:ab__mb_motivation_items WHERE motivation_item_id = ?i', $motivation_item_id);
db_query('DELETE FROM ?:ab__mb_motivation_item_descriptions WHERE motivation_item_id = ?i', $motivation_item_id);
return true;
}
return false;
}
function fn_ab__mb_get_motivation_items($params = array(), $items_per_page = 0, $lang_code = CART_LANGUAGE)
{
$params = LastView::instance()->update('ab__motivation_items', $params);
$default_params = array(
'page' => 1,
'items_per_page' => $items_per_page,
'area' => AREA,
);
$params = array_merge($default_params, $params);
$sortings = array(
'name' => '?:ab__mb_motivation_item_descriptions.name',
'status' => '?:ab__mb_motivation_items.status',
'position' => '?:ab__mb_motivation_items.position',
);
$sorting = db_sort($params, $sortings, 'position', 'asc');
$fields = array(
'?:ab__mb_motivation_items.*',
'?:ab__mb_motivation_item_descriptions.name',
'?:ab__mb_motivation_item_descriptions.description',
'?:ab__mb_motivation_item_descriptions.lang_code',
);
$condition = $limit = $join = '';
if (AREA == 'C') {
$condition .= db_quote(' AND ?:ab__mb_motivation_items.status = ?s', 'A');
}
if (!empty($params['item_ids'])) {
$condition .= db_quote(' AND ?:ab__mb_motivation_items.motivation_item_id IN (?n)', explode(',', $params['item_ids']));
}
if (!empty($params['vendor_edit'])) {
$condition .= db_quote(' AND ?:ab__mb_motivation_items.vendor_edit = ?s', 'Y');
}
if (!empty($params['category_id'])) {
$condition .= db_quote(' AND (categories_ids IS NULL OR FIND_IN_SET(?i, categories_ids))', $params['category_id']);
}
if ($params['area'] == 'C') {
$params['destination_id'] = Tygh::$app['location']->getDestinationId();
}
if (!empty($params['destination_id'])) {
$condition .= db_quote(' AND (destinations_ids IS NULL OR FIND_IN_SET(?i, destinations_ids))', $params['destination_id']);
}
if (isset($params['name']) && fn_string_not_empty($params['name'])) {
$condition .= db_quote(' AND ?:ab__mb_motivation_item_descriptions.name LIKE ?l', '%' . trim($params['name']) . '%');
}
if (fn_allowed_for('ULTIMATE')) {
$condition .= fn_get_company_condition('?:ab__mb_motivation_items.company_id');
} elseif (!empty($params['company_id'])) {
$fields[] = 'vd.description AS vendor_description';
$join .= db_quote(' LEFT JOIN ?:ab__mb_vendors_descriptions AS vd ON vd.motivation_item_id = ?:ab__mb_motivation_items.motivation_item_id AND vd.company_id = ?i AND vd.lang_code = ?s', $params['company_id'], $lang_code);
}
$join .= db_quote(' INNER JOIN ?:ab__mb_motivation_item_descriptions ON ?:ab__mb_motivation_item_descriptions.motivation_item_id = ?:ab__mb_motivation_items.motivation_item_id AND ?:ab__mb_motivation_item_descriptions.company_id = ?:ab__mb_motivation_items.company_id AND ?:ab__mb_motivation_item_descriptions.lang_code = ?s', $lang_code);
if (!empty($params['items_per_page'])) {
$params['total_items'] = db_get_field('SELECT COUNT(DISTINCT(?:ab__mb_motivation_items.motivation_item_id)) FROM ?:ab__mb_motivation_items ?p WHERE 1 ?p', $join, $condition);
$limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
}
$motivation_items = db_get_hash_array('SELECT ?p FROM ?:ab__mb_motivation_items ?p WHERE 1 ?p ?p ?p', 'motivation_item_id', implode(', ', $fields), $join, $condition, $sorting, $limit);
$image_pairs = fn_get_image_pairs(array_keys($motivation_items), 'motivation_item', 'M');
foreach ($image_pairs as $key => $image) {
$motivation_items[$key]['main_pair'] = reset($image);
}
if (fn_allowed_for('MULTIVENDOR')) {
foreach ($motivation_items as &$item) {
if (!empty($item['vendor_description'])) {
$item['description'] = $item['vendor_description'];
}
}
}
if ($params['area'] == 'A') {
foreach ($motivation_items as $motivation_item_id => $motivation_item) {
$categories = fn_get_categories_list($motivation_item['categories_ids']);
$motivation_items[$motivation_item_id]['categories'] = implode(', ', $categories);
$destinations = fn_ab__mb_get_destinations_list($motivation_item['destinations_ids']);
$motivation_items[$motivation_item_id]['destinations'] = implode(', ', $destinations);
}
}
return array($motivation_items, $params);
}
function fn_ab__mb_get_motivation_item_data($motivation_item_id, $lang_code = CART_LANGUAGE)
{
$fields = array(
'?:ab__mb_motivation_items.*',
'?:ab__mb_motivation_item_descriptions.name',
'?:ab__mb_motivation_item_descriptions.description',
);
$join = db_quote('LEFT JOIN ?:ab__mb_motivation_item_descriptions ON ?:ab__mb_motivation_item_descriptions.motivation_item_id = ?:ab__mb_motivation_items.motivation_item_id AND ?:ab__mb_motivation_item_descriptions.lang_code = ?s', $lang_code);
$condition = db_quote('?:ab__mb_motivation_items.motivation_item_id = ?i', $motivation_item_id);
if (fn_allowed_for('ULTIMATE')) {
$condition .= fn_get_company_condition('?:ab__mb_motivation_items.company_id');
} else {
$condition .= db_quote(' AND ?:ab__mb_motivation_items.company_id = ?i', 0);
}
$motivation_item = db_get_row('SELECT ?p FROM ?:ab__mb_motivation_items ?p WHERE ?p', implode(',', $fields), $join, $condition);
if (empty($motivation_item)) {
return false;
}
$motivation_item['main_pair'] = fn_get_image_pairs($motivation_item['motivation_item_id'], 'motivation_item', 'M');
return $motivation_item;
}
function fn_ab__mb_update_by_vendor($motivation_item_data, $motivation_item_id, $lang_code, $company_id)
{
if (empty($motivation_item_data) || empty($motivation_item_data['description'])) {
return false;
}
db_replace_into('ab__mb_vendors_descriptions', array(
'motivation_item_id' => $motivation_item_id,
'company_id' => $company_id,
'lang_code' => $lang_code,
'description' => $motivation_item_data['description'],
));
}
function fn_ab__motivation_block_install_blocks($status = 'A')
{
$path = AB__MB_DATA_PATH . 'blocks/';
$company_id = fn_get_runtime_company_id();
$theme_name = Settings::instance()->getValue('theme_name', '', $company_id);
if (!is_file($path . $theme_name . '.json')) {
$theme_name = 'responsive';
}
$data = json_decode(fn_get_contents($path . $theme_name . '.json'), true);
if (!empty($data)) {
$notifications = array();
$langs = Languages::getAll();
$is_ru = in_array('ru', array_keys($langs));
foreach ($data as $block) {
$block['status'] = $status;
$block_id = fn_ab__mb_update_motivation_item($block, 0, CART_LANGUAGE, $company_id);
if ($is_ru) {
fn_ab__mb_update_motivation_item($block['ru'], $block_id, 'ru', $company_id);
}
$notifications[] = '<a href="' . fn_url('ab__motivation_block.update&motivation_item_id=' . $block_id) . '">' . (CART_LANGUAGE == 'ru' ? $block['ru']['name'] : $block['name']) . '</a>';
}
fn_set_notification('N', __('notice'), __('ab__mb.demodata.successes.blocks', array('[blocks]' => implode(', ', $notifications))));
return $notifications;
}
fn_set_notification('E', __('error'), __('ab__mb.demodata.errors.no_data'));
return false;
}
function fn_ab__motivation_block_prepare_block_to_cloning($block = array())
{
unset($block['motivation_item_id']);
$block['status'] = 'D';
$block['name'] .= ' [CLONE]';
return $block;
}
function fn_ab__mb_clone_element($item_id)
{
$old_data = fn_ab__mb_get_motivation_item_data($item_id);
if (empty($old_data)) {
return false;
}
$company_id = fn_get_runtime_company_id();
$old_data = fn_ab__motivation_block_prepare_block_to_cloning($old_data);
$new_id = fn_ab__mb_update_motivation_item($old_data, 0, CART_LANGUAGE, $company_id);
foreach (array_keys(Languages::getAll()) as $lang_code) {
if ($lang_code != CART_LANGUAGE) {
$temp = fn_ab__motivation_block_prepare_block_to_cloning(fn_ab__mb_get_motivation_item_data($item_id, $lang_code));
fn_ab__mb_update_motivation_item($temp, $new_id, $lang_code, $company_id);
}
}
return $new_id;
}
function fn_ab__mb_change_element_status($elements, $status)
{
db_query('UPDATE ?:ab__mb_motivation_items SET status = ?s WHERE motivation_item_id in (?n)', $status, (array) $elements);
}
function fn_ab__mb_get_destinations_list($destinations_ids, $lang_code = CART_LANGUAGE)
{
static $max_destinations = 10;
$d_names = array();
if (!empty($destinations_ids)) {
$d_ids = fn_explode(',', $destinations_ids);
$tr_d_ids = array_slice($d_ids, 0, $max_destinations);
foreach ($tr_d_ids as $tr_d_id) {
$d_names[] = fn_get_destination_name($tr_d_id, $lang_code);
}
if (sizeof($tr_d_ids) < sizeof($d_ids)) {
$d_names[] = '... (' . sizeof($d_ids) . ')';
}
} else {
$d_names[] = __('ab__mb_all_destinations');
}
return $d_names;
}
