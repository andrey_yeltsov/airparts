<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2018   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and  accept   *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
use Tygh\Registry;
if (!defined('BOOTSTRAP')) { die('Access denied'); }
function fn_ab__cwe_format_email ($e){
if (strlen($e) > 5 and strpos($e,'@') !== false){
list($a, $b) = explode('@', $e);
return $a . microtime(true) . '@' . $b;
}
return false;
}
function fn_ab__checkout_without_email_change_order_status($status_to, $status_from, $order_info, &$force_notification, $order_statuses, $place_order){
$email_mask = Registry::get('addons.ab__checkout_without_email.user_email');
if (!empty($email_mask) and strlen($email_mask)){
list(,$email_end) = explode('@', $email_mask);
if (isset($order_info['email']) and strpos($order_info['email'], "@".$email_end)!==false){
$force_notification['C'] = false;
}
}
}
