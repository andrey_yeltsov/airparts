<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2020   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and accept    *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
if (!defined('BOOTSTRAP')) {
die('Access denied');
}
use Tygh\Registry;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
return;
}
if ($mode == 'view') {
$page_data = Tygh::$app['view']->getTemplateVars('page');
if (Registry::get('addons.blog.status') == 'A' && $page_data['page_type'] == PAGE_TYPE_BLOG) {
$banner_id = db_get_field('SELECT b.banner_id FROM ?:banners AS b
WHERE b.status = \'A\' AND b.abt__yt_page_id = ?i AND type = \'abyt\' AND abt__yt_data_type = \'blog\'
ORDER BY b.position ASC LIMIT 1', $page_data['page_id']);
if (!empty($banner_id)) {
Tygh::$app['view']->assign('b', fn_abt__yt_get_banner_data(['banner_id' => $banner_id], true));
}
}
}
