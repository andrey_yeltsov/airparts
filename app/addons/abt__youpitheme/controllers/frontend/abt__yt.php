<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2020   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and accept    *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
if (!defined('BOOTSTRAP')) {
die('Access denied');
}
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
if ($mode == 'get_banner') {
if (!empty($_REQUEST['banners'])) {
if (!empty($_REQUEST['banners'])) {
$res = fn_abt__yt_get_banners_content($_REQUEST['banners']);
foreach ($res as $id => $html) {
Tygh::$app['ajax']->assignHTML($id, $html);
}
}
}
exit();
}
if ($mode == 'load_menu') {
$result_ids = (array) explode(',', $_REQUEST['result_ids']);
if (!empty($result_ids)) {
foreach ($result_ids as $result_id) {
Tygh::$app['ajax']->assignHtml($result_id, fn_abt__yt_ajax_menu_get($result_id));
}
}
exit;
}
if ($mode == 'get_features' && AREA == 'C' && defined('AJAX_REQUEST')) {
if (!empty($_REQUEST['product']) && (int) $_REQUEST['product'] > 0 && !empty($_REQUEST['category']) && (int) $_REQUEST['category'] > 0 && $_REQUEST['existing_ids']) {
$arr = [];
$arr[$_REQUEST['product']]['product_id'] = $_REQUEST['product'];
$arr[$_REQUEST['product']]['category_ids'] = [$_REQUEST['category']];
$arr[$_REQUEST['product']]['existing'] = explode(',', $_REQUEST['existing_ids']);
$featuresArr = fn_abt__yt_get_products_features_list($arr);
if (!empty($featuresArr)) {
Tygh::$app['view']->assign('product', ['main_category' => $_REQUEST['category']]);
Tygh::$app['view']->assign('product_features_list', $featuresArr[$_REQUEST['product']]);
Tygh::$app['ajax']->assign('getted_features', Tygh::$app['view']->fetch('addons/abt__youpitheme/views/abt__yt_product_features/list.tpl'));
}
}
exit();
}
}
if ($mode == 'show_banner_in_popup' && defined('AJAX_REQUEST')) {
if (!empty($_REQUEST['link_id'])) {
$link_id = intval($_REQUEST['link_id']);
$b = fn_abt__yt_get_banner_data(db_get_row('SELECT link_id, banner_id, width FROM ?:abt__yt_objects_banners WHERE link_id = ?i', $_REQUEST['link_id']), true);
if (!empty($b)) {
Tygh::$app['view']->assign('b', $b);
if ($b['abt__yt_data_type'] == 'blog' && $b['abt__yt_product_list_use'] == 'Y' && !empty($b['abt__yt_product_list'])) {
list($products) = fn_get_products(['pid' => $b['abt__yt_product_list']]);
fn_gather_additional_products_data($products, [
'get_icon' => true,
'get_detailed' => true,
'get_additional' => true,
'get_options' => true,
'get_discounts' => true,
'get_features' => false,
]);
Tygh::$app['view']->assign('products', $products);
}
Tygh::$app['ajax']->assign('data', Tygh::$app['view']->fetch('addons/abt__youpitheme/views/abt__yt_banner/view_in_popup.tpl'));
}
}
exit();
} elseif ($mode == 'qty_discounts' && !empty($_REQUEST['product_id'])) {
$product = fn_get_product_data($_REQUEST['product_id'], $auth, CART_LANGUAGE, '', true, true, true, true, fn_is_preview_action($auth, $_REQUEST), true, false, true);
Tygh::$app['view']->assign('product', $product);
Tygh::$app['view']->assign('ab__is_popup', true);
Tygh::$app['ajax']->assignHtml($_REQUEST['result_ids'], Tygh::$app['view']->fetch('views/products/components/products_qty_discounts.tpl'));
exit();
}
