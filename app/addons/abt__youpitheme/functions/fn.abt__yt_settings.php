<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2020   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and accept    *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
use Tygh\Registry;
if (!defined('BOOTSTRAP')) {
die('Access denied');
}
function fn_abt__youpitheme_dispatch_assign_template()
{
Registry::set('settings.abt__yt', fn_get_abt__yt_settings());
Registry::set('settings.abt__device', fn_abt__yt_get_device_type());
}
function fn_get_abt__yt_settings($type = 'general', $full_info = false, $style = '')
{
static $settings;
$lang_code = DESCR_SL;
$company_id = fn_get_runtime_company_id();
$cache_prefix = 'abt__youpitheme';
$key = "_{$lang_code}_" . md5('settings' . $type . ($full_info ? 'true' : 'false') . $style);
$cache_tables = ['abt__yt_settings', 'abt__yt_less_settings'];
if (fn_allowed_for('MULTIVENDOR')) {
$cache_tables[] = 'companies';
}
Registry::registerCache([$cache_prefix, $key], $cache_tables, Registry::cacheLevel('static'), true);
if (empty($settings[$key])) {
if ($cache = Registry::get($key)) {
$settings[$key] = $cache;
} else {
$schema_settings = fn_get_schema('abt__yt_settings', $type == 'general' ? 'objects' : 'less_objects');
usort($schema_settings, function ($a, $b) {
if (empty($a['position'])) {
$a['position'] = 10000;
}
if (empty($b['position'])) {
$b['position'] = 10000;
}
if ($a['position'] > $b['position']) {
return 1;
} elseif ($a['position'] < $b['position']) {
return -1;
}
return strcasecmp($a['section'], $b['section']);
});
foreach ($schema_settings as &$schema_setting) {
usort($schema_setting['items'], function ($a, $b) {
if ($a['position'] > $b['position']) {
return 1;
} elseif ($a['position'] < $b['position']) {
return -1;
}
return strcasecmp($a['name'], $b['name']);
});
}
if ($type == 'general') {
$db_settings = db_get_hash_multi_array('SELECT * FROM ?:abt__yt_settings WHERE lang_code = ?s ?p', ['section', 'name'], $lang_code, fn_get_company_condition('?:abt__yt_settings.company_id'));
} else {
$db_settings = db_get_hash_multi_array('SELECT * FROM ?:abt__yt_less_settings WHERE style = ?s ?p', ['section', 'name'], $style, fn_get_company_condition('?:abt__yt_less_settings.company_id'));
}
foreach ($schema_settings as $section) {
if (!empty($section['items']) && is_array($section['items'])) {
$items = [];
foreach ($section['items'] as $i) {
if ($full_info) {
$items[$i['name']] = $i;
if (!empty($db_settings[$section['section']][$i['name']])) {
$items[$i['name']]['value'] = $db_settings[$section['section']][$i['name']]['value'];
} elseif ($type == 'less' && !empty($i['value_styles'][$style])) {
$items[$i['name']]['value'] = $i['value_styles'][$style];
}
} else {
$items[$i['name']] = $i['value'];
if (!empty($db_settings[$section['section']][$i['name']])) {
$items[$i['name']] = $db_settings[$section['section']][$i['name']]['value'];
} elseif ($type == 'less' && !empty($i['value_styles'][$style])) {
$items[$i['name']] = $i['value_styles'][$style];
}
}
}
if (!empty($items)) {
$settings[$key][$section['section']] = $items;
}
}
}
Registry::set($key, $settings[$key]);
}
}
return $settings[$key];
}
function fn_update_abt__yt_settings($data, $type = 'general', $style = '')
{
$lang_code = DESCR_SL;
$company_id = fn_get_runtime_company_id();
$settings = fn_get_abt__yt_settings($type, true, $style);
foreach ($settings as $section => $items) {
foreach ($items as $name => $item) {
$v = (isset($data[$section][$name]) ? $data[$section][$name] : $item['value']);
if ($type == 'general') {
$d = [
'section' => $section,
'name' => $name,
'company_id' => $company_id,
'lang_code' => $lang_code,
'value' => $v,
];
$n = db_get_field('SELECT name FROM ?:abt__yt_settings WHERE section = ?s AND name = ?s ?p LIMIT 1', $section, $name, fn_get_company_condition('?:abt__yt_settings.company_id'));
if (empty($n)) {
foreach (fn_get_translation_languages() as $d['lang_code'] => $l) {
db_query('INSERT INTO ?:abt__yt_settings ?e', $d);
}
} else {
db_query('REPLACE INTO ?:abt__yt_settings ?e', $d);
}
$multilanguage = (!empty($item['multilanguage']) && $item['multilanguage'] == 'Y') ? 'Y' : 'N';
if ($multilanguage == 'N') {
db_query('UPDATE ?:abt__yt_settings SET value = ?s WHERE section = ?s AND name = ?s ?p', $v, $section, $name, fn_get_company_condition('?:abt__yt_settings.company_id'));
}
} elseif ($type == 'less') {
$d = [
'style' => $style,
'section' => $section,
'name' => $name,
'company_id' => $company_id,
'value' => $v,
];
db_query('REPLACE INTO ?:abt__yt_less_settings ?e', $d);
if (!preg_match('/^ab__/', $section)) {
$less_file = Registry::get('config.dir.design_frontend') . 'abt__youpitheme/styles/data/' . $style;
if (file_exists($less_file)) {
$less = fn_get_contents($less_file);
if (!empty($less)) {
if ($item['type'] == 'checkbox') {
$v = ($v == 'Y') ? 'true' : 'false';
}
$v .= (!empty($item['suffix'])) ? $item['suffix'] : '';
$less_var = '@abt__yt_' . $section . '_' . $name . ': ' . $v . ";\n";
if (preg_match('/@abt__yt_' . $section . '_' . $name . ':.*?;/m', $less)) {
$less = preg_replace('/(*ANYCRLF)@abt__yt_' . $section . '_' . $name . ':.*?;$/m', str_replace("\n", '', $less_var), $less);
} else {
$less .= $less_var;
}
fn_put_contents($less_file, $less);
}
}
} else {
$addon = $section;
$less_file = Registry::get('config.dir.root') . "/design/themes/abt__youpitheme/css/addons/{$addon}/styles/data/{$style}";
if ($item['type'] == 'checkbox') {
$v = ($v == 'Y') ? 'true' : 'false';
}
$v .= (!empty($item['suffix'])) ? $item['suffix'] : '';
$less_var = sprintf("@abt__yt_addons_%s_%s: %s;\n", $addon, $name, $v);
if (!file_exists($less_file)) {
fn_put_contents($less_file, $less_var);
} else {
$less = fn_get_contents($less_file);
if (preg_match('/@abt__yt_addons_' . $addon . '_' . $name . ':.*?;/m', $less)) {
$less = preg_replace('/(*ANYCRLF)@abt__yt_addons_' . $addon . '_' . $name . ':.*?;$/m', str_replace("\n", '', $less_var), $less);
} else {
$less .= $less_var;
}
fn_put_contents($less_file, $less);
}
}
fn_clear_cache('assets');
}
}
}
return true;
}
function fn_abt__yt_get_device_type()
{
static $device_type = '';
if (!empty($device_type)) {
return $device_type;
}
if (defined('CONSOLE') || !isset($_SERVER['HTTP_USER_AGENT']) || !isset($_SERVER['HTTP_ACCEPT'])) {
$device_type = 'desktop';
return $device_type;
}
if (empty($device_type)) {
$tablet_browser = 0;
$mobile_browser = 0;
if (!empty($_SERVER['HTTP_USER_AGENT'])) {
$http_user_agent = strtolower($_SERVER['HTTP_USER_AGENT']);
$mobile_agents = ['w3c ', 'acs-', 'alav', 'alca', 'amoi', 'audi',
'avan', 'benq', 'bird', 'blac', 'blaz', 'brew', 'cell', 'cldc', 'cmd-',
'dang', 'doco', 'eric', 'hipt', 'inno', 'ipaq', 'java', 'jigs', 'kddi',
'keji', 'leno', 'lg-c', 'lg-d', 'lg-g', 'lge-', 'maui', 'maxo', 'midp',
'mits', 'mmef', 'mobi', 'mot-', 'moto', 'mwbp', 'nec-', 'newt', 'noki',
'palm', 'pana', 'pant', 'phil', 'play', 'port', 'prox', 'qwap', 'sage',
'sams', 'sany', 'sch-', 'sec-', 'send', 'seri', 'sgh-', 'shar', 'sie-',
'siem', 'smal', 'smar', 'sony', 'sph-', 'symb', 't-mo', 'teli', 'tim-',
'tosh', 'tsm-', 'upg1', 'upsi', 'vk-v', 'voda', 'wap-', 'wapa', 'wapi',
'wapp', 'wapr', 'webc', 'winw', 'winw', 'xda ', 'xda-', ];
if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/', $http_user_agent)) {
$tablet_browser++;
}
if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/', $http_user_agent)) {
$mobile_browser++;
}
if (in_array(substr($http_user_agent, 0, 4), $mobile_agents)) {
$mobile_browser++;
}
if (strpos($http_user_agent, 'opera mini') > 0) {
$mobile_browser++;
$stock_ua = isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA']) ? $_SERVER['HTTP_X_OPERAMINI_PHONE_UA'] : (isset($_SERVER['HTTP_DEVICE_STOCK_UA']) ? $_SERVER['HTTP_DEVICE_STOCK_UA'] : '');
if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/', strtolower($stock_ua))) {
$tablet_browser++;
}
}
}
if (!empty($_SERVER['HTTP_ACCEPT'])) {
if (strpos(strtolower($_SERVER['HTTP_ACCEPT']), 'application/vnd.wap.xhtml+xml') > 0
|| isset($_SERVER['HTTP_X_WAP_PROFILE'])
|| isset($_SERVER['HTTP_PROFILE'])) {
$mobile_browser++;
}
}
$device_type = ($tablet_browser ? 'tablet' : ($mobile_browser ? 'mobile' : 'desktop'));
}
return $device_type;
}
function fn_get_abt__yt_addons_less_settings($style_id)
{
$result = [];
$temp_settings = fn_get_abt__yt_settings('less', true, $style_id);
foreach ($temp_settings as $section => $settings) {
if (preg_match('/^ab__/', $section)) {
$result[$section] = $settings;
}
}
return $result;
}
function fn_abt__youpitheme_styles_block_files(&$styles)
{
$runtime_layout = Registry::get('runtime.layout');
if (AREA == 'C' && $runtime_layout['theme_name'] == 'abt__youpitheme') {
$style_id = $runtime_layout['style_id'] . '.less';
$settings = fn_get_abt__yt_addons_less_settings($style_id);
$temp_styles = [];
if (!empty($settings)) {
$root = Registry::get('config.dir.root') . '/';
$pattern = '@design/themes/abt__youpitheme/css/addons/(' . implode('|', array_keys($settings)) . ')/styles.less$@';
foreach ($styles as $style) {
if (preg_match($pattern, $style['file'], $match)) {
$addon = $match[1];
$style_file = "design/themes/abt__youpitheme/css/addons/{$addon}/styles/data/{$style_id}";
$temp_styles[] = [
'file' => $root . $style_file,
'relative' => $style_file,
'media' => '',
];
if (!file_exists($root . $style_file) && !empty($settings[$addon])) {
$d = [];
foreach ($settings[$addon] as $setting => $setting_data) {
$value = $setting_data['value'];
if ($setting_data['type'] == 'checkbox') {
$value = $setting_data['value'] == 'Y' ? 'true' : 'false';
}
$value .= (!empty($setting_data['suffix'])) ? $setting_data['suffix'] : '';
$d[] = sprintf('@abt__yt_addons_%s_%s: %s;', $addon, $setting, $value);
}
fn_put_contents($root . $style_file, implode("\n", $d) . "\n");
}
}
$temp_styles[] = $style;
}
}
if (!empty($temp_styles)) {
$styles = $temp_styles;
}
}
}
