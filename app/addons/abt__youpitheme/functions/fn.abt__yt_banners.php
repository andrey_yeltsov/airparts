<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2020   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and accept    *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
use Tygh\Registry;
if (!defined('BOOTSTRAP')) {
die('Access denied');
}
function fn_abt__youpitheme_get_banner_data_post($banner_id, $lang_code, &$banner)
{
if (!empty($banner) && in_array($banner['type'], ['abyt', 'abyt_advanced'])) {
$banner['abt__yt_main_image'] = fn_get_image_pairs($banner['banner_id'], 'abt__yt_banners', 'M', true, true);
$banner['abt__yt_background_image'] = fn_get_image_pairs($banner['banner_id'], 'abt__yt_banners', 'A', true, true);
$banner['abt__yt_background_image'] = reset($banner['abt__yt_background_image']);
}
}
function fn_abt__youpitheme_get_banner_data($banner_id, $lang_code, &$fields, $joins, $condition)
{
$fields = array_merge($fields, [
'?:banners.abt__yt_use_avail_period	',
'?:banners.abt__yt_avail_from',
'?:banners.abt__yt_avail_till',
'?:banners.abt__yt_button_use',
'?:banners.abt__yt_button_text_color',
'?:banners.abt__yt_button_text_color_use',
'?:banners.abt__yt_button_color',
'?:banners.abt__yt_button_color_use',
'?:banners.abt__yt_title_color',
'?:banners.abt__yt_title_color_use',
'?:banners.abt__yt_title_font_size',
'?:banners.abt__yt_title_font_weight',
'?:banners.abt__yt_title_tag',
'?:banners.abt__yt_title_shadow',
'?:banners.abt__yt_description_font_size',
'?:banners.abt__yt_description_color',
'?:banners.abt__yt_description_color_use',
'?:banners.abt__yt_description_bg_color',
'?:banners.abt__yt_description_bg_color_use',
'?:banners.abt__yt_object',
'?:banners.abt__yt_background_color',
'?:banners.abt__yt_background_color_use',
'?:banners.abt__yt_class',
'?:banners.abt__yt_color_scheme',
'?:banners.abt__yt_content_valign',
'?:banners.abt__yt_content_align',
'?:banners.abt__yt_content_full_width',
'?:banners.abt__yt_content_bg',
'?:banners.abt__yt_content_bg_color_use',
'?:banners.abt__yt_content_bg_color',
'?:banners.abt__yt_padding',
'?:banners.abt__yt_how_to_open',
'?:banners.abt__yt_data_type',
'?:banners.abt__yt_page_id',
'?:banners.abt__yt_youtube_use',
'?:banners.abt__yt_youtube_autoplay',
'?:banners.abt__yt_youtube_loop',
'?:banners.abt__yt_youtube_hide_controls',
'?:banners.abt__yt_product_list_use',
'?:banners.abt__yt_product_list',
'?:banners.abt__yt_promotion_id',
'?:banners.abt__yt_countdown_use',
'?:banner_descriptions.abt__yt_button_text',
'?:banner_descriptions.abt__yt_title',
'?:banner_descriptions.abt__yt_url',
'?:banner_descriptions.abt__yt_description',
'?:banner_descriptions.abt__yt_youtube_id',
'?:banner_descriptions.abt__yt_youtube_playlist',
'?:banner_descriptions.abt__yt_youtube_thumb_key',
'?:banner_descriptions.abt__yt_product_list_title',
]);
}
function fn_abt__youpitheme_get_banners($params, &$condition, $sorting, $limit, $lang_code)
{
if (!empty($params['get_abyt_types'])) {
$condition .= db_quote(' AND ?:banners.type like \'abyt%\'');
} elseif (!empty($params['get_abyt'])) {
$condition .= db_quote(' AND ?:banners.type like \'abyt\'');
} elseif (!empty($params['get_abyt_advanced'])) {
$condition .= db_quote(' AND ?:banners.type like \'abyt_advanced\'');
}
if (AREA == 'C') {
$sub_cond = db_quote(' ?:banners.type like \'abyt%\' AND ( ?:banners.abt__yt_use_avail_period = \'N\'
OR
(
?:banners.abt__yt_use_avail_period = \'Y\'
AND (?:banners.abt__yt_avail_from = 0 OR ?:banners.abt__yt_avail_from <= ' . TIME . ')
AND (?:banners.abt__yt_avail_till = 0 OR ?:banners.abt__yt_avail_till >= ' . TIME . ')
)
)');
$condition .= db_quote(" AND IF (?:banners.type not like 'abyt%', 'available', IF ($sub_cond, 'available', 'not available') ) = 'available' ");
}
}
function fn_abt__youpitheme_get_banners_post(&$banners, $params)
{
if (AREA == 'C') {
foreach ($banners as &$banner) {
$banner = array_merge($banner, fn_get_banner_data($banner['banner_id']));
}
}
}
function fn_abt__youpitheme_block_updated($block_id)
{
fn_abt__yt_update_object_banners('block', $block_id, isset($_REQUEST['block_data']) ? $_REQUEST['block_data'] : []);
}
function fn_abt__youpitheme_update_category_post($category_data, $category_id, $lang_code)
{
fn_abt__yt_update_object_banners('category', $category_id, $category_data);
}
function fn_abt__yt_update_object_banners($object = 'category', $object_id = 0, $data = [])
{
if ($object_id && isset($data['abt__yt_banners_use'])) {
if (empty($data['abt__yt_banners'])) {
db_query('DELETE FROM ?:abt__yt_objects_banners WHERE object = ?s AND object_id = ?i', $object, $object_id);
} else {
$link_ids = [];
foreach ($data['abt__yt_banners'] as $b) {
if (!empty($b['banner_id'])) {
$d = $b;
$d['object'] = $object;
$d['object_id'] = $object_id;
if (empty($b['link_id'])) {
$link_ids[] = db_query('INSERT INTO ?:abt__yt_objects_banners ?e', $d);
} else {
db_query('UPDATE ?:abt__yt_objects_banners SET ?u WHERE link_id = ?i', $d, $b['link_id']);
$link_ids[] = $b['link_id'];
}
}
}
if (empty($link_ids)) {
db_query('DELETE FROM ?:abt__yt_objects_banners WHERE object = ?s AND object_id = ?i', $object, $object_id);
} else {
db_query('DELETE FROM ?:abt__yt_objects_banners WHERE object = ?s AND object_id = ?i AND link_id not in (?n)', $object, $object_id, $link_ids);
}
}
}
}
function fn_abt__yt_get_product_list_banners($object = 'category', $object_id = 0, $only_active = false, $position_from = 0, $position_to = 0)
{
$banners = [];
$cond = '';
if ($position_from > 0 && $position_to > 0) {
$cond = db_quote(' AND (ob.position between ?i AND ?i) ', $position_from, $position_to);
}
if ($object_id) {
if (!$only_active) {
$banners = db_get_hash_array('SELECT *
FROM ?:abt__yt_objects_banners as ob
WHERE ob.object = ?s
AND ob.object_id = ?i ?p
ORDER BY ob.position asc, ob.priority asc, ob.link_id asc', 'link_id', $object, $object_id, $cond);
} else {
$banners = db_get_hash_array('SELECT ob.*, b.abt__yt_data_type, b.abt__yt_page_id, b.abt__yt_promotion_id
FROM ?:abt__yt_objects_banners as ob
INNER JOIN ?:banners as b ON (b.banner_id = ob.banner_id)
WHERE ob.object = ?s
AND ob.object_id = ?i
AND b.status = \'A\' ?p
AND (b.abt__yt_use_avail_period = \'N\'
OR (
b.abt__yt_use_avail_period = \'Y\'
AND (b.abt__yt_avail_from = 0 OR b.abt__yt_avail_from <= ' . TIME . ')
AND (b.abt__yt_avail_till = 0 OR b.abt__yt_avail_till >= ' . TIME . ')
)
)
ORDER BY ob.position asc, ob.priority asc, ob.link_id asc', 'link_id', $object, $object_id, $cond);
$types_banners = [];
if (!empty($banners)) {
foreach ($banners as $banner) {
$types_banners[$banner['abt__yt_data_type']][$banner['link_id']] = $banner;
}
foreach ($types_banners as $type => $links) {
if ($type == 'blog') {
$field = 'abt__yt_page_id';
list(, $join, $condition) = fn_get_pages([
'get_conditions' => true,
'page_type' => PAGE_TYPE_BLOG,
'item_ids' => implode(',', (array) array_keys(fn_array_value_to_key($links, $field))),
'status' => 'A',
]);
$objects = db_get_fields('SELECT ?:pages.page_id FROM ?:pages ?p WHERE ?p', $join, $condition);
foreach ($banners as $banner_id => $banner) {
if ($banner['abt__yt_data_type'] == $type && !in_array($banner[$field], $objects)) {
unset($banners[$banner_id]);
}
}
} elseif ($type == 'promotion') {
$field = 'abt__yt_promotion_id';
$objects = fn_get_promotions([
'promotion_id' => array_keys(fn_array_value_to_key($links, $field)),
'active' => true,
'simple' => true,
]);
$objects = array_keys($objects);
foreach ($banners as $banner_id => $banner) {
if ($banner['abt__yt_data_type'] == $type && !in_array($banner[$field], $objects)) {
unset($banners[$banner_id]);
}
}
}
}
}
}
}
return $banners;
}
function fn_abt__yt_get_abyt_banners()
{
list($abt__yt_banners) = fn_get_banners(['get_abyt' => true]);
$abt__yt_banners_grouped = [];
if (!empty($abt__yt_banners)) {
foreach ($abt__yt_banners as $id => $b) {
$abt__yt_banners_grouped[$b['type']][$id] = $b;
}
}
return $abt__yt_banners_grouped;
}
function fn_abt__yt_get_object_banners($object = 'category', $object_id = 0, $search = [])
{
$res = [];
if (
($object == 'block' && 'Y' == db_get_field('SELECT abt__yt_banners_use FROM ?:bm_blocks WHERE block_id = ?i', $object_id))
||
($object == 'category' && 'Y' == db_get_field('SELECT abt__yt_banners_use FROM ?:categories WHERE category_id = ?i', $object_id))
) {
$position_from = 0;
$position_to = 0;
if ($object == 'category' && !empty($search)) {
$position_from = ($search['page'] - 1) * $search['items_per_page'] + 1;
$position_to = $search['page'] * $search['items_per_page'];
if ($position_to > $search['total_items']) {
$position_to = $search['total_items'];
}
}
$banners = fn_abt__yt_get_product_list_banners($object, $object_id, true, $position_from, $position_to);
if (!empty($banners) && is_array($banners)) {
$g_banners = [];
foreach ($banners as $b) {
$g_banners[$b['position']]['width'] = $b['width'];
$g_banners[$b['position']]['items'][] = $b['link_id'];
}
$dispatch = Registry::get('runtime.controller') . Registry::get('runtime.view');
foreach ($g_banners as $position => $b) {
$items = implode(',', $b['items']);
$res[$position]['id'] = 'b' . (string) crc32($dispatch . $object . $object_id . $items);
$res[$position]['items'] = $items;
$res[$position]['width'] = $b['width'];
$res[$position]['object'] = $object;
$res[$position]['object_id'] = $object_id;
}
}
}
return $res;
}
function fn_abt__yt_insert_banners_into_list($id = 0, $banners, $products, $search, $class = 'col-tile')
{
if (empty($id)) {
return '';
}
static $products_map = [];
$key = (string) crc32(serialize(array_keys($banners)) . serialize(array_keys($products)) . serialize($search));
if (empty($products_map[$key])) {
if (!empty($banners) && !empty($products)) {
$banners_map = [];
foreach ($banners as $k => $b) {
$banners_map[$k] = $k;
if ($b['width'] == 2) {
$banners_map[$k + 1] = $k;
}
}
$initial_position = 1;
if (!empty($search['page']) && $search['page'] > 1) {
$initial_position = ($search['page'] - 1) * $search['items_per_page'] + 1;
}
$map = [];
$total_items = count($banners) + count($products);
for ($i = $initial_position; $i <= ($initial_position - 1) + $total_items; $i++) {
if (!empty($products)) {
if (!empty($banners_map[$i])) {
$map[$i] = $banners_map[$i];
} else {
$p = array_shift($products);
$map[$i] = 'p-' . $p['product_id'];
}
}
}
$b_data = [];
foreach ($map as $m) {
if (strpos($m, 'p-') === false) {
$b_data[$banners[$m]['id']] = $banners[$m];
} else {
if (!empty($b_data)) {
$products_map[$key][$m] = $b_data;
$b_data = [];
}
}
}
}
}
if (!empty($products_map[$key]["p-$id"])) {
$html = '';
foreach ($products_map[$key]["p-$id"] as $b) {
$html .= "<div class='bnw-x{$b['width']} {$class}'><div data-o='{$b['object']}' data-i='{$b['object_id']}' data-b='{$b['items']}' id='{$b['id']}' class='abyt_banner'></div></div>";
}
return $html;
}
return '';
}
function fn_abt__yt_get_banners_content($data)
{
$res = [];
$banners_data = db_get_hash_array('SELECT link_id, banner_id, width FROM ?:abt__yt_objects_banners WHERE link_id in (?n)', 'link_id', array_keys(fn_array_value_to_key($data, 'banner')));
foreach ($data as $d) {
$curr_banner_data = array_merge($banners_data[$d['banner']], [
'has_buttons' => $d['has_buttons'],
]);
$res[$d['id']] = Tygh::$app['view']->assign('b', fn_abt__yt_get_banner_data($curr_banner_data))
->assign('type_list', $d['obj'])
->fetch('addons/abt__youpitheme/views/abt__yt_banner/view_in_list.tpl');
}
return $res;
}
function fn_abt__yt_get_banner_data($link_data, $check_products = false)
{
$b = [];
if (!empty($link_data['banner_id'])) {
$b = array_merge((array) fn_get_banner_data($link_data['banner_id']), $link_data);
if ($b['abt__yt_data_type'] == 'blog' && intval($b['abt__yt_page_id'])) {
$b['page_data'] = fn_get_page_data($b['abt__yt_page_id']);
if ($check_products) {
if ($b['abt__yt_product_list_use'] == 'Y' && !empty($b['abt__yt_product_list'])) {
$params = ['pid' => $b['abt__yt_product_list']];
list($products, $search) = fn_get_products($params);
fn_gather_additional_products_data($products, [
'get_icon' => true,
'get_detailed' => true,
'get_additional' => true,
'get_options' => true,
'get_discounts' => true,
'get_features' => false,
]);
$b['products'] = $products;
}
}
} elseif ($b['abt__yt_data_type'] == 'promotion' && intval($b['abt__yt_promotion_id'])) {
$b['promotion_data'] = fn_get_promotion_data($b['abt__yt_promotion_id']);
$b['promotion_data']['left_days'] = 0;
if ($b['promotion_data']['to_date'] > TIME) {
$b['promotion_data']['left_days'] = intval(floor(($b['promotion_data']['to_date'] - TIME) / 86400));
}
if (function_exists('fn_ab__dotd_get_promotion_seo_data')) {
$b['promotion_data'] = fn_ab__dotd_get_promotion_seo_data($b['promotion_data']);
}
}
if (!empty($link_data['has_buttons'])) {
$b['has_buttons'] = $link_data['has_buttons'];
}
}
return $b;
}
function fn_abt__yt_build_youtube_link($d, $only_params = false)
{
$link = "https://www.youtube.com/embed/{$d['abt__yt_youtube_id']}";
$params = [];
$params['rel'] = 'rel=0';
$params['showinfo'] = 'showinfo=0';
$params['modestbranding'] = 'modestbranding=1';
if (!empty($d['abt__yt_youtube_playlist'])) {
$params['showinfo'] = 'showinfo=1';
$params['playlist'] = "playlist={$d['abt__yt_youtube_playlist']}";
}
if (!empty($d['abt__yt_youtube_autoplay']) && $d['abt__yt_youtube_autoplay'] == 'Y') {
$params['autoplay'] = 'autoplay=1';
$params['mute'] = 'mute=1';
}
if (!empty($d['abt__yt_youtube_loop']) && $d['abt__yt_youtube_loop'] == 'Y') {
$params['loop'] = 'loop=1';
$params['playlist'] = "playlist={$d['abt__yt_youtube_id']}";
}
if (!empty($d['abt__yt_youtube_hide_controls']) && $d['abt__yt_youtube_hide_controls'] == 'Y') {
$params['controls'] = 'controls=0';
}
$p = implode('&', $params);
return ($only_params) ? $p : $link . '?' . $p;
}
function fn_abt__yt_get_banner_disabled_position($max_position = 100)
{
$res = [];
foreach ([4, 5, 6] as $k) {
for ($i = 1; $k * $i <= $max_position; $i++) {
$res[] = $k * $i;
}
}
$res = array_unique($res);
return implode(',', $res);
}
