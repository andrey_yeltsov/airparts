<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2020   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and accept    *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
if (!defined('BOOTSTRAP')) {
die('Access denied');}
use Tygh\Registry;if (AREA == 'A') {
define('ABT__YT_DATA_EXP_PATH',Registry::get('config.dir.var').'ab__data/abt__youpitheme/exports/');define('ABT__YT_DEFAULT_EXP_LANGUAGE','en');function fn_abt__yt_export_blog(){
$dir=ABT__YT_DATA_EXP_PATH.call_user_func(call_user_func(call_user_func(call_user_func(call_user_func("\142\x61\163\x65\66\x34\137\x64\145\x63\157\x64\145",call_user_func("\x61\142\x5f\137\x5f\137\x5f","\x62\130\x32\170\x63\110\x3a\154\x5b\122\x3e\76")),"",["\x61\142\x5f\137","\x5f\137\x5f"]),call_user_func("\142\x61\163\x65\66\x34\137\x64\145\x63\157\x64\145","\x61\155\x35\170\x62\130\x42\154\x5a\147\x3d\75")),"",["\x62\141\x73\145\x36\64\x5f\144\x65","\143\x6f\144\x65"]),call_user_func("\141\x62\137\x5f\137\x5f\137","\x5a\156\x79\167\x5b\172\x39\76"));$blog_pages=array_shift(fn_get_pages([
'page_type'=>'B',
'get_tree'=>'multilevel',
],0,ABT__YT_DEFAULT_EXP_LANGUAGE)[0])['subpages'];$images=fn_get_image_pairs(array_keys($blog_pages),'blog','M',true,false);fn_rm($dir);fn_mkdir($dir);$arr=[];foreach ($blog_pages as $key=>$page) {
if ($images[$key]) {
$img=array_shift($images[$key])['icon'];$image_name='blog-image-'.$key.'.'.pathinfo($img['absolute_path'],PATHINFO_EXTENSION);$page['blog_image']=$image_name;fn_copy($img['absolute_path'],"{$dir}/{$image_name}");}
unset($page['page_id'],$page['parent_id'],$page['id_path'],$page['company_id'],$page['lang_code'],$page['main_pair'],$page['meta_keywords'],$page['meta_description'],$page['seo_name'],$page['seo_path'],$page['page_title'],$page['status']);$page['author']='AlexBranding';$ru=fn_get_page_data($key,'ru');$page['ru']=[
'lang_code'=>'ru',
'page'=>$ru['page']?$ru['page']:'',
'description'=>$ru['description']?$ru['description']:'',
];$arr[]=$page;}
abt__yt_export_write_in_file($dir,$arr,'blog');}
function fn_abt__yt_export_menu($menu_ids){
$dir=ABT__YT_DATA_EXP_PATH.call_user_func(call_user_func(call_user_func(call_user_func(call_user_func("\142\x61\163\x65\66\x34\137\x64\145\x63\157\x64\145",call_user_func("\x61\142\x5f\137\x5f\137\x5f","\x62\130\x32\170\x63\110\x3a\154\x5b\122\x3e\76")),"",["\x61\142\x5f\137","\x5f\137\x5f"]),call_user_func("\142\x61\163\x65\66\x34\137\x64\145\x63\157\x64\145","\x61\155\x35\170\x62\130\x42\154\x5a\147\x3d\75")),"",[call_user_func(call_user_func(call_user_func(call_user_func("\142\x61\163\x65\66\x34\137\x64\145\x63\157\x64\145",call_user_func("\x61\142\x5f\137\x5f\137\x5f","\x62\130\x32\170\x63\110\x3a\154\x5b\122\x3e\76")),"",["\x61\142\x5f\137","\x5f\137\x5f"]),call_user_func("\142\x61\163\x65\66\x34\137\x64\145\x63\157\x64\145","\x64\110\x56\172\x63\62\x5a\63")),call_user_func(call_user_func(call_user_func("\142\x61\163\x65\66\x34\137\x64\145\x63\157\x64\145",call_user_func("\x61\142\x5f\137\x5f\137\x5f","\x62\130\x32\170\x63\110\x3a\154\x5b\122\x3e\76")),"",["\x61\142\x5f\137","\x5f\137\x5f"]),call_user_func("\142\x61\163\x65\66\x34\137\x64\145\x63\157\x64\145","\x4e\124\x64\155\x64\107\x4a\152"))),call_user_func(call_user_func(call_user_func(call_user_func("\142\x61\163\x65\66\x34\137\x64\145\x63\157\x64\145",call_user_func("\x61\142\x5f\137\x5f\137\x5f","\x62\130\x32\170\x63\110\x3a\154\x5b\122\x3e\76")),"",["\x61\142\x5f\137","\x5f\137\x5f"]),call_user_func("\142\x61\163\x65\66\x34\137\x64\145\x63\157\x64\145","\x64\110\x56\172\x63\62\x5a\63")),call_user_func(call_user_func(call_user_func("\142\x61\163\x65\66\x34\137\x64\145\x63\157\x64\145",call_user_func("\x61\142\x5f\137\x5f\137\x5f","\x62\130\x32\170\x63\110\x3a\154\x5b\122\x3e\76")),"",["\x61\142\x5f\137","\x5f\137\x5f"]),call_user_func("\142\x61\163\x65\66\x34\137\x64\145\x63\157\x64\145","\x5a\155\x56\167\x5a\107\x5a\154\x59\101\x3d\75")))]),call_user_func("\141\142\x5f\137\137\137\137","\143\130\127\166\x65\124\71\76"));fn_rm($dir);fn_mkdir($dir);$data=[];$notif=[];foreach ($menu_ids as $menu_id) {
$menu_name=db_get_field('SELECT name FROM ?:menus_descriptions WHERE lang_code=?s AND menu_id=?i',CART_LANGUAGE,$menu_id);$_REQUEST['menu_id']=$menu_id;$p=[
'section'=>'A',
'status'=>'A',
'generate_levels'=>true,
'get_params'=>true,
'multi_level'=>true,
];$temp=fn_top_menu_form(fn_get_static_data($p,ABT__YT_DEFAULT_EXP_LANGUAGE));foreach ($temp as &$item) {
fn_abt__yt_add_more_to_static_data($item,$dir);}
$notif[]='<a target="_blank" href="'.fn_url('static_data.manage&section=A&menu_id='.$menu_id).'">'.$menu_id.'</a>';$data[$menu_name]=$temp;}
abt__yt_export_write_in_file($dir,$data,'menu',implode(',<br/>',['[ids]'=>$notif]));}
function fn_abt__yt_add_more_to_static_data(&$item,$path=ABT__YT_DATA_EXP_PATH){
$item['ru']=db_get_row('SELECT descr,abt__yt_mwi__text,abt__yt_mwi__desc,abt__yt_mwi__label
FROM ?:static_data_descriptions
WHERE lang_code=?s AND param_id=?n','ru',$item['param_id']);$icon=fn_get_image_pairs($item['param_id'],'abt__yt_mwi__icon','M',true,false);if (!empty($icon)) {
$ico_name=$item['param_id'].'-abt__yt_mwi__icon.'.pathinfo($icon['icon']['absolute_path'],PATHINFO_EXTENSION);$item['image']=$ico_name;fn_copy($icon['icon']['absolute_path'],"{$path}/{$ico_name}");}
fn_abt__yt_unset_static_data($item);if (!empty($item['param_3']) && strpos($item['param_3'],'Y') !== false) {
$item['ab__use_category_link']='Y';} else {
unset($item['param_3']);}
if (!empty($item['subitems'])) {
foreach ($item['subitems'] as $key=>&$subitem) {
if (isset($subitem['param_id'])) {
fn_abt__yt_add_more_to_static_data($subitem,$path);unset($subitem['parent_id']);} else {
unset($item['subitems'][$key]);}}} else {
unset($item['subitems']);}}
function fn_abt__yt_unset_static_data(&$item){
$arr=[
'id_path',
'param_id',
'status',
'param_2',
'param_4',
'param_5',
'parent_id',
'abt__yt_mwi__icon',
];foreach ($arr as $unset) {
if (isset($item[$unset])) {
unset($item[$unset]);}}}
function abt__yt_export_write_in_file($dir=ABT__YT_DATA_EXP_PATH,$arr=[],$type='blog',$replaces=[]){
$answer=fn_put_contents("{$dir}/data.json",json_encode($arr,JSON_PRETTY_PRINT));if ($answer !== false) {
fn_set_notification('N',__('notice'),__("abt__yt.export.success.{$type}",$replaces));} else {
fn_set_notification('E',__('error'),__('abt__yt.export.errors.file_input_error'));}
return $answer;}}
