<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2020   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and accept    *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
use Tygh\Registry;
function fn_abt__yt_refresh_icons_file()
{
$repo_path = Registry::get('config.dir.themes_repository') . 'abt__youpitheme';
$file_content = fn_get_contents($repo_path . '/css/addons/abt__youpitheme/icons.less');
$file_content = str_replace('media/custom_fonts', 'media/fonts/addons/abt__youpitheme', $file_content);
file_put_contents(Registry::get('config.dir.design_backend') . 'css/addons/abt__youpitheme/front_icons.less', $file_content);
$extensions = ['eot', 'woff', 'ttf', 'svg'];
$fonts_dir = Registry::get('config.dir.design_backend') . 'media/fonts/addons/abt__youpitheme/';
fn_mkdir($fonts_dir);
for ($i = 0; $i < count($extensions); $i++) {
copy($repo_path . '/media/custom_fonts/Social.' . $extensions[$i], $fonts_dir . 'Social.' . $extensions[$i]);
}
}
function fn_abt__youpitheme_ab__mb_get_icons_post(&$icons, $file_path)
{
$icons['ypi_social'] = fn_abt__yt_get_social_icons()['yt_icons'];
$icons['ypi_mb_icons'] = fn_abt__yt_get_mb_icons()['mb_icons'];
}
function fn_abt__yt_get_social_icons()
{
$repo_path = Registry::get('config.dir.themes_repository') . 'abt__youpitheme/';
$icons = [
'yt_icons' => [],
];
$file_path = $repo_path . 'css/addons/abt__youpitheme/icons.less';
$file_content = fn_get_contents($file_path);
if (!empty($file_content)) {
$rule = "'\.(?!ty-)(.*?):before'i";
preg_match_all($rule, $file_content, $matches);
$icons['yt_icons'] = $matches[1];
}
return $icons;
}
function fn_abt__yt_get_mb_icons()
{
$repo_path = Registry::get('config.dir.themes_repository') . 'abt__youpitheme/';
$icons = [
'mb_icons' => [],
];
$file_path = $repo_path . 'css/addons/ab__motivation_block/icons.less';
$file_content = fn_get_contents($file_path);
if (!empty($file_content)) {
$rule = "'\&\.(.*?):before'i";
preg_match_all($rule, $file_content, $matches);
$icons['mb_icons'] = $matches[1];
}
return $icons;
}
