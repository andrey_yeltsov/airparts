<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2020   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and accept    *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
use Tygh\Registry;
if (!defined('BOOTSTRAP')) {
die('Access denied');
}
function fn_abt__youpitheme_update_static_data(&$data, $param_id, $condition, $section, $lang_code)
{
if (Registry::get('runtime.mode') == 'update') {
$data['abt__yt_mwi__text'] = $_POST['static_data']['abt__yt_mwi__text'];
fn_attach_image_pairs('abt__yt_mwi__icon', 'abt__yt_mwi__icon', $param_id, $lang_code);
}
}
function fn_abt__youpitheme_get_static_data($params, &$fields, $condition, $sorting, $lang_code)
{
$fields[] = '?:static_data_descriptions.abt__yt_mwi__desc';
$fields[] = '?:static_data_descriptions.abt__yt_mwi__text';
$fields[] = '?:static_data_descriptions.abt__yt_mwi__label';
$fields[] = 'sd.abt__yt_mwi__status';
$fields[] = 'sd.abt__yt_mwi__text_position';
$fields[] = 'sd.abt__yt_mwi__dropdown';
$fields[] = 'sd.abt__yt_mwi__label_color';
$fields[] = 'sd.abt__yt_mwi__label_background';
}
function fn_abt__youpitheme_top_menu_form_post(&$top_menu, $level, $active)
{
static $abt__yt_mwi_icon_get = 'N';
static $abt__yt_mwi_icon_ids = [];
if ($abt__yt_mwi_icon_get == 'N') {
$abt__yt_mwi_icon_get = 'Y';
$abt__yt_mwi_icon_ids = db_get_fields('SELECT object_id FROM ?:images_links WHERE object_type = \'abt__yt_mwi__icon\'');
}
if ($abt__yt_mwi_icon_get == 'Y'
&& !empty($abt__yt_mwi_icon_ids) && is_array($abt__yt_mwi_icon_ids)
&& !empty($top_menu) && is_array($top_menu)) {
$ids = [];
foreach ($top_menu as $i => $m) {
if (in_array($i, $abt__yt_mwi_icon_ids) && !empty($m['abt__yt_mwi__status']) && $m['abt__yt_mwi__status'] == 'Y') {
$ids[] = $i;
}
}
if (!empty($ids) && is_array($ids)) {
$images = fn_get_image_pairs($ids, 'abt__yt_mwi__icon', 'M', true, false);
foreach ($images as $i => $image) {
$top_menu[$i]['abt__yt_mwi__icon'] = reset($image);
}
}
}
}
function fn_abt__yt_ajax_menu_save($data, $id, $lang_code = DESCR_SL)
{
static $init_cache = false;
$cache_name = 'abt__yt_am';
$key = $id . '_' . $lang_code . '_' . fn_abt__yt_get_device_type();
if (!$init_cache) {
$init_cache = true;
Registry::registerCache(['abt__youpitheme', $cache_name], ['static_data', 'static_data_descriptions'], Registry::cacheLevel('static'), true);
}
Registry::set($cache_name . '.' . $key, $data);
}
function fn_abt__yt_ajax_menu_get($id, $lang_code = CART_LANGUAGE)
{
static $init_cache = false;
$cache_name = 'abt__yt_am';
$key = $id . '_' . $lang_code . '_' . fn_abt__yt_get_device_type();
if (!$init_cache) {
$init_cache = true;
Registry::registerCache(['abt__youpitheme', $cache_name], ['static_data', 'static_data_descriptions'], Registry::cacheLevel('static'), true);
}
static $data;
if (empty($data)) {
$data = Registry::get($cache_name);
}
return isset($data[$key]) ? $data[$key] : '';
}
function fn_abt__yt_split_elements_for_menu($elements, $cols, $items_in_big_cols, $big_cols_count)
{
if ($cols == 1) {
return [$elements];
}
$return = [];
$big_cols_local_counter = 0;
for ($i = 0; $i < $cols; $i++) {
if (!empty($elements)) {
$return[$i] = [];
$local_counter = 0;
$in_col = $items_in_big_cols >= 2 ? $items_in_big_cols - 1 : 1;
if ($big_cols_local_counter < $big_cols_count || !$big_cols_count) {
$in_col = $items_in_big_cols;
$big_cols_local_counter++;
}
foreach ($elements as $id => $elem) {
if ($local_counter == $in_col) {
break;
}
$local_counter++;
$return[$i][$id] = $elem;
unset($elements[$id]);
}
}
}
return $return;
}
