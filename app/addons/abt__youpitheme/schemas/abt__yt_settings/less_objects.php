<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2020   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and accept    *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
$schema = [
[
'section' => 'general',
'position' => 100,
'items' => [
[
'name' => 'main_color',
'type' => 'colorpicker',
'position' => 100,
'value' => '#ffffff',
'value_styles' => [
'Bee.less' => '#ffffff',
'Bluor.less' => '#ffffff',
'Coral.less' => '#ffffff',
'Grass.less' => '#ffffff',
'Original.less' => '#ffffff',
'Plum.less' => '#ffffff',
'Sea.less' => '#ffffff',
'Sunset.less' => '#ffffff',
],
],
[
'name' => 'not_main_color',
'type' => 'checkbox',
'position' => 200,
'value' => 'Y',
],
[
'name' => 'variant_buttons_color',
'type' => 'checkbox',
'position' => 300,
'value' => 'N',
],
[
'name' => 'show_grid_border',
'type' => 'selectbox',
'position' => 400,
'value' => 'margin_border',
'variants' => [
'not_border',
'solid_border',
'margin_border',
],
'value_styles' => [
'Bee.less' => '',
'Bluor.less' => '',
'Coral.less' => '',
'Grass.less' => '',
'Original.less' => '',
'Plum.less' => '',
'Sea.less' => '',
'Sunset.less' => '',
],
],
[
'name' => 'show_rounds',
'type' => 'selectbox',
'position' => 500,
'value' => 'show_full_rounds',
'variants' => [
'not_rounds',
'show_rounds',
'show_full_rounds',
],
'value_styles' => [
'Bee.less' => '',
'Bluor.less' => '',
'Coral.less' => '',
'Grass.less' => '',
'Original.less' => '',
'Plum.less' => '',
'Sea.less' => '',
'Sunset.less' => '',
],
],
[
'name' => 'color_stars_rating',
'type' => 'colorpicker',
'position' => 600,
'value' => '#0083d9',
'value_styles' => [
'Bee.less' => '#0275d8',
'Bluor.less' => '#ed5f2b',
'Coral.less' => '#ee1c25',
'Grass.less' => '#F9690E',
'Original.less' => '#00a0b5',
'Plum.less' => '#4854a2',
'Sea.less' => '#00bbb3',
'Sunset.less' => '#6a6cb7',
],
],
],
],
];
return $schema;
