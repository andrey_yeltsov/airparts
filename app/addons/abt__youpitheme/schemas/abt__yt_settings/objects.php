<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2020   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and accept    *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
$schema = [
[
'section' => 'general',
'position' => 100,
'items' => [
[
'name' => 'brand_feature_id',
'type' => 'input',
'class' => 'input-small cm-value-integer',
'position' => 100,
'value' => 18,
],
[
'name' => 'load_more_products',
'type' => 'checkbox',
'position' => 200,
'value' => 'Y',
],
[
'name' => 'view_cat_subcategories',
'type' => 'checkbox',
'position' => 300,
'value' => 'N',
],
[
'name' => 'use_scroller_for_menu',
'type' => 'checkbox',
'position' => 400,
'value' => 'Y',
],
[
'name' => 'menu_min_height',
'type' => 'input',
'class' => 'input-small cm-value-integer',
'position' => 500,
'value' => 450,
],
[
'name' => 'blog_page_id',
'type' => 'input',
'class' => 'input-small cm-value-integer',
'position' => 600,
'value' => '',
],
[
'name' => 'breadcrumbs_view',
'type' => 'selectbox',
'class' => 'input-big',
'position' => 700,
'value' => 'hide_with_btn',
'variants' => [
'hide_with_btn',
'do_not_hide',
'show_only_first_and_last',
],
],
[
'name' => 'use_lazy_load_for_images',
'type' => 'checkbox',
'position' => 800,
'value' => 'Y',
],
[
'name' => 'check_clone_theme',
'type' => 'checkbox',
'position' => 10000,
'value' => 'Y',
],
],
],
[
'section' => 'product_list',
'position' => 200,
'items' => [
[
'name' => 'height_list_prblock',
'type' => 'input',
'class' => 'input-small cm-value-integer',
'position' => 50,
'value' => '430',
],
[
'name' => 'grid_item_hover_zoom',
'type' => 'checkbox',
'position' => 75,
'value' => 'N',
],
[
'name' => 'grid_list_descr',
'type' => 'selectbox',
'class' => 'input-big',
'position' => 100,
'value' => 'features',
'variants' => [
'none',
'description',
'features',
],
],
[
'name' => 'limit_product_variations',
'type' => 'input',
'class' => 'input-small cm-value-integer',
'position' => 50,
'value' => '5',
],
[
'name' => 'max_features',
'type' => 'selectbox',
'class' => 'input-small',
'position' => 200,
'value' => '3',
'variants' => [
'1',
'2',
'3',
'4',
'5',
'6',
'7',
'8',
'9',
'10',
],
'variants_as_language_variable' => 'N',
],
[
'name' => 'show_sku',
'type' => 'checkbox',
'position' => 300,
'value' => 'N',
],
[
'name' => 'show_amount',
'type' => 'checkbox',
'position' => 400,
'value' => 'Y',
],
[
'name' => 'show_brand',
'type' => 'selectbox',
'class' => 'input-big',
'position' => 500,
'value' => 'none',
'variants' => [
'none',
'as_text',
'as_logo',
],
],
[
'name' => 'show_qty',
'type' => 'checkbox',
'position' => 600,
'value' => 'N',
],
[
'name' => 'show_buttons',
'type' => 'checkbox',
'position' => 700,
'value' => 'Y',
],
[
'name' => 'service_buttons_position',
'type' => 'selectbox',
'class' => 'input-big',
'position' => 750,
'value' => 'splitted_buttons',
'variants' => [
'splitted_buttons',
'in_images_block',
'in_buttons_block',
],
],
[
'name' => 'show_qty_discounts',
'type' => 'checkbox',
'position' => 800,
'value' => 'N',
],
[
'name' => 'show_gallery',
'type' => 'checkbox',
'position' => 900,
'value' => 'N',
],
],
],
[
'section' => 'products',
'position' => 300,
'items' => [
[
'name' => 'id_block_con_right_col',
'type' => 'input',
'class' => 'input-small cm-value-integer',
'position' => 200,
'value' => '',
],
[
'name' => 'show_variants_of_variations_options',
'type' => 'selectbox',
'class' => 'input-big',
'position' => 300,
'value' => 'with_indent',
'variants' => [
'without_indent',
'with_indent',
'below_title',
],
],
],
],
];
return $schema;
