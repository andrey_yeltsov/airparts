<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2020   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and accept    *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
$schema['update_handlers'][] = 'abt__yt_settings';
$schema['update_handlers'][] = 'abt__yt_less_settings';
$schema['update_handlers'][] = 'abt__yt_microdata';
$schema['update_handlers'][] = 'abt__yt_microdata_description';
$schema['update_handlers'][] = 'banners';
$schema['update_handlers'][] = 'promotions';
$schema['update_handlers'][] = 'pages';
if (empty($schema['callable_handlers']['abt__device'])) {
$schema['callable_handlers']['abt__device'] = ['fn_abt__yt_get_device_type'];
}
return $schema;
