<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2020   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and accept    *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
$schema['blocks/products/abt__yt_products_multicolumns_with_banners.tpl'] = [
'settings' => [
'additional_data' => [
'type' => 'template',
'template' => 'addons/abt__youpitheme/views/abt__yt/components/block_manager/block_banners.tpl',
'default_value' => 2,
],
],
'bulk_modifier' => [
'fn_gather_additional_products_data' => [
'products' => '#this',
'params' => [
'get_icon' => true,
'get_detailed' => true,
'get_options' => true,
'get_additional' => true,
],
],
],
];
$schema['blocks/products/abt__yt_products_scroller_advanced_with_banners.tpl'] = [
'settings' => [
'show_price' => [
'type' => 'checkbox',
'default_value' => 'Y',
],
'enable_quick_view' => [
'type' => 'checkbox',
'default_value' => 'N',
],
'not_scroll_automatically' => [
'type' => 'checkbox',
'default_value' => 'N',
],
'scroll_per_page' => [
'type' => 'checkbox',
'default_value' => 'N',
],
'speed' => [
'type' => 'input',
'default_value' => 400,
],
'pause_delay' => [
'type' => 'input',
'default_value' => 3,
],
'item_quantity' => [
'type' => 'input',
'default_value' => 5,
],
'thumbnail_width' => [
'type' => 'input',
'default_value' => 80,
],
'outside_navigation' => [
'type' => 'checkbox',
'default_value' => 'Y',
],
'additional_data' => [
'type' => 'template',
'template' => 'addons/abt__youpitheme/views/abt__yt/components/block_manager/block_banners_without_x2.tpl',
'default_value' => 2,
],
],
'bulk_modifier' => [
'fn_gather_additional_products_data' => [
'products' => '#this',
'params' => [
'get_icon' => true,
'get_detailed' => true,
'get_options' => true,
'get_additional' => true,
],
],
],
];
$tmpls = [
'blocks/products/products_scroller.tpl',
'blocks/products/products_multicolumns.tpl',
'blocks/products/abt__yt_products_multicolumns_with_banners.tpl',
'blocks/products/abt__yt_products_scroller_advanced_with_banners.tpl',
];
if (!empty($tmpls)) {
foreach ($tmpls as $tmpl) {
$schema[$tmpl]['bulk_modifier']['fn_abt__yt_add_products_features_list'] = ['products' => '#this'];
}
}
$schema['blocks/menu/dropdown_horizontal_abt__yt_mwi.tpl'] = [
'settings' => [
'abt__yt_filling_type' => [
'type' => 'template',
'default_value' => 'column_filling',
'template' => 'addons/abt__youpitheme/views/abt__yt/components/block_manager/menu_filling_type.tpl',
'values' => [
'column_filling' => [
'show_fields' => [
'abt__yt_columns_count',
],
],
'row_filling' => [
'show_fields' => [
'abt__yt_columns_count',
],
],
],
],
'abt__yt_columns_count' => [
'type' => 'selectbox',
'default_value' => '4',
'values' => [
'2' => 'abt__yt_columns_count.2',
'3' => 'abt__yt_columns_count.3',
'4' => 'abt__yt_columns_count.4',
'5' => 'abt__yt_columns_count.5',
'6' => 'abt__yt_columns_count.6',
],
],
'abt_menu_long_names' => [
'type' => 'checkbox',
'default_value' => 'N',
],
'abt_menu_long_names_max_width' => [
'type' => 'input',
'default_value' => '100',
],
'dropdown_second_level_elements' => [
'type' => 'input',
'default_value' => '12',
],
'abt_menu_icon_items' => [
'type' => 'checkbox',
'default_value' => 'N',
],
'dropdown_third_level_elements' => [
'type' => 'input',
'default_value' => '6',
],
'no_hidden_elements_second_level_view' => [
'type' => 'input',
'default_value' => '5',
],
'abt_menu_ajax_load' => [
'type' => 'checkbox',
'default_value' => 'N',
],
],
];
$schema['blocks/menu/dropdown_vertical_abt__yt_mwi.tpl'] = [
'settings' => [
'abt__yt_filling_type' => [
'type' => 'template',
'default_value' => 'column_filling',
'template' => 'addons/abt__youpitheme/views/abt__yt/components/block_manager/menu_filling_type.tpl',
'values' => [
'column_filling' => [
'show_fields' => [
'abt__yt_columns_count',
],
],
'row_filling' => [
'show_fields' => [
'abt__yt_columns_count',
],
],
],
],
'abt__yt_columns_count' => [
'type' => 'selectbox',
'default_value' => '4',
'values' => [
'2' => 'abt__yt_columns_count.2',
'3' => 'abt__yt_columns_count.3',
'4' => 'abt__yt_columns_count.4',
'5' => 'abt__yt_columns_count.5',
'6' => 'abt__yt_columns_count.6',
],
],
'dropdown_second_level_elements' => [
'type' => 'input',
'default_value' => '12',
],
'abt_menu_icon_items' => [
'type' => 'checkbox',
'default_value' => 'N',
],
'dropdown_third_level_elements' => [
'type' => 'input',
'default_value' => '6',
],
'no_hidden_elements_second_level_view' => [
'type' => 'input',
'default_value' => '5',
],
'elements_per_column_third_level_view' => [
'type' => 'input',
'default_value' => '10',
],
'abt_menu_ajax_load' => [
'type' => 'checkbox',
'default_value' => 'N',
],
],
];
if (fn_allowed_for('MULTIVENDOR')) {
$schema['blocks/menu_mv/dropdown_horizontal_abt__yt_mwi.tpl'] = $schema['blocks/menu/dropdown_horizontal_abt__yt_mwi.tpl'];
$schema['blocks/menu_mv/dropdown_vertical_abt__yt_mwi.tpl'] = $schema['blocks/menu/dropdown_vertical_abt__yt_mwi.tpl'];
unset($schema['blocks/menu_mv/dropdown_horizontal_abt__yt_mwi.tpl']['settings']['abt_menu_icon_items'], $schema['blocks/menu_mv/dropdown_vertical_abt__yt_mwi.tpl']['settings']['abt_menu_icon_items']);
$schema['blocks/menu_mv/dropdown_horizontal_abt__yt_mwi.tpl']['params'] =
$schema['blocks/menu_mv/dropdown_vertical_abt__yt_mwi.tpl']['params'] = [
'plain' => false,
'group_by_level' => true,
'max_nesting_level' => 3,
'request' => [
'active_category_id' => '%CATEGORY_ID%',
],
];
}
return $schema;
