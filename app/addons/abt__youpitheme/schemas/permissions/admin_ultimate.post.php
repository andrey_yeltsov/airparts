<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2020   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and accept    *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
$schema['abt__yt']['modes']['settings']['page_title'] = 'abt__yt.settings';
$schema['abt__yt']['modes']['settings']['vendor_only'] = true;
$schema['abt__yt']['modes']['settings']['use_company'] = true;
$schema['abt__yt']['modes']['less_settings']['page_title'] = 'abt__yt.less_settings';
$schema['abt__yt']['modes']['less_settings']['vendor_only'] = true;
$schema['abt__yt']['modes']['less_settings']['use_company'] = true;
$schema['abt__yt']['modes']['microdata']['page_title'] = 'abt__yt.microdata';
$schema['abt__yt']['modes']['microdata']['vendor_only'] = true;
$schema['abt__yt']['modes']['microdata']['use_company'] = true;
$schema['abt__yt']['modes']['demodata']['page_title'] = 'abt__yt.demodata';
$schema['abt__yt']['modes']['demodata']['vendor_only'] = true;
$schema['abt__yt']['modes']['demodata']['use_company'] = true;
$schema['abt__yt_buy_together']['modes']['generate']['page_title'] = 'abt__yt_buy_together.manage';
$schema['abt__yt_buy_together']['modes']['generate']['vendor_only'] = true;
$schema['abt__yt_buy_together']['modes']['generate']['use_company'] = true;
$schema['abt__yt_buy_together']['modes']['manage']['page_title'] = 'abt__yt_buy_together.generate';
$schema['abt__yt_buy_together']['modes']['manage']['vendor_only'] = true;
$schema['abt__yt_buy_together']['modes']['manage']['use_company'] = true;
return $schema;
