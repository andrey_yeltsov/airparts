<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2020   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and accept    *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
use Tygh\Registry;
if (!defined('BOOTSTRAP')) {
die('Access denied');
}
$schema['central']['ab__addons']['items']['abt__youpitheme'] = [
'attrs' => ['class' => 'is-addon'],
'href' => 'abt__yt.settings',
'position' => 2,
'subitems' => [
'abt__yt.settings' => [
'href' => 'abt__yt.settings',
'position' => 100,
],
'abt__yt.less_settings' => [
'href' => 'abt__yt.less_settings',
'position' => 200,
],
'abt__yt.microdata' => [
'href' => 'abt__yt.microdata',
'position' => 300,
],
'abt__yt.icons' => [
'href' => 'abt__yt.icons',
'position' => 300,
],
'abt__yt.demodata' => [
'href' => 'abt__yt.demodata',
'position' => 400,
],
'abt__yt.help' => [
'href' => 'abt__yt.help',
'position' => 1000,
],
],
];
if (Registry::get('addons.buy_together.status') == 'A') {
$schema['central']['products']['items']['abt__yt_buy_together.generate'] = [
'href' => 'abt__yt_buy_together.generate',
'position' => 800,
];
$schema['central']['products']['items']['abt__yt_buy_together.manage'] = [
'href' => 'abt__yt_buy_together.manage',
'position' => 900,
];
}
return $schema;
